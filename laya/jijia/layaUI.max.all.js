var CLASS$=Laya.class;
var STATICATTR$=Laya.static;
var View=laya.ui.View;
var Dialog=laya.ui.Dialog;
var ActivityUI=(function(_super){
		function ActivityUI(){
			
		    this.bgImg=null;
		    this.loadingBar=null;
		    this.loadText=null;

			ActivityUI.__super.call(this);
		}

		CLASS$(ActivityUI,'ui.ActivityUI',_super);
		var __proto__=ActivityUI.prototype;
		__proto__.createChildren=function(){
		    
			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ActivityUI.uiView);
		}
		ActivityUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":0,"var":"bgImg","skin":"activity/bg.jpg","scaleY":1.3,"scaleX":1.3,"centerX":0}},{"type":"Image","props":{"y":134,"x":34,"skin":"activity/logoload.png"}},{"type":"ProgressBar","props":{"y":826,"x":79,"var":"loadingBar","skin":"activity/progressBar.png","sizeGrid":"0,0,4,0,1"}},{"type":"Label","props":{"y":859,"x":161,"width":324,"var":"loadText","text":"亲，请稍等 (･∀･) 努力加载中","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Label","props":{"y":693,"x":272,"width":175,"text":"健康游戏忠告","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":727,"x":207,"width":293,"text":"抵制不良游戏， 拒绝盗版游戏。","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":749,"x":207,"width":293,"text":"注意自我保护， 谨防受骗上当。","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":770,"x":207,"width":293,"text":"适度游戏益脑， 沉迷游戏伤身。","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":792,"x":207,"width":293,"text":"合理安排时间， 享受健康生活。","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":902,"x":161,"width":324,"text":"著作权人：深圳小多科技有限公司\\n\\n本游戏合适16岁以上用户","height":55,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"center"}}]};
		return ActivityUI;
	})(View);
var ActivityDailyUI=(function(_super){
		function ActivityDailyUI(){
			
		    this.bgImg=null;
		    this.titleList=null;
		    this.investmentPanel=null;
		    this.btn_investment=null;
		    this.btn_pay=null;
		    this.investmentList=null;
		    this.richManPanel=null;
		    this.countdown=null;
		    this.ani_laGan=null;
		    this.btn_laGan=null;
		    this.diamondMax=null;
		    this.number_0=null;
		    this.number_1=null;
		    this.number_2=null;
		    this.number_3=null;
		    this.number_4=null;
		    this.remainingTimes=null;
		    this.spendDiamond=null;
		    this.time_0=null;
		    this.time_1=null;
		    this.time_2=null;
		    this.vipPanel=null;
		    this.receiveToday2=null;
		    this.receiveOver2=null;
		    this.btn_receive_2=null;
		    this.btn_pay_vip2=null;
		    this.receiveToday1=null;
		    this.receiveOver1=null;
		    this.timeBg1=null;
		    this.timeValue1=null;
		    this.btn_receive_1=null;
		    this.btn_pay_vip1=null;
		    this.receiveToday0=null;
		    this.receiveOver0=null;
		    this.timeBg0=null;
		    this.timeValue0=null;
		    this.btn_receive_0=null;
		    this.btn_pay_vip0=null;
		    this.exchangePanel=null;
		    this.cosmicCubeText=null;
		    this.exchangeDataList=null;
		    this.giftPackPanel=null;
		    this.giftCodeText=null;
		    this.btn_giftCode=null;
		    this.btn_back=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.flashFighter=null;
		    this.payTip=null;

			ActivityDailyUI.__super.call(this);
		}

		CLASS$(ActivityDailyUI,'ui.ActivityDailyUI',_super);
		var __proto__=ActivityDailyUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.ImageScale",bean.ImageScale);
			View.regComponent("bean.ImageAlpha",bean.ImageAlpha);
			View.regComponent("bean.ImageFloatUpDown",bean.ImageFloatUpDown);
			View.regComponent("bean.ImageRotation",bean.ImageRotation);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.FlashFigterUI",ui.FlashFigterUI);
			View.regComponent("ui.PayTipUI",ui.PayTipUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ActivityDailyUI.uiView);
		}
		ActivityDailyUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":94,"x":0,"skin":"activityDaily/di.png"}},{"type":"List","props":{"y":98,"x":43,"width":557,"var":"titleList","spaceY":0,"spaceX":0,"repeatY":1,"repeatX":4,"height":129,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"width":140,"name":"render","height":129},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"activityDaily/iconk.png","name":"selectIcon"}},{"type":"Image","props":{"y":9,"x":11,"skin":"activityDaily/icon1.png","name":"icon"}},{"type":"Image","props":{"skin":"menu/zd.png","name":"new_activity"}}]}]},{"type":"Image","props":{"y":165,"x":27,"skin":"activityDaily/dijt.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":165,"x":613,"skin":"activityDaily/dijt.png","scaleX":-1,"anchorY":0.5,"anchorX":0.5}},{"type":"Panel","props":{"y":236,"x":0,"width":640,"visible":false,"var":"investmentPanel","height":630},"child":[{"type":"Image","props":{"y":-1,"x":114,"skin":"activityDaily/zi1.png"}},{"type":"Image","props":{"y":141,"x":129,"skin":"activityDaily/zi2.png"}},{"type":"Button","props":{"y":90,"x":230,"var":"btn_investment","stateNum":1,"skin":"activityDaily/btn_investment.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":90,"x":406,"var":"btn_pay","stateNum":1,"skin":"activityDaily/btn_pay.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"List","props":{"y":221,"x":45,"width":587,"var":"investmentList","vScrollBarSkin":"menu/vscroll.png","spaceY":2,"spaceX":0,"repeatY":3,"repeatX":1,"height":406},"child":[{"type":"Box","props":{"y":0,"x":0,"width":586,"name":"render","height":128},"child":[{"type":"Image","props":{"skin":"activityDaily/tiaodi.png"}},{"type":"Image","props":{"y":19,"x":24,"skin":"quality/4.png"}},{"type":"Image","props":{"y":31,"x":38,"skin":"tool/3002.png"}},{"type":"Label","props":{"y":20,"x":155,"width":280,"text":"领取","name":"aTitle","height":20,"fontSize":20,"font":"SimHei","color":"#000000","align":"left"}},{"type":"Label","props":{"y":53,"x":160,"wordWrap":true,"width":226,"text":"角色达到","name":"aText","height":53,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":37,"x":454,"width":91,"text":"已领取","name":"hadReceive","height":24,"fontSize":24,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Button","props":{"y":63,"x":484,"stateNum":1,"skin":"rank/btn_task_receive.png","name":"btn_receive","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":55,"x":412,"width":125,"text":"投资后可领取","name":"canNot","height":18,"fontSize":18,"font":"SimHei","color":"#000000","align":"center"}},{"type":"Label","props":{"y":55,"x":412,"width":125,"text":"1/1","name":"schedule","height":18,"fontSize":18,"font":"SimHei","color":"#000000","align":"center"}}]}]}]},{"type":"Panel","props":{"y":236,"x":0,"width":640,"visible":false,"var":"richManPanel","height":630},"child":[{"type":"Image","props":{"y":10,"x":100,"skin":"activityDaily/btdi.png"}},{"type":"Image","props":{"y":19,"x":177,"skin":"activityDaily/bt1.png"}},{"type":"Image","props":{"y":20,"x":382,"skin":"activityDaily/bt2.png"}},{"type":"Label","props":{"y":19,"x":267,"width":116,"var":"countdown","height":26,"fontSize":26,"font":"SimHei","color":"#00ff00","align":"center"}},{"type":"Image","props":{"y":62,"x":23,"skin":"activityDaily/laohuji3.png"}},{"type":"Image","props":{"y":137,"x":281,"skin":"activityDaily/diguang.png","scaleTime":1000,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.ImageScale"}},{"type":"Animation","props":{"y":170,"x":544,"width":0,"visible":false,"var":"ani_laGan","source":"Ani_lhjg.ani","height":0}},{"type":"Button","props":{"y":178,"x":552,"var":"btn_laGan","stateNum":1,"skin":"activityDaily/btn_laGan.png"},"child":[{"type":"Image","props":{"y":-8,"x":-9,"skin":"activityDaily/laohujigan1ch.png","alphaTime":380,"runtime":"bean.ImageAlpha"}},{"type":"Image","props":{"y":-104,"x":-16,"skin":"activityDaily/lhjts.png","distanceFloat":8,"runtime":"bean.ImageFloatUpDown"}}]},{"type":"Label","props":{"y":100,"x":173,"width":224,"text":"最高可获得钻石","strokeColor":"#002bcb","stroke":4,"height":19,"fontSize":19,"font":"SimHei","color":"#00ff00","align":"center"}},{"type":"Label","props":{"y":150,"x":284,"width":370,"var":"diamondMax","scaleY":0.8,"scaleX":0.8,"height":55,"fontSize":55,"font":"myFontGameScore","color":"#a6a007","bold":true,"anchorY":0.5,"anchorX":0.5,"align":"center"}},{"type":"Clip","props":{"y":290,"x":420,"var":"number_0","skin":"activityDaily/battleNumber.png","index":0,"clipY":1,"clipX":10,"clipWidth":38,"clipHeight":52}},{"type":"Clip","props":{"y":290,"x":344,"var":"number_1","skin":"activityDaily/battleNumber.png","index":0,"clipY":1,"clipX":10,"clipWidth":38,"clipHeight":52}},{"type":"Clip","props":{"y":290,"x":267,"var":"number_2","skin":"activityDaily/battleNumber.png","index":0,"clipY":1,"clipX":10,"clipWidth":38,"clipHeight":52}},{"type":"Clip","props":{"y":290,"x":191,"var":"number_3","skin":"activityDaily/battleNumber.png","index":0,"clipY":1,"clipX":10,"clipWidth":38,"clipHeight":52}},{"type":"Clip","props":{"y":290,"x":115,"var":"number_4","skin":"activityDaily/battleNumber.png","index":0,"clipY":1,"clipX":10,"clipWidth":38,"clipHeight":52}},{"type":"Label","props":{"y":401,"x":351,"wordWrap":true,"width":58,"text":"还剩","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":401,"x":429,"wordWrap":true,"width":76,"text":"次摇奖","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":410,"x":421,"wordWrap":true,"width":30,"var":"remainingTimes","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#00fe4e","anchorY":0.5,"anchorX":0.5,"align":"center"}},{"type":"Image","props":{"y":434,"x":221,"skin":"tool/3002.png","scaleY":0.5,"scaleX":0.5}},{"type":"Label","props":{"y":440,"x":254,"width":102,"var":"spendDiamond","height":24,"fontSize":24,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Image","props":{"y":502,"x":21,"skin":"activityDaily/di_lhj.png"}},{"type":"Image","props":{"y":521,"x":44,"skin":"share/di2xin.png"}},{"type":"Image","props":{"y":552,"x":44,"skin":"share/di2xin.png"}},{"type":"Image","props":{"y":585,"x":44,"skin":"share/di2xin.png"}},{"type":"Label","props":{"y":525,"x":77,"wordWrap":true,"width":248,"text":"活动期间，每位玩家拥有","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":525,"x":332,"wordWrap":true,"width":142,"text":"抽奖机会","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":525,"x":285,"width":50,"var":"time_0","text":"2次","height":18,"fontSize":18,"font":"SimHei","color":"#a6a007","bold":true,"align":"center"}},{"type":"Label","props":{"y":556,"x":77,"wordWrap":true,"width":282,"text":"活动期间每充值6元即可获得","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":556,"x":364,"wordWrap":true,"width":215,"text":"额外的抽奖机会","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":556,"x":309,"width":50,"var":"time_1","text":"1次","height":18,"fontSize":18,"font":"SimHei","color":"#a6a007","bold":true,"align":"center"}},{"type":"Label","props":{"y":587,"x":77,"wordWrap":true,"width":363,"text":"每位玩家在活动期间，最多可累计获得","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":587,"x":448,"wordWrap":true,"width":157,"text":"额外的抽奖机会","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":587,"x":392,"width":50,"var":"time_2","text":"10次","height":18,"fontSize":18,"font":"SimHei","color":"#a6a007","bold":true,"align":"center"}}]},{"type":"Panel","props":{"y":236,"x":0,"width":640,"visible":false,"var":"vipPanel","height":630},"child":[{"type":"Image","props":{"y":24,"x":0,"skin":"activityDaily/card3.png"}},{"type":"Image","props":{"y":119,"x":488,"visible":false,"var":"receiveToday2","skin":"activityDaily/zklq.png"}},{"type":"Image","props":{"y":119,"x":488,"visible":false,"var":"receiveOver2","skin":"activityDaily/zklw.png"}},{"type":"Button","props":{"y":175,"x":544,"visible":false,"var":"btn_receive_2","stateNum":1,"skin":"rank/btn_task_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":175,"x":534,"visible":false,"var":"btn_pay_vip2","stateNum":1,"skin":"activityDaily/btn_pay_vip.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":215,"x":0,"skin":"activityDaily/card2.png"}},{"type":"Image","props":{"y":310,"x":488,"visible":false,"var":"receiveToday1","skin":"activityDaily/zklq.png"}},{"type":"Image","props":{"y":310,"x":488,"visible":false,"var":"receiveOver1","skin":"activityDaily/zklw.png"}},{"type":"Image","props":{"y":244,"x":532,"visible":false,"var":"timeBg1","skin":"activityDaily/shenyu.png"},"child":[{"type":"Label","props":{"y":12,"x":4,"width":55,"text":"剩余","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Label","props":{"y":34,"x":33,"width":34,"text":"天","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"left"}},{"type":"Label","props":{"y":33,"x":-12,"width":43,"var":"timeValue1","height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","bold":true,"align":"right"}}]},{"type":"Button","props":{"y":369,"x":544,"visible":false,"var":"btn_receive_1","stateNum":1,"skin":"rank/btn_task_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":367,"x":534,"visible":false,"var":"btn_pay_vip1","stateNum":1,"skin":"activityDaily/btn_pay_vip.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":406,"x":0,"skin":"activityDaily/card1.png"}},{"type":"Image","props":{"y":501,"x":488,"visible":false,"var":"receiveToday0","skin":"activityDaily/zklq.png"}},{"type":"Image","props":{"y":501,"x":488,"visible":false,"var":"receiveOver0","skin":"activityDaily/zklw.png"}},{"type":"Image","props":{"y":435,"x":532,"visible":false,"var":"timeBg0","skin":"activityDaily/shenyu.png"},"child":[{"type":"Label","props":{"y":12,"x":4,"width":55,"text":"剩余","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Label","props":{"y":34,"x":33,"width":34,"text":"天","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"left"}},{"type":"Label","props":{"y":33,"x":-12,"width":43,"var":"timeValue0","height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","bold":true,"align":"right"}}]},{"type":"Button","props":{"y":559,"x":544,"visible":false,"var":"btn_receive_0","stateNum":1,"skin":"rank/btn_task_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":559,"x":534,"visible":false,"var":"btn_pay_vip0","stateNum":1,"skin":"activityDaily/btn_pay_vip.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":236,"x":0,"width":640,"visible":false,"var":"exchangePanel","height":630},"child":[{"type":"Image","props":{"y":70,"x":223,"skin":"activityDaily/fkd.png"}},{"type":"Image","props":{"y":8,"x":420,"skin":"activityDaily/fk.png","distanceFloat":8,"runtime":"bean.ImageFloatUpDown"}},{"type":"Image","props":{"y":10,"x":25,"skin":"activityDaily/zi1_jn.png"}},{"type":"Image","props":{"y":117,"x":46,"skin":"activityDaily/zi2_jn.png"}},{"type":"Label","props":{"y":124,"x":197,"width":144,"var":"cosmicCubeText","strokeColor":"#0042ff","stroke":4,"height":24,"fontSize":24,"font":"SimHei","color":"#00ffcc","align":"left"}},{"type":"List","props":{"y":178,"x":17,"width":636,"var":"exchangeDataList","vScrollBarSkin":"menu/vscroll.png","spaceY":0,"spaceX":0,"repeatY":3,"repeatX":1,"height":459},"child":[{"type":"Box","props":{"y":0,"x":0,"width":636,"name":"render","height":153},"child":[{"type":"Image","props":{"skin":"activityDaily/tiaodi_jn.png"}},{"type":"Image","props":{"y":40,"x":165,"skin":"activityDaily/tiaojt.png"}},{"type":"Image","props":{"y":33,"x":40,"visible":true,"skin":"quality/2.png","name":"quality_left"}},{"type":"Image","props":{"y":45,"x":55,"width":71,"name":"icon_left","height":74}},{"type":"Image","props":{"y":102,"x":51,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":105,"x":53,"width":29,"name":"number_left","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":28,"x":108,"skin":"quality/suipian.png","name":"fragment_left"}},{"type":"Image","props":{"y":27,"x":92,"width":75,"name":"qualityNumber_left","height":21,"anchorX":0.5}},{"type":"List","props":{"y":121,"x":94,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList_left","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":134,"x":42,"width":100,"visible":true,"name":"name_left","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":33,"x":276,"visible":true,"skin":"quality/5.png","name":"quality_right"}},{"type":"Image","props":{"y":45,"x":291,"width":71,"name":"icon_right","height":74}},{"type":"Image","props":{"y":102,"x":287,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":105,"x":289,"width":29,"name":"number_right","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Button","props":{"y":91,"x":503,"stateNum":1,"skin":"activityDaily/btn_exchange.png","name":"btn_exchange","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]}]}]},{"type":"Panel","props":{"y":236,"x":0,"width":640,"visible":false,"var":"giftPackPanel","height":630},"child":[{"type":"Image","props":{"y":296,"x":319,"skin":"activityDaily/di_gp2.png","rotationTime":20000,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.ImageRotation"}},{"type":"Image","props":{"y":206,"x":102,"skin":"activityDaily/di_gp1.png"}},{"type":"Image","props":{"y":241,"x":212,"skin":"role/bt1.png"}},{"type":"Image","props":{"y":305,"x":168,"skin":"role/kuan.png"}},{"type":"TextInput","props":{"y":308,"x":176,"width":300,"var":"giftCodeText","promptColor":"#f2ffac","prompt":"请输入礼品码","height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Button","props":{"y":415,"x":320,"var":"btn_giftCode","stateNum":1,"skin":"dialogRes/btn_bg_ye.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":12,"x":10,"width":116,"text":"领取","strokeColor":"#813100","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"center"}}]}]},{"type":"Button","props":{"y":900,"x":551,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"FlashFigter","props":{"y":0,"x":0,"visible":false,"var":"flashFighter","runtime":"ui.FlashFigterUI"}},{"type":"PayTip","props":{"y":0,"x":0,"visible":false,"var":"payTip","runtime":"ui.PayTipUI"}}]};
		return ActivityDailyUI;
	})(View);
var ActivityJiNiangUI=(function(_super){
		function ActivityJiNiangUI(){
			
		    this.activityTime=null;
		    this.btn_jnText=null;
		    this.rankText=null;
		    this.totalText=null;
		    this.differencePanel=null;
		    this.differenceNumber=null;
		    this.differenceRank=null;
		    this.btn_dialogClose=null;
		    this.rewardDialog=null;
		    this.btn_close=null;
		    this.rewardList=null;
		    this.dataLoading=null;

			ActivityJiNiangUI.__super.call(this);
		}

		CLASS$(ActivityJiNiangUI,'ui.ActivityJiNiangUI',_super);
		var __proto__=ActivityJiNiangUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ActivityJiNiangUI.uiView);
		}
		ActivityJiNiangUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":68,"x":11,"skin":"activityDaily/di_jn.png"}},{"type":"Label","props":{"y":72,"x":175,"width":300,"var":"activityTime","height":20,"fontSize":20,"font":"SimHei","color":"#ffe400","bold":true,"align":"center"}},{"type":"Label","props":{"y":605,"x":76,"width":493,"text":"前往任意地点进行战斗就有机会遭遇机娘BOSS","height":20,"fontSize":20,"font":"SimHei","color":"#ffe400","bold":true,"align":"center"}},{"type":"Button","props":{"y":666,"x":547,"var":"btn_jnText","stateNum":1,"skin":"activityDaily/btn_jnText.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":648,"x":253,"width":145,"text":"你的排名","height":24,"fontSize":24,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":692,"x":129,"width":387,"var":"rankText","strokeColor":"#0030ff","stroke":6,"height":60,"fontSize":60,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":773,"x":106,"width":259,"text":"累计获得机娘信物:","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":773,"x":370,"width":157,"var":"totalText","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Panel","props":{"y":813,"x":0,"width":640,"var":"differencePanel","height":88},"child":[{"type":"Label","props":{"y":13,"x":59,"width":93,"text":"再获得","height":20,"fontSize":20,"font":"SimHei","color":"#00f5ff","align":"right"}},{"type":"Label","props":{"y":13,"x":262,"width":208,"text":"机娘信物即可进入前","height":20,"fontSize":20,"font":"SimHei","color":"#00f5ff","align":"center"}},{"type":"Label","props":{"y":13,"x":537,"width":67,"text":"名","height":20,"fontSize":20,"font":"SimHei","color":"#00f5ff","align":"left"}},{"type":"Label","props":{"y":13,"x":153,"width":123,"var":"differenceNumber","height":20,"fontSize":20,"font":"SimHei","color":"#ef4089","align":"center"}},{"type":"Label","props":{"y":13,"x":455,"width":83,"var":"differenceRank","height":20,"fontSize":20,"font":"SimHei","color":"#ffe400","bold":true,"align":"center"}}]},{"type":"Button","props":{"y":77,"x":573,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"rewardDialog","height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":172,"x":15,"width":607,"skin":"backpack/di1_3.png","sizeGrid":"150,120,150,120","height":570}},{"type":"Image","props":{"y":177,"x":183,"skin":"backpack/jlshm.png"}},{"type":"Button","props":{"y":179,"x":590,"var":"btn_close","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":709,"x":39,"width":560,"text":"活动结束后进行结算，根据当前名次发放对应奖励到邮箱","height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","bold":true,"align":"center"}},{"type":"List","props":{"y":223,"x":33,"width":573,"visible":true,"var":"rewardList","vScrollBarSkin":"menu/vscroll.png","spaceY":5,"repeatY":8,"repeatX":1,"height":477},"child":[{"type":"Box","props":{"name":"render","height":55},"child":[{"type":"Image","props":{"x":-2.1316282072803006e-14,"visible":true,"skin":"rank/ditiao.png"}},{"type":"Label","props":{"y":19,"x":25.99999999999998,"width":43,"text":"名次","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":182.99999999999997,"width":43,"text":"奖励","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":71.99999999999997,"width":100,"visible":true,"name":"rankIndex","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":232.99999999999997,"width":330,"visible":true,"name":"rankGoods","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}}]}]}]},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return ActivityJiNiangUI;
	})(View);
var ActivityPayUI=(function(_super){
		function ActivityPayUI(){
			
		    this.bgImage=null;
		    this.btn_dialogClose=null;
		    this.btn_pay=null;
		    this.tomorrowOpen=null;
		    this.goodsList=null;
		    this.dataLoading=null;
		    this.flashFighter=null;
		    this.payTip=null;
		    this.topNotice=null;
		    this.noticeText=null;

			ActivityPayUI.__super.call(this);
		}

		CLASS$(ActivityPayUI,'ui.ActivityPayUI',_super);
		var __proto__=ActivityPayUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.FlashFigterUI",ui.FlashFigterUI);
			View.regComponent("ui.PayTipUI",ui.PayTipUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ActivityPayUI.uiView);
		}
		ActivityPayUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":68,"x":42,"var":"bgImage","skin":"role/bg1.png"}},{"type":"Button","props":{"y":74,"x":596,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":667,"x":450,"var":"btn_pay","stateNum":1,"skin":"role/btn_pay.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":586,"x":316.99999999999983,"visible":false,"var":"tomorrowOpen","skin":"role/bb3.png"}},{"type":"List","props":{"y":565,"x":94,"width":236,"var":"goodsList","vScrollBarSkin":"menu/vscroll.png","spaceX":2,"repeatY":2,"repeatX":2,"height":215},"child":[{"type":"Box","props":{"y":0,"x":-2,"width":110,"name":"render","height":100},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":78,"x":13,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"FlashFigter","props":{"y":0,"x":0,"visible":false,"var":"flashFighter","runtime":"ui.FlashFigterUI"}},{"type":"PayTip","props":{"y":0,"x":0,"visible":false,"var":"payTip","runtime":"ui.PayTipUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return ActivityPayUI;
	})(View);
var AnncDialogUI=(function(_super){
		function AnncDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.indexList=null;
		    this.textList=null;

			AnncDialogUI.__super.call(this);
		}

		CLASS$(AnncDialogUI,'ui.AnncDialogUI',_super);
		var __proto__=AnncDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.FlashArrow",bean.FlashArrow);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(AnncDialogUI.uiView);
		}
		AnncDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":69,"x":0,"width":634,"skin":"backpack/di1_3.png","sizeGrid":"150,120,150,120","height":758}},{"type":"Image","props":{"y":76,"x":181,"skin":"backpack/gg.png"}},{"type":"Button","props":{"y":78,"x":591,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":187,"x":29,"width":573,"skin":"rank/diTip.png","sizeGrid":"40,40,40,40","height":596}},{"type":"Image","props":{"y":487,"x":588,"skin":"warehouse/biao.png","rotation":-90,"moveDirection":"right","runtime":"bean.FlashArrow"}},{"type":"Image","props":{"y":487,"x":48,"skin":"warehouse/biao.png","scaleX":-1,"rotation":90,"moveDirection":"left","runtime":"bean.FlashArrow"}},{"type":"List","props":{"y":796,"x":310,"width":480,"var":"indexList","spaceX":17,"repeatY":1,"height":34,"hScrollBarSkin":"menu/hscroll.png","anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"visible":true,"skin":"warehouse/fxpoint1.png","name":"icon"}}]}]},{"type":"List","props":{"y":139,"x":60,"width":525,"var":"textList","repeatY":1,"height":636,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"y":0,"x":0,"width":525,"name":"render","height":636},"child":[{"type":"Label","props":{"x":5,"width":500,"visible":true,"name":"anncTitle","height":40,"fontSize":40,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Panel","props":{"y":76,"width":525,"vScrollBarSkin":"menu/vscroll.png","name":"textPanel","height":560},"child":[{"type":"HTMLDivElement","props":{"y":0,"x":0,"width":525,"name":"anncText","height":800}}]}]}]}]};
		return AnncDialogUI;
	})(View);
var BackpackDialogUI=(function(_super){
		function BackpackDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.check_fragment=null;
		    this.check_resources=null;
		    this.check_other=null;
		    this.resourcesList=null;
		    this.fragmentList=null;
		    this.otherList=null;
		    this.btn_backpack_sell=null;
		    this.goods_description=null;
		    this.quality_icon=null;
		    this.goods_icon=null;
		    this.goods_starList=null;
		    this.goods_qualityNumber=null;
		    this.goods_fragment=null;
		    this.goods_name=null;
		    this.goods_number=null;
		    this.goods_price=null;
		    this.btn_close=null;
		    this.sellDlg=null;
		    this.dataLoading=null;
		    this.topNotice=null;
		    this.noticeText=null;

			BackpackDialogUI.__super.call(this);
		}

		CLASS$(BackpackDialogUI,'ui.BackpackDialogUI',_super);
		var __proto__=BackpackDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.SellDialogUI",ui.SellDialogUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(BackpackDialogUI.uiView);
		}
		BackpackDialogUI.uiView={"type":"View","props":{"width":640,"visible":false,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":93,"x":19,"skin":"backpack/di1_3.png"}},{"type":"Button","props":{"y":96,"x":590,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":108,"x":186,"skin":"backpack/btdin.png"}},{"type":"Image","props":{"y":359,"x":30,"skin":"backpack/ndi2.png"}},{"type":"Image","props":{"y":200,"x":32,"skin":"backpack/ndi1.png"}},{"type":"CheckBox","props":{"y":181,"x":263,"var":"check_fragment","stateNum":2,"skin":"backpack/check_fragment.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":181,"x":130,"var":"check_resources","stateNum":2,"skin":"backpack/check_resources.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":181,"x":396,"var":"check_other","stateNum":2,"skin":"backpack/check_other.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"List","props":{"y":224,"x":57,"width":535,"var":"resourcesList","vScrollBarSkin":"menu/vscroll.png","repeatX":4,"height":300},"child":[{"type":"Box","props":{"y":0,"x":-5,"name":"render"},"child":[{"type":"Image","props":{"y":1,"x":0,"visible":false,"skin":"backpack/xuan.png","name":"selectedImg"}},{"type":"Image","props":{"y":6,"x":5,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":20,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":56,"visible":true,"skin":"backpack/shuliangkuang.png"}},{"type":"Label","props":{"y":78,"x":58,"width":40,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":73,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"x":57,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":59,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"List","props":{"y":224,"x":57,"width":535,"var":"fragmentList","vScrollBarSkin":"menu/vscroll.png","repeatX":4,"height":300},"child":[{"type":"Box","props":{"y":0,"x":-5,"name":"render"},"child":[{"type":"Image","props":{"y":1,"visible":false,"skin":"backpack/xuan.png","name":"selectedImg"}},{"type":"Image","props":{"y":6,"x":5,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":20,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":56,"visible":true,"skin":"backpack/shuliangkuang.png"}},{"type":"Label","props":{"y":78,"x":58,"width":40,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":73,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"x":57,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":59,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"List","props":{"y":224,"x":57,"width":535,"visible":false,"var":"otherList","vScrollBarSkin":"menu/vscroll.png","repeatX":4,"height":300},"child":[{"type":"Box","props":{"y":0,"x":-5,"name":"render"},"child":[{"type":"Image","props":{"y":1,"visible":false,"skin":"backpack/xuan.png","name":"selectedImg"}},{"type":"Image","props":{"y":6,"x":5,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":20,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":56,"visible":true,"skin":"backpack/shuliangkuang.png"}},{"type":"Label","props":{"y":78,"x":58,"width":40,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":73,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"x":57,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":59,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Button","props":{"y":775,"x":440,"var":"btn_backpack_sell","stateNum":1,"skin":"backpack/btn_sell.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":740,"x":110,"width":37,"skin":"tool/3001.png","name":"jinbi","height":37}},{"type":"Label","props":{"y":668,"x":93,"wordWrap":true,"width":442,"var":"goods_description","height":58,"fontSize":18,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Image","props":{"y":590,"x":315,"var":"quality_icon","skin":"quality/0.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"goods_icon","height":74}},{"type":"List","props":{"y":89,"x":54,"width":80,"var":"goods_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"goods_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":1,"x":68,"var":"goods_fragment","skin":"quality/suipian.png"}}]},{"type":"Label","props":{"y":611,"x":93,"width":168,"var":"goods_name","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":613,"x":376,"width":60,"text":"拥有数量","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac"}},{"type":"Label","props":{"y":613,"x":506,"width":20,"text":"件","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac"}},{"type":"Image","props":{"y":608,"x":446,"skin":"backpack/slk.png"}},{"type":"Label","props":{"y":614,"x":448,"width":46,"var":"goods_number","height":15,"fontSize":15,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":747,"x":156,"width":171,"var":"goods_price","height":22,"fontSize":22,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Button","props":{"y":889,"x":320,"var":"btn_close","stateNum":1,"skin":"backpack/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"SellDialog","props":{"y":0,"x":0,"visible":false,"var":"sellDlg","runtime":"ui.SellDialogUI"}},{"type":"DataLoading","props":{"visible":false,"var":"dataLoading","centerY":0,"centerX":0,"runtime":"ui.DataLoadingUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return BackpackDialogUI;
	})(View);
var BadgeUI=(function(_super){
		function BadgeUI(){
			
		    this.bgImg=null;
		    this.badgeIcon=null;
		    this.badgeName=null;
		    this.arrow_left=null;
		    this.arrow_right=null;
		    this.badgeCurrent=null;
		    this.badgeNext=null;
		    this.badgeBar=null;
		    this.starList=null;
		    this.expBar=null;
		    this.badgeExp=null;
		    this.badgeExpLimit=null;
		    this.btn_back=null;
		    this.btn_train=null;
		    this.trainPrice=null;
		    this.ani_badge=null;
		    this.ani_badgePerfect=null;
		    this.ani_badgeStar=null;
		    this.dataLoading=null;

			BadgeUI.__super.call(this);
		}

		CLASS$(BadgeUI,'ui.BadgeUI',_super);
		var __proto__=BadgeUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.FlashArrow",bean.FlashArrow);
			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(BadgeUI.uiView);
		}
		BadgeUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Sprite","props":{"y":5,"x":0,"width":640,"var":"badgeIcon","height":600}},{"type":"Image","props":{"y":153,"x":26,"skin":"role/dibt.png"}},{"type":"Label","props":{"y":293,"x":78,"wordWrap":true,"width":27,"var":"badgeName","valign":"top","height":138,"fontSize":25,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":294,"x":52,"var":"arrow_left","skin":"warehouse/biao.png","rotation":90,"moveDirection":"left","runtime":"bean.FlashArrow"}},{"type":"Image","props":{"y":360,"x":585,"var":"arrow_right","skin":"warehouse/biao.png","rotation":-90,"moveDirection":"right","runtime":"bean.FlashArrow"}},{"type":"Image","props":{"y":505,"x":48,"skin":"role/dizi.png"}},{"type":"Image","props":{"y":505,"x":337,"skin":"role/dizi.png"}},{"type":"Label","props":{"y":525,"x":94,"width":112,"text":"当前星级","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":525,"x":375,"width":112,"text":"下一星级","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":568,"x":74,"wordWrap":true,"width":215,"var":"badgeCurrent","height":75,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":568,"x":356,"wordWrap":true,"width":215,"var":"badgeNext","height":75,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Image","props":{"y":663,"x":0,"skin":"warehouse/diye.png"}},{"type":"ProgressBar","props":{"y":704,"x":122,"var":"badgeBar","skin":"role/progressBar_badge.png","sizeGrid":"0,0,0,0,1"}},{"type":"List","props":{"y":691,"x":105,"width":440,"var":"starList","spaceX":17,"repeatY":1,"repeatX":10,"height":42},"child":[{"type":"Box","props":{"width":28,"name":"render","height":28},"child":[{"type":"Image","props":{"skin":"warehouse/xin1.png","name":"icon"}}]}]},{"type":"ProgressBar","props":{"y":745,"x":106,"var":"expBar","skin":"role/progressBar_exp.png","sizeGrid":"1,4,0,4,1"}},{"type":"Image","props":{"y":776,"x":299,"skin":"role/szbucon.png"}},{"type":"Label","props":{"y":775,"x":138,"width":164,"var":"badgeExp","height":23,"fontSize":23,"font":"myFontYellow","align":"right"}},{"type":"Label","props":{"y":775,"x":315,"width":164,"var":"badgeExpLimit","height":23,"fontSize":23,"font":"myFontYellow","align":"left"}},{"type":"Button","props":{"y":887,"x":558,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":884,"x":321,"var":"btn_train","stateNum":1,"skin":"role/btn_train.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":13,"x":72,"width":49,"skin":"tool/3002.png","height":49}},{"type":"Label","props":{"y":25.999999999999886,"x":20.499999999999943,"width":59,"text":"培养","strokeColor":"#4c0c2f","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","bold":true,"align":"right"}},{"type":"Label","props":{"y":24,"x":120,"width":104,"var":"trainPrice","strokeColor":"#4c0c2f","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Animation","props":{"y":225,"x":245,"visible":false,"var":"ani_badge","source":"Ani_badge.ani"}},{"type":"Animation","props":{"y":274,"x":320,"visible":false,"var":"ani_badgePerfect","source":"Ani_badgePerfect.ani"}},{"type":"Animation","props":{"y":647,"x":71,"visible":false,"var":"ani_badgeStar","source":"Ani_badgeStar.ani"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return BadgeUI;
	})(View);
var BeginnerGuideUI=(function(_super){
		function BeginnerGuideUI(){
			
		    this.maskSprite=null;
		    this.guide0=null;
		    this.bg0=null;
		    this.btn_close0=null;
		    this.btn_task=null;
		    this.new_task=null;
		    this.circle0=null;
		    this.arrow0=null;
		    this.guide1=null;
		    this.bg1=null;
		    this.btn_close1=null;
		    this.btn_task_go=null;
		    this.circle1=null;
		    this.arrow1=null;
		    this.guide2=null;
		    this.bg2=null;
		    this.btn_close2=null;
		    this.btn_battle=null;
		    this.circle2=null;
		    this.arrow2=null;
		    this.guide3=null;
		    this.bg3=null;
		    this.btn_close3=null;
		    this.button_pause=null;
		    this.circle3=null;
		    this.arrow3=null;
		    this.guide4=null;
		    this.bg4=null;
		    this.btn_close4=null;
		    this.btn_exit=null;
		    this.circle4=null;
		    this.arrow4=null;
		    this.guide5=null;
		    this.bg5=null;
		    this.btn_close5=null;
		    this.btn_back5=null;
		    this.circle5=null;
		    this.arrow5=null;
		    this.guide6=null;
		    this.bg6=null;
		    this.btn_close6=null;
		    this.btn_task6=null;
		    this.circle6=null;
		    this.arrow6=null;
		    this.guide7=null;
		    this.bg7=null;
		    this.btn_close7=null;
		    this.btn_task_receive=null;
		    this.circle7=null;
		    this.arrow7=null;
		    this.guide8=null;
		    this.bg8=null;
		    this.btn_close8=null;
		    this.btn_fighter=null;
		    this.circle8=null;
		    this.arrow8=null;
		    this.guide9=null;
		    this.bg9=null;
		    this.btn_close9=null;
		    this.check_strengthen=null;
		    this.new_strengthen=null;
		    this.circle9=null;
		    this.arrow9=null;
		    this.guide10=null;
		    this.bg10=null;
		    this.btn_close10=null;
		    this.btn_weapon_main=null;
		    this.selectedImg_main=null;
		    this.weapon_main_icon=null;
		    this.weapon_main_starList=null;
		    this.weapon_main_qualityNumber=null;
		    this.weapon_main_strengthen=null;
		    this.circle10=null;
		    this.arrow10=null;
		    this.guide11=null;
		    this.bg11=null;
		    this.btn_close11=null;
		    this.btn_strengthen_1_11=null;
		    this.circle11=null;
		    this.arrow11=null;
		    this.guide12=null;
		    this.bg12=null;
		    this.btn_close12=null;
		    this.btn_armo=null;
		    this.selectedImg_armo=null;
		    this.armo_icon=null;
		    this.armo_starList=null;
		    this.armo_qualityNumber=null;
		    this.armo_strengthen=null;
		    this.circle12=null;
		    this.arrow12=null;
		    this.guide13=null;
		    this.btn_strengthen_1_13=null;
		    this.circle13=null;
		    this.arrow13=null;
		    this.guide14=null;
		    this.bg14=null;
		    this.btn_close14=null;
		    this.btn_weapon_sub=null;
		    this.selectedImg_sub=null;
		    this.weapon_sub_icon=null;
		    this.weapon_sub_starList=null;
		    this.weapon_sub_qualityNumber=null;
		    this.weapon_sub_strengthen=null;
		    this.circle14=null;
		    this.arrow14=null;
		    this.guide15=null;
		    this.bg15=null;
		    this.btn_close15=null;
		    this.btn_wingman=null;
		    this.selectedImg_wingman=null;
		    this.wingman_icon=null;
		    this.wingman_starList=null;
		    this.wingman_qualityNumber=null;
		    this.wingman_strengthen=null;
		    this.circle15=null;
		    this.arrow15=null;
		    this.guide16=null;
		    this.bg16=null;
		    this.btn_close16=null;
		    this.btn_back16=null;
		    this.circle16=null;
		    this.arrow16=null;
		    this.guide17=null;
		    this.btn_back17=null;
		    this.circle17=null;
		    this.arrow17=null;
		    this.guide18=null;
		    this.bg18=null;
		    this.btn_close18=null;
		    this.btn_18=null;
		    this.circle18=null;
		    this.arrow18=null;
		    this.guide19=null;
		    this.bg19=null;
		    this.btn_close19=null;
		    this.btn_19=null;
		    this.circle19=null;
		    this.arrow19=null;
		    this.guide20=null;
		    this.bg20=null;
		    this.btn_close20=null;
		    this.btn_20=null;
		    this.circle20=null;
		    this.arrow20=null;
		    this.guide21=null;
		    this.bg21=null;
		    this.btn_close21=null;
		    this.btn_21=null;
		    this.circle21=null;
		    this.arrow21=null;
		    this.guide22=null;
		    this.bg22=null;
		    this.btn_close22=null;
		    this.btn_22=null;
		    this.circle22=null;
		    this.arrow22=null;
		    this.guide23=null;
		    this.bg23=null;
		    this.btn_close23=null;
		    this.btn_23=null;
		    this.circle23=null;
		    this.arrow23=null;
		    this.guide24=null;
		    this.bg24=null;
		    this.btn_close24=null;
		    this.btn_24=null;
		    this.circle24=null;
		    this.arrow24=null;
		    this.guide25=null;
		    this.bg25=null;
		    this.btn_close25=null;
		    this.btn_25=null;
		    this.trainPrice=null;
		    this.circle25=null;
		    this.arrow25=null;
		    this.guide26=null;
		    this.bg26=null;
		    this.btn_close26=null;

			BeginnerGuideUI.__super.call(this);
		}

		CLASS$(BeginnerGuideUI,'ui.BeginnerGuideUI',_super);
		var __proto__=BeginnerGuideUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.GuideBg",bean.GuideBg);
			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.GuideCircle",bean.GuideCircle);
			View.regComponent("bean.GuideArrow",bean.GuideArrow);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(BeginnerGuideUI.uiView);
		}
		BeginnerGuideUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.001},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Sprite","props":{"y":480,"x":320,"width":640,"var":"maskSprite","scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.6},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"var":"guide0","height":960},"child":[{"type":"Image","props":{"y":435.99999999999994,"x":10.99999999999999,"var":"bg0","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":30,"wordWrap":true,"width":280,"text":"亲爱的指挥官：   \\n\\n欢迎您加入战队~~ 先去领取你的任务吧！","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269,"x":289,"var":"btn_close0","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":880,"x":164,"var":"btn_task","stateNum":1,"skin":"menu/btn_task.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"visible":false,"var":"new_task","skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":878,"x":163,"var":"circle0","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":799,"x":187,"var":"arrow0","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide1","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg1","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n点击【前往】按钮，能够快速的前往目标。\\n是不是很方便~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289.00000000000006,"var":"btn_close1","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":212,"x":558.0000000000002,"visible":true,"var":"btn_task_go","stateNum":1,"skin":"rank/btn_task_go.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":209,"x":560,"var":"circle1","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":138,"x":575,"var":"arrow1","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide2","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg2","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n点击【战斗】按钮，开启我们的第一场战斗吧~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close2","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":894,"x":401.0000000000001,"var":"btn_battle","stateNum":1,"skin":"world/btn_battle.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":894,"x":399,"var":"circle2","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":815,"x":413,"var":"arrow2","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide3","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg3","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n点击【暂停】按钮，就能立即停止战斗哦~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close3","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":94,"x":598,"var":"button_pause","stateNum":1,"skin":"attack/button_pause.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":91,"x":598,"var":"circle3","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":172,"x":580,"var":"arrow3","skin":"dialogRes/zxjt.png","scaleY":-1,"scaleX":-1,"anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide4","height":960},"child":[{"type":"Image","props":{"y":610,"x":10.99999999999999,"var":"bg4","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n让我们先结束战斗吧~~   因为还有更重要的事情等我们去做呢~\\n\\(≧▽≦)/~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close4","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":438,"x":401,"var":"btn_exit","stateNum":1,"skin":"attack/btn_exit.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":435,"x":399,"var":"circle4","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":354,"x":413,"var":"arrow4","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide5","height":960},"child":[{"type":"Image","props":{"y":376,"x":10.99999999999999,"var":"bg5","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n似乎有任务完成了~   \\n\\n快去领奖励吧！","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close5","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":895,"x":569,"var":"btn_back5","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":892,"x":570,"var":"circle5","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":813,"x":582,"var":"arrow5","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide6","height":960},"child":[{"type":"Image","props":{"y":435.99999999999994,"x":10.99999999999999,"var":"bg6","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n如果你达成了任务条件，界面会有个小红点提示你去领奖呢~~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close6","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":880,"x":164,"var":"btn_task6","stateNum":1,"skin":"menu/btn_task.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"visible":true,"skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":877,"x":161,"var":"circle6","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":799,"x":185,"var":"arrow6","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide7","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg7","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n点击【领取】按钮，你就可以拿到奖励了~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close7","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":231,"x":557,"visible":true,"var":"btn_task_receive","stateNum":1,"skin":"rank/btn_task_receive.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":225,"x":558,"var":"circle7","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":155,"x":575,"var":"arrow7","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide8","height":960},"child":[{"type":"Image","props":{"y":319.99999999999994,"x":10.99999999999999,"var":"bg8","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n接下来去战机界面看看吧~~   \\n\\n说不定能找到变强的办法。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close8","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":878,"x":480,"var":"btn_fighter","stateNum":1,"skin":"menu/btn_fighter.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":876,"x":481,"var":"circle8","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":799,"x":500,"var":"arrow8","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide9","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg9","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n想要接下来的战斗更加的轻松，让我们去强化装备吧！！","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close9","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"CheckBox","props":{"y":894,"x":202,"var":"check_strengthen","stateNum":2,"skin":"warehouse/check_strengthen.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"visible":true,"var":"new_strengthen","skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":893,"x":202,"var":"circle9","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":816,"x":218,"var":"arrow9","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide10","height":960},"child":[{"type":"Image","props":{"y":246,"x":10.99999999999999,"var":"bg10","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n这是主武器，战机主要的攻击装备。   \\n强化后能增加主武器的攻击力。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close10","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":735,"x":103.00000000000009,"var":"btn_weapon_main","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":true,"var":"selectedImg_main","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"weapon_main_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"weapon_main_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"weapon_main_qualityNumber","height":21,"anchorX":0.5}},{"type":"Box","props":{"y":-6,"x":51,"width":75,"height":21,"anchorX":0.5}},{"type":"Sprite","props":{"y":-6,"x":13,"width":75,"height":21}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"weapon_main_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":734,"x":103,"var":"circle10","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":638,"x":120,"var":"arrow10","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide11","height":960},"child":[{"type":"Image","props":{"y":246,"x":10.99999999999999,"var":"bg11","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n让我们强化一次看看会有什么样的变化。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close11","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":605,"x":186,"var":"btn_strengthen_1_11","stateNum":1,"skin":"warehouse/btn_strengthen_1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"visible":true,"skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":604,"x":185,"var":"circle11","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":532,"x":200,"var":"arrow11","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide12","height":960},"child":[{"type":"Image","props":{"y":246,"x":10.99999999999999,"var":"bg12","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n这是装甲，战机的防御性装备。   \\n\\n强化后，能增强装甲的防御能力。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close12","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":735,"x":243,"var":"btn_armo","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":true,"var":"selectedImg_armo","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"armo_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"armo_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"armo_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"armo_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":734,"x":243,"var":"circle12","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":638,"x":260,"var":"arrow12","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide13","height":960},"child":[{"type":"Button","props":{"y":605,"x":186,"var":"btn_strengthen_1_13","stateNum":1,"skin":"warehouse/btn_strengthen_1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"visible":true,"skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":604,"x":185,"var":"circle13","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":532,"x":200,"var":"arrow13","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide14","height":960},"child":[{"type":"Image","props":{"y":246,"x":10.99999999999999,"var":"bg14","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n这是副武器，战机辅助攻击装备。   \\n\\n强化后，能增加武器的攻击力。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close14","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":735,"x":384,"var":"btn_weapon_sub","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":true,"var":"selectedImg_sub","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"weapon_sub_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"weapon_sub_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"weapon_sub_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"weapon_sub_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":734,"x":384,"var":"circle14","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":638,"x":401,"var":"arrow14","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide15","height":960},"child":[{"type":"Image","props":{"y":246,"x":10.99999999999999,"var":"bg15","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n这是僚机，战机两侧的辅助攻击装备。   \\n\\n强化后，能增加武器的攻击力。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close15","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":735,"x":525,"var":"btn_wingman","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":true,"var":"selectedImg_wingman","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"wingman_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"wingman_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"wingman_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"wingman_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":734,"x":525,"var":"circle15","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":638,"x":542,"var":"arrow15","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide16","height":960},"child":[{"type":"Image","props":{"y":484,"x":10.99999999999999,"var":"bg16","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n(*@ο@*) 哇～   \\n\\n亲爱的指挥官，您的战机变得更加的强大了，快去战场试一试吧~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close16","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":895,"x":569,"var":"btn_back16","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":893,"x":570,"var":"circle16","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":817,"x":582,"var":"arrow16","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide17","height":960},"child":[{"type":"Button","props":{"y":895,"x":569,"var":"btn_back17","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":893,"x":570,"var":"circle17","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":817,"x":582,"var":"arrow17","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide18","height":960},"child":[{"type":"Image","props":{"y":414,"x":11,"var":"bg18","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"亲爱的指挥官：  \\n \\n战机已经得到了强化，现在我们就一起去战场实战检验一下吧！","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close18","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":895,"x":322,"var":"btn_18","stateNum":1,"skin":"menu/btn_begin.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":895,"x":324,"var":"circle18","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":819,"x":336,"var":"arrow18","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide19","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg19","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n点击【战斗】按钮，继续我们上一次的战斗。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000001,"x":289,"var":"btn_close19","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":894,"x":401,"var":"btn_19","stateNum":1,"skin":"world/btn_battle.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":891,"x":400,"var":"circle19","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":812,"x":414,"var":"arrow19","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide20","height":960},"child":[{"type":"Image","props":{"y":303.99999999999994,"x":10.99999999999999,"var":"bg20","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"当你遇到boss的时候可以使用大招“空中支援”和“雷霆激光”～\\n\\n试试“空中支援”的火力吧。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close20","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":772,"x":87,"var":"btn_20","stateNum":1,"skin":"attack/wsparticle_daoju2.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":772,"x":87,"var":"circle20","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":693,"x":101,"var":"arrow20","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide21","height":960},"child":[{"type":"Image","props":{"y":353.99999999999994,"x":10.99999999999999,"var":"bg21","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n现在我们再一次点击【暂停】按钮，对徽章技能进行培养增强吧~~","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000003,"x":289,"var":"btn_close21","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":95,"x":598,"var":"btn_21","stateNum":1,"skin":"attack/button_pause.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":91,"x":598,"var":"circle21","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":172,"x":580,"var":"arrow21","skin":"dialogRes/zxjt.png","scaleY":-1,"scaleX":-1,"anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide22","height":960},"child":[{"type":"Image","props":{"y":610,"x":10.99999999999999,"var":"bg22","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n先结束战斗吧~~   \\n\\n让我们回到主界面对徽章进行培养。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close22","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":438,"x":401,"var":"btn_22","stateNum":1,"skin":"attack/btn_exit.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":435,"x":399,"var":"circle22","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":354,"x":413,"var":"arrow22","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide23","height":960},"child":[{"type":"Image","props":{"y":376,"x":10.99999999999999,"var":"bg23","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n现在让我们回到主界面对徽章进行培养。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close23","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":895,"x":569,"var":"btn_23","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":894,"x":569,"var":"circle23","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":815,"x":581,"var":"arrow23","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide24","height":960},"child":[{"type":"Image","props":{"y":346,"x":10.99999999999999,"var":"bg24","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n对徽章培养可以增强战斗中大技能“空中支援”、“雷霆激光”的火力哦。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close24","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":880,"x":587,"var":"btn_24","stateNum":1,"skin":"menu/btn_badge.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":879,"x":586,"var":"circle24","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":800,"x":598,"var":"arrow24","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide25","height":960},"child":[{"type":"Image","props":{"y":376,"x":10.99999999999999,"var":"bg25","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n\\n现在就对“空中支援”徽章进行一次培养吧。","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close25","stateNum":1,"skin":"dialogRes/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Button","props":{"y":884,"x":324,"var":"btn_25","stateNum":1,"skin":"role/btn_train.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":20.999999999999886,"x":85.5,"skin":"tool/3002.png","scaleY":0.7,"scaleX":0.7}},{"type":"Label","props":{"y":25.999999999999886,"x":20.499999999999943,"width":59,"text":"培养","strokeColor":"#4c0c2f","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","bold":true,"align":"right"}},{"type":"Label","props":{"y":23.999999999999886,"x":123.5,"width":92,"var":"trainPrice","strokeColor":"#4c0c2f","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":882,"x":324,"var":"circle25","skin":"dialogRes/zxh.png","anchorY":0.5,"anchorX":0.5,"runtime":"bean.GuideCircle"}},{"type":"Image","props":{"y":803,"x":336,"var":"arrow25","skin":"dialogRes/zxjt.png","anchorY":1,"anchorX":0.5,"runtime":"bean.GuideArrow"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"guide26","height":960},"child":[{"type":"Image","props":{"y":376,"x":10.99999999999999,"var":"bg26","skin":"dialogRes/ditalk1.png","runtime":"bean.GuideBg"},"child":[{"type":"Label","props":{"y":124,"x":29.999999999999996,"wordWrap":true,"width":280,"text":"\\n恭喜您完成了全部新手教学，现在让我们回到战斗大显身手吧！","overflow":"hidden","height":170,"fontSize":20,"font":"SimHei","color":"#ffffff"}},{"type":"Button","props":{"y":269.0000000000002,"x":289,"var":"btn_close26","stateNum":1,"skin":"dialogRes/btn_finish.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]}]}]};
		return BeginnerGuideUI;
	})(View);
var BuyPhysicalDialogUI=(function(_super){
		function BuyPhysicalDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.btn_exchange=null;
		    this.diamondLabel=null;
		    this.textLabel=null;
		    this.todayLabel=null;
		    this.priceLabel=null;
		    this.numberLabel=null;
		    this.payTip=null;
		    this.topNotice=null;
		    this.noticeText=null;

			BuyPhysicalDialogUI.__super.call(this);
		}

		CLASS$(BuyPhysicalDialogUI,'ui.BuyPhysicalDialogUI',_super);
		var __proto__=BuyPhysicalDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.PayTipUI",ui.PayTipUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(BuyPhysicalDialogUI.uiView);
		}
		BuyPhysicalDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":217,"x":77,"skin":"dialogRes/bg_dhdl.png"}},{"type":"Image","props":{"y":228,"x":210,"skin":"dialogRes/dhdl.png"}},{"type":"Button","props":{"y":228,"x":544,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":558,"x":320,"var":"btn_exchange","stateNum":1,"skin":"dialogRes/btn_submit.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":380,"x":178,"skin":"tool/3002.png","scaleY":0.7,"scaleX":0.7}},{"type":"Image","props":{"y":382,"x":389,"skin":"tool/3003.png","scaleY":0.7,"scaleX":0.7}},{"type":"Image","props":{"y":391,"x":307,"skin":"role/dhjt.png"}},{"type":"Label","props":{"y":432,"x":164,"wordWrap":true,"width":191,"text":"拥有钻石总量：","leading":6,"height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":432,"x":352,"wordWrap":true,"width":168,"var":"diamondLabel","leading":6,"height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":312,"x":143,"wordWrap":true,"width":373,"var":"textLabel","text":"使用少量钻石可以兑换电力哦！","height":36,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":352,"x":138,"width":373,"var":"todayLabel","text":"（今日已用0/10次）","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Label","props":{"y":395,"x":232,"width":80,"var":"priceLabel","height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":395,"x":443,"width":80,"var":"numberLabel","height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"PayTip","props":{"y":0,"x":0,"visible":false,"var":"payTip","runtime":"ui.PayTipUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return BuyPhysicalDialogUI;
	})(View);
var CheckInUI=(function(_super){
		function CheckInUI(){
			
		    this.btn_dialogClose=null;
		    this.updateTime=null;
		    this.checkInCount=null;
		    this.rewardList=null;
		    this.checkInReward=null;
		    this.btn_receive=null;
		    this.btn_checkIn=null;
		    this.goodsInfo=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.topNotice=null;
		    this.noticeText=null;

			CheckInUI.__super.call(this);
		}

		CLASS$(CheckInUI,'ui.CheckInUI',_super);
		var __proto__=CheckInUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.GoodsInfoUI",ui.GoodsInfoUI);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(CheckInUI.uiView);
		}
		CheckInUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":88,"x":4,"skin":"dialogRes/bg_checkIn.png"}},{"type":"Button","props":{"y":98,"x":594,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":99,"x":206,"skin":"role/bt.png"}},{"type":"Label","props":{"y":673,"x":145,"width":87,"text":"每日","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":673,"x":288,"width":179,"text":"点后，登录可签到","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":673,"x":239,"wordWrap":true,"width":45,"var":"updateTime","text":"0:00","leading":6,"height":20,"fontSize":20,"font":"SimHei","color":"#ff4593","align":"center"}},{"type":"Image","props":{"y":702,"x":45,"skin":"rank/lantiao1.png"}},{"type":"Image","props":{"y":709,"x":60,"skin":"world/iconqdjl.png"}},{"type":"Label","props":{"y":714,"x":344,"width":100,"visible":true,"text":"签到次数","height":15,"fontSize":15,"font":"SimHei","color":"#0c274c","bold":true,"align":"right"}},{"type":"Label","props":{"y":712,"x":446,"wordWrap":true,"width":66,"var":"checkInCount","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","bold":true,"align":"left"}},{"type":"List","props":{"y":174,"x":20,"width":620,"var":"rewardList","vScrollBarSkin":"menu/vscroll.png","spaceY":10,"spaceX":2,"repeatY":4,"repeatX":5,"height":480},"child":[{"type":"Box","props":{"y":0,"x":0,"width":121,"name":"render","height":147},"child":[{"type":"Image","props":{"y":14,"visible":true,"skin":"role/gedi.png","name":"bg"}},{"type":"Image","props":{"y":21,"x":5,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":33,"x":20,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":90,"x":16,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":93,"x":18,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":16,"x":73,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":15,"x":57,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":109,"x":59,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":126,"x":7,"width":100,"visible":true,"text":"第999天","name":"date","height":18,"fontSize":18,"font":"SimHei","color":"#000000","align":"center"}},{"type":"Image","props":{"x":77,"visible":false,"skin":"role/gedijtgo.png","name":"current"}},{"type":"Image","props":{"y":14,"visible":false,"skin":"role/gelq.png","name":"isCheck"}}]}]},{"type":"List","props":{"y":730,"x":154,"width":344,"var":"checkInReward","spaceY":0,"spaceX":0,"repeatY":1,"repeatX":5,"height":70,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"y":0,"x":0,"width":64,"scaleY":0.6,"scaleX":0.6,"name":"render","height":110},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":78,"x":13,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Button","props":{"y":756,"x":556,"var":"btn_receive","stateNum":1,"skin":"rank/btn_task_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":890,"x":320,"var":"btn_checkIn","stateNum":1,"skin":"dialogRes/btn_checkIn.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"GoodsInfo","props":{"y":0,"x":0,"visible":false,"var":"goodsInfo","runtime":"ui.GoodsInfoUI"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return CheckInUI;
	})(View);
var CreateRoleUI=(function(_super){
		function CreateRoleUI(){
			
		    this.bgImg=null;
		    this.tipLabel=null;
		    this.roleName=null;
		    this.btn_login=null;
		    this.btn_randomName=null;
		    this.btn_boy=null;
		    this.btn_girl=null;
		    this.headBoy=null;
		    this.head0_0=null;
		    this.head0_1=null;
		    this.head0_2=null;
		    this.headGirl=null;
		    this.head1_0=null;
		    this.head1_1=null;
		    this.head1_2=null;
		    this.topNotice=null;
		    this.noticeText=null;

			CreateRoleUI.__super.call(this);
		}

		CLASS$(CreateRoleUI,'ui.CreateRoleUI',_super);
		var __proto__=CreateRoleUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.SexButton",bean.SexButton);
			View.regComponent("bean.HeadImage",bean.HeadImage);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(CreateRoleUI.uiView);
		}
		CreateRoleUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":218,"x":0,"skin":"createRole/di.png"}},{"type":"Label","props":{"y":232,"x":214,"width":210,"text":"创 建 角 色","height":24,"fontSize":24,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":711,"x":185,"width":278,"var":"tipLabel","height":30,"fontSize":20,"color":"#ff0000","align":"left"}},{"type":"TextInput","props":{"y":580,"x":191,"width":210,"var":"roleName","type":"text","promptColor":"#63889a","prompt":"请输入2-8个汉字","height":40,"fontSize":24,"color":"#85fed1","align":"center"}},{"type":"Button","props":{"y":657,"x":320,"var":"btn_login","stateNum":1,"skin":"createRole/btn_login.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":597,"x":431,"var":"btn_randomName","stateNum":1,"skin":"createRole/btn_random.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":749,"x":83,"var":"btn_boy","stateNum":1,"skin":"createRole/btn_sexnan.png","scaleY":0.75,"scaleX":0.75,"optionIndex":"left","anchorY":1,"anchorX":0.5,"runtime":"bean.SexButton"}},{"type":"Button","props":{"y":749,"x":559,"var":"btn_girl","stateNum":1,"skin":"createRole/btn_sexnv.png","optionIndex":"center","anchorY":1,"anchorX":0.5,"runtime":"bean.SexButton"}},{"type":"List","props":{"y":337,"width":640,"visible":false,"var":"headBoy","spaceX":10,"repeatY":1,"repeatX":3,"height":166,"centerX":0},"child":[{"type":"Image","props":{"y":83,"var":"head0_0","skin":"createRole/head0_0.png","optionIndex":"1","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}},{"type":"Image","props":{"y":83,"var":"head0_1","skin":"createRole/head0_1.png","optionIndex":"2","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}},{"type":"Image","props":{"y":83,"var":"head0_2","skin":"createRole/head0_2.png","optionIndex":"3","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}}]},{"type":"List","props":{"y":337,"width":640,"var":"headGirl","spaceX":10,"repeatY":1,"repeatX":3,"height":166,"centerX":0},"child":[{"type":"Image","props":{"y":83,"var":"head1_0","skin":"createRole/head1_0.png","optionIndex":"1","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}},{"type":"Image","props":{"y":83,"var":"head1_1","skin":"createRole/head1_1.png","optionIndex":"2","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}},{"type":"Image","props":{"y":83,"var":"head1_2","skin":"createRole/head1_2.png","optionIndex":"3","anchorY":0.5,"anchorX":0.5,"runtime":"bean.HeadImage"}}]},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return CreateRoleUI;
	})(View);
var DataLoadingUI=(function(_super){
		function DataLoadingUI(){
			

			DataLoadingUI.__super.call(this);
		}

		CLASS$(DataLoadingUI,'ui.DataLoadingUI',_super);
		var __proto__=DataLoadingUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ImageRotate",bean.ImageRotate);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(DataLoadingUI.uiView);
		}
		DataLoadingUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"hitTestPrior":true,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":393,"x":320,"skin":"menu/ld1.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":393,"x":320,"skin":"menu/ld3.png","isRotate":true,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.ImageRotate"}}]};
		return DataLoadingUI;
	})(View);
var DBQBHelpUI=(function(_super){
		function DBQBHelpUI(){
			

			DBQBHelpUI.__super.call(this);
		}

		CLASS$(DBQBHelpUI,'ui.DBQBHelpUI',_super);
		var __proto__=DBQBHelpUI.prototype;
		__proto__.createChildren=function(){
		    
			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(DBQBHelpUI.uiView);
		}
		DBQBHelpUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":217,"x":19,"skin":"dbqb/di_help.png"}},{"type":"Label","props":{"y":257,"x":154,"width":320,"text":"玩法说明","strokeColor":"#2e6579","stroke":4,"height":30,"fontSize":30,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":325,"x":33,"width":21,"text":"1","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":388,"x":33,"width":21,"text":"2","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":420,"x":33,"width":21,"text":"3","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":483,"x":33,"width":21,"text":"4","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":515,"x":33,"width":21,"text":"5","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":326,"x":65,"wordWrap":true,"width":543,"text":"夺宝奇兵是一种无尽关卡的玩法，怪物会随机出现，玩家需要从容不迫的应对。","leading":14,"height":50,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"left"}},{"type":"Label","props":{"y":389,"x":65,"wordWrap":true,"width":543,"text":"每成功击杀一个BOSS即可获得1个宝箱。","leading":14,"height":18,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"left"}},{"type":"Label","props":{"y":420,"x":65,"wordWrap":true,"width":543,"text":"在宝箱界面可以进行【开启宝箱】操作，每次开启宝箱都可随机获得一个奖品。","leading":14,"height":50,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"left"}},{"type":"Label","props":{"y":483,"x":65,"wordWrap":true,"width":543,"text":"夺宝奇兵玩法无次数限制，与普通关卡一样，死亡会扣除电力。","leading":14,"height":18,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"left"}},{"type":"Label","props":{"y":515,"x":65,"wordWrap":true,"width":543,"text":"战斗中，击杀怪物可以获得分数。战斗结束后根据分数进行排名。","leading":14,"height":18,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"left"}}]};
		return DBQBHelpUI;
	})(View);
var DeskUI=(function(_super){
		function DeskUI(){
			
		    this.icon_hook=null;
		    this.btn_dialogClose=null;
		    this.btn_receive=null;
		    this.btn_sendToDesk=null;

			DeskUI.__super.call(this);
		}

		CLASS$(DeskUI,'ui.DeskUI',_super);
		var __proto__=DeskUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(DeskUI.uiView);
		}
		DeskUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":185,"x":91,"skin":"share/di.png"}},{"type":"Image","props":{"y":160,"x":95,"skin":"share/tu.png"}},{"type":"Image","props":{"y":199,"x":193,"skin":"share/bt.png"}},{"type":"Image","props":{"y":581,"x":121,"skin":"share/di2.png"}},{"type":"Image","props":{"y":592,"x":153,"skin":"share/di2xin.png"}},{"type":"Image","props":{"y":624,"x":153,"skin":"share/di2xin.png"}},{"type":"Image","props":{"y":655,"x":153,"var":"icon_hook","skin":"share/di2xin.png"}},{"type":"Label","props":{"y":594,"x":188,"width":330,"text":"将游戏发送桌面","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":627,"x":188,"width":330,"text":"下次可更方便进入游戏哦！","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":657,"x":188,"width":330,"text":"首次发送奖励200钻石+5万积分","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Button","props":{"y":191,"x":540,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":746,"x":325,"visible":false,"var":"btn_receive","stateNum":1,"skin":"share/btn_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":746,"x":325,"var":"btn_sendToDesk","stateNum":1,"skin":"share/btn_sendToDesk.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]};
		return DeskUI;
	})(View);
var dialogStationUI=(function(_super){
		function dialogStationUI(){
			
		    this.stationList=null;
		    this.userStationList=null;

			dialogStationUI.__super.call(this);
		}

		CLASS$(dialogStationUI,'ui.dialogStationUI',_super);
		var __proto__=dialogStationUI.prototype;
		__proto__.createChildren=function(){
		    
			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(dialogStationUI.uiView);
		}
		dialogStationUI.uiView={"type":"View","props":{"width":600,"popupCenter":true,"height":600},"child":[{"type":"Image","props":{"skin":"station/diye.png","sizeGrid":"0,0,0,0","centerY":0,"centerX":0}},{"type":"Button","props":{"y":35,"x":476,"stateNum":1,"skin":"dialogRes/button_close.png","name":"btn_dialogClose"}},{"type":"List","props":{"y":143,"x":115,"width":192,"var":"stationList","vScrollBarSkin":"menu/vscroll.png","spaceY":9,"repeatY":9,"repeatX":1,"height":360},"child":[{"type":"Box","props":{"y":0,"x":0,"name":"render"},"child":[{"type":"Image","props":{"skin":"station/fu.png","name":"titleBg"}},{"type":"Label","props":{"x":9,"width":120,"text":"label","name":"stationName","height":33,"fontSize":24,"color":"#ffffff","align":"left"}},{"type":"Label","props":{"x":130,"width":50,"name":"stateName","height":33,"fontSize":26,"bold":true,"align":"center"}}]}]},{"type":"Image","props":{"y":97,"x":138.5,"skin":"station/biaoti2.png"}},{"type":"Image","props":{"y":24,"x":197,"skin":"station/biaoti.png"}},{"type":"List","props":{"y":207,"x":332,"width":180,"var":"userStationList","vScrollBarSkin":"menu/vscroll.png","spaceY":6,"repeatX":1,"height":255},"child":[{"type":"Box","props":{"y":0,"x":0,"name":"render"},"child":[{"type":"Image","props":{"skin":"station/fu2.png"}},{"type":"Label","props":{"x":10,"width":120,"name":"stationName","height":33,"fontSize":24,"color":"#ffffff","align":"left"}},{"type":"Label","props":{"x":120,"width":50,"name":"stateName","height":33,"fontSize":26,"align":"center"}},{"type":"Label","props":{"y":35,"x":15,"width":150,"name":"userInfo","height":20,"fontSize":16,"color":"#ea11d0","align":"center"}}]}]},{"type":"Image","props":{"y":162,"x":359,"skin":"station/biaoti3.png"}}]};
		return dialogStationUI;
	})(View);
var DuoBaoQiBingUI=(function(_super){
		function DuoBaoQiBingUI(){
			
		    this.bgImg=null;
		    this.menu=null;
		    this.btn_help=null;
		    this.chestOpen=null;
		    this.chest=null;
		    this.btn_back=null;
		    this.btn_friend=null;
		    this.btn_open=null;
		    this.btn_start=null;
		    this.chestNumber=null;
		    this.openLog=null;
		    this.chestSeleted=null;
		    this.chest0=null;
		    this.chest1=null;
		    this.chest2=null;
		    this.chest3=null;
		    this.chest4=null;
		    this.chest5=null;
		    this.chest6=null;
		    this.chest7=null;
		    this.chest8=null;
		    this.chest9=null;
		    this.chest10=null;
		    this.chest11=null;
		    this.rank=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.helpPanel=null;

			DuoBaoQiBingUI.__super.call(this);
		}

		CLASS$(DuoBaoQiBingUI,'ui.DuoBaoQiBingUI',_super);
		var __proto__=DuoBaoQiBingUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);
			View.regComponent("ui.RankDBQBUI",ui.RankDBQBUI);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.DBQBHelpUI",ui.DBQBHelpUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(DuoBaoQiBingUI.uiView);
		}
		DuoBaoQiBingUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Panel","props":{"y":94,"x":0,"width":640,"var":"menu","height":866},"child":[{"type":"Image","props":{"y":604,"x":5,"skin":"dbqb/di2.png"}},{"type":"Image","props":{"skin":"dbqb/di1.png"}},{"type":"Label","props":{"y":25,"x":212,"width":208,"text":"夺宝奇兵","strokeColor":"#0072ff","stroke":4,"height":32,"fontSize":32,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Button","props":{"y":41,"x":419,"var":"btn_help","stateNum":1,"skin":"dbqb/btn_help.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":266,"x":316,"visible":false,"var":"chestOpen","skin":"dbqb/boxk.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":266,"x":263,"var":"chest","skin":"attack/chest.png"}},{"type":"Button","props":{"y":802,"x":563,"var":"btn_back","stateNum":1,"skin":"dbqb/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":802,"x":83,"var":"btn_friend","stateNum":1,"skin":"dbqb/btn_friend.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":410,"x":317,"var":"btn_open","stateNum":1,"skin":"dbqb/btn_open.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":803,"x":323,"var":"btn_start","stateNum":1,"skin":"dbqb/btn_start.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":353,"x":228,"width":127,"text":"宝箱数量:","strokeColor":"#0072ff","stroke":2,"height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"Label","props":{"y":351,"x":361,"width":65,"var":"chestNumber","text":"0","height":22,"fontSize":22,"font":"SimHei","color":"#ffe400","bold":true,"align":"left"}},{"type":"List","props":{"y":626,"x":42,"width":580,"var":"openLog","vScrollBarSkin":"menu/vscroll.png","spaceY":1,"spaceX":0,"repeatY":4,"repeatX":1,"height":104},"child":[{"type":"Box","props":{"y":0,"x":0,"width":580,"name":"render","height":20},"child":[{"type":"HTMLDivElement","props":{"width":580,"name":"logText","height":20}}]}]},{"type":"Image","props":{"y":55,"x":165,"var":"chestSeleted","skin":"dbqb/den.png"}},{"type":"Image","props":{"y":79,"x":167,"var":"chest0","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":79,"x":267,"var":"chest1","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":79,"x":366,"var":"chest2","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":180,"x":466,"var":"chest3","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":279,"x":466,"var":"chest4","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":381,"x":466,"var":"chest5","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":482,"x":366,"var":"chest6","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":482,"x":267,"var":"chest7","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":482,"x":167,"var":"chest8","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":382,"x":65,"var":"chest9","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":280,"x":65,"var":"chest10","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":180,"x":65,"var":"chest11","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":-3,"x":67,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":68,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":72,"x":12,"width":30,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]}]},{"type":"RankDBQB","props":{"y":0,"x":0,"visible":false,"var":"rank","runtime":"ui.RankDBQBUI"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"DBQBHelp","props":{"visible":false,"var":"helpPanel","runtime":"ui.DBQBHelpUI"}}]};
		return DuoBaoQiBingUI;
	})(View);
var EmailDialogUI=(function(_super){
		function EmailDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.emailNumber=null;
		    this.emailList=null;
		    this.btn_receive=null;
		    this.textDialog=null;
		    this.reward=null;
		    this.dataLoading=null;

			EmailDialogUI.__super.call(this);
		}

		CLASS$(EmailDialogUI,'ui.EmailDialogUI',_super);
		var __proto__=EmailDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);
			View.regComponent("ui.EmailTextDialogUI",ui.EmailTextDialogUI);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(EmailDialogUI.uiView);
		}
		EmailDialogUI.uiView={"type":"View","props":{"width":640,"visible":false,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":71,"x":22,"width":613,"skin":"backpack/di1_3.png","sizeGrid":"150,120,150,120","height":746}},{"type":"Button","props":{"y":80,"x":592,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":97,"x":45,"skin":"rank/icon0.png"}},{"type":"Label","props":{"y":105,"x":111,"width":51,"text":"邮箱:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac"}},{"type":"Label","props":{"y":106,"x":162,"width":100,"var":"emailNumber","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":107,"x":420,"width":166,"text":"*邮件只能保留7天","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"List","props":{"y":146,"x":45,"width":569,"var":"emailList","vScrollBarSkin":"menu/vscroll.png","spaceY":12,"repeatY":6,"repeatX":1,"height":652},"child":[{"type":"Box","props":{"name":"render","height":98},"child":[{"type":"Image","props":{"visible":true,"skin":"rank/lantiao1.png","name":"bg"}},{"type":"Image","props":{"y":4,"x":8,"visible":true,"skin":"rank/icon2.png","name":"icon"}},{"type":"Label","props":{"y":9,"x":110,"width":166,"visible":true,"name":"task_name","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"left"}},{"type":"HTMLDivElement","props":{"y":38,"x":126,"width":325,"name":"task_text","height":49}},{"type":"Label","props":{"y":16,"x":472,"width":82,"visible":true,"name":"dayText","height":18,"fontSize":18,"font":"SimHei","color":"#000000","bold":true,"align":"left"}},{"type":"Label","props":{"y":15,"x":442,"width":30,"visible":true,"name":"task_day","height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","bold":true,"align":"right"}}]}]},{"type":"Button","props":{"y":888,"x":320,"var":"btn_receive","stateNum":1,"skin":"dialogRes/btn_receive.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"EmailTextDialog","props":{"y":0,"x":0,"visible":false,"var":"textDialog","runtime":"ui.EmailTextDialogUI"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return EmailDialogUI;
	})(View);
var EmailTextDialogUI=(function(_super){
		function EmailTextDialogUI(){
			
		    this.bg=null;
		    this.btn_dialogClose=null;
		    this.goodsList=null;
		    this.goodsTitile=null;
		    this.btn_receive=null;
		    this.sendName=null;
		    this.emailText=null;

			EmailTextDialogUI.__super.call(this);
		}

		CLASS$(EmailTextDialogUI,'ui.EmailTextDialogUI',_super);
		var __proto__=EmailTextDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(EmailTextDialogUI.uiView);
		}
		EmailTextDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":194,"x":68,"width":513,"var":"bg","skin":"dialogRes/di2.png","sizeGrid":"50,105,60,55","height":471}},{"type":"Image","props":{"y":207,"x":200,"skin":"dialogRes/bt.png"}},{"type":"Button","props":{"y":203,"x":554,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":288,"x":85,"skin":"rank/lanTask.png"}},{"type":"List","props":{"y":526,"x":86,"width":536,"var":"goodsList","scaleY":0.9,"scaleX":0.9,"repeatY":1,"repeatX":5,"height":128,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"y":0,"x":0,"width":106,"name":"render","height":113},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":51,"visible":true,"skin":"backpack/shuliangkuang.png"}},{"type":"Label","props":{"y":78,"x":53,"width":40,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#000000","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Label","props":{"y":506,"x":101,"width":70,"var":"goodsTitile","text":"附件:","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","bold":true}},{"type":"Button","props":{"y":719,"x":320,"var":"btn_receive","stateNum":1,"skin":"dialogRes/btn_receive_email.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":470,"x":404,"width":124,"var":"sendName","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","align":"right"}},{"type":"HTMLDivElement","props":{"y":312,"x":111,"width":420,"var":"emailText","height":150}}]};
		return EmailTextDialogUI;
	})(View);
var FlashFigterUI=(function(_super){
		function FlashFigterUI(){
			
		    this.light=null;
		    this.star=null;
		    this.title=null;
		    this.figterIcon=null;

			FlashFigterUI.__super.call(this);
		}

		CLASS$(FlashFigterUI,'ui.FlashFigterUI',_super);
		var __proto__=FlashFigterUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.FlashScale",bean.FlashScale);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(FlashFigterUI.uiView);
		}
		FlashFigterUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":471,"x":342,"var":"light","toRotate":true,"skin":"flashRes/huan.png","delayTime":100,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":487,"x":338,"var":"star","toRotate":false,"skin":"flashRes/huanxin.png","delayTime":200,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":230,"x":330,"var":"title","toRotate":false,"skin":"flashRes/bt2.png","delayTime":0,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":395,"x":320,"var":"figterIcon","toRotate":false,"skin":"menu/1001.png","delayTime":300,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}}]};
		return FlashFigterUI;
	})(View);
var FlashLevelUpUI=(function(_super){
		function FlashLevelUpUI(){
			
		    this.light=null;
		    this.star=null;
		    this.levelBg=null;
		    this.levelOld=null;
		    this.levelNew=null;
		    this.levelUp=null;
		    this.head=null;
		    this.investment=null;

			FlashLevelUpUI.__super.call(this);
		}

		CLASS$(FlashLevelUpUI,'ui.FlashLevelUpUI',_super);
		var __proto__=FlashLevelUpUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.FlashScale",bean.FlashScale);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(FlashLevelUpUI.uiView);
		}
		FlashLevelUpUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":426,"x":341,"var":"light","toRotate":true,"skin":"flashRes/huan.png","delayTime":100,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":428,"x":339,"var":"star","toRotate":false,"skin":"flashRes/huanxin.png","delayTime":200,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":576,"x":357,"var":"levelBg","toRotate":false,"skin":"flashRes/sjtiao.png","delayTime":400,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"},"child":[{"type":"Label","props":{"y":87,"x":26.999999999999858,"width":40,"var":"levelOld","height":20,"fontSize":36,"font":"myFontBlue","align":"center"}},{"type":"Label","props":{"y":87,"x":324.99999999999994,"width":40,"var":"levelNew","height":20,"fontSize":36,"font":"myFontBlue","align":"center"}}]},{"type":"Image","props":{"y":227,"x":330,"var":"levelUp","toRotate":false,"skin":"flashRes/bt11.png","delayTime":0,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":417,"x":331,"var":"head","toRotate":false,"skin":"createRole/head0_0.png","delayTime":300,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Panel","props":{"y":668,"x":202,"width":252,"visible":false,"var":"investment","height":165},"child":[{"type":"Image","props":{"skin":"flashRes/levelupicon2.png"}},{"type":"Image","props":{"y":44,"x":54,"skin":"flashRes/levelupicon.png"}}]}]};
		return FlashLevelUpUI;
	})(View);
var GameUI=(function(_super){
		function GameUI(){
			
		    this.upBgPanel=null;
		    this.scoreIcon=null;
		    this.button_pause=null;
		    this.jn_time_bg=null;
		    this.jn_time_title0=null;
		    this.jn_time_title1=null;
		    this.scoreLabel=null;
		    this.hpBar=null;
		    this.chest=null;
		    this.chestNumberLable=null;
		    this.physicalLable=null;
		    this.chapterLable=null;
		    this.boss_hp=null;
		    this.jnXW=null;
		    this.jnNumber=null;
		    this.batterImg=null;
		    this.batterLabel=null;
		    this.skillPanel=null;
		    this.button_kzhzhy=null;
		    this.skillNumber_kzhzhy=null;
		    this.button_ltjg=null;
		    this.skillNumber_ltjg=null;
		    this.bossWarning=null;
		    this.boxShow=null;
		    this.boxImg=null;
		    this.readyBg=null;
		    this.readygo=null;
		    this.chapterBg=null;
		    this.chapterId=null;
		    this.readyJN_0=null;
		    this.readyJN_1=null;
		    this.reward=null;
		    this.maskSprite=null;
		    this.jnPanel=null;
		    this.ani_jn_0_0=null;
		    this.ani_jn_0_1=null;
		    this.ani_jn_1_0=null;
		    this.ani_jn_1_1=null;
		    this.btn_goJn=null;
		    this.btn_noJn=null;
		    this.jnPanelPass=null;
		    this.ani_jn_pass_0=null;
		    this.ani_jn_pass_1=null;
		    this.btn_jnOver=null;
		    this.btn_jnBack=null;
		    this.jnPanelFail=null;
		    this.ani_jn_fail0_0=null;
		    this.ani_jn_fail0_1=null;
		    this.btn_jnOver_fail0=null;
		    this.btn_jnBack_fail=null;
		    this.ani_jn_fail1_0=null;
		    this.ani_jn_fail1_1=null;
		    this.btn_jnOver_fail1=null;
		    this.overBg=null;
		    this.overFalsh=null;
		    this.scoreIconOver=null;
		    this.expBg=null;
		    this.chestBg=null;
		    this.levelLine=null;
		    this.scoreAddition=null;
		    this.scoreAdd=null;
		    this.chapterIdMax=null;
		    this.levelGuan=null;
		    this.levelTitle=null;
		    this.level_di=null;
		    this.best_dbqb=null;
		    this.chestImg=null;
		    this.chestX=null;
		    this.chestNumber=null;
		    this.bestBg=null;
		    this.chestTitle=null;
		    this.bestScore=null;
		    this.overTitle=null;
		    this.overTitle_dbqb=null;
		    this.goodsList=null;
		    this.btn_over=null;
		    this.btn_share=null;
		    this.btn_over_dbqb=null;
		    this.dbqb_rank=null;
		    this.myBg=null;
		    this.myName=null;
		    this.myScore=null;
		    this.myHead=null;
		    this.otherBg=null;
		    this.otherHead=null;
		    this.otherName=null;
		    this.otherScore=null;
		    this.rankTitle=null;
		    this.btn_share_rank=null;
		    this.btn_over_dbqb_rank=null;
		    this.resourcesInfo=null;
		    this.pauseBg=null;
		    this.radio_music=null;
		    this.radio_sound=null;
		    this.btn_exit=null;
		    this.btn_continue=null;
		    this.topNotice=null;
		    this.noticeText=null;
		    this.dataLoading=null;

			GameUI.__super.call(this);
		}

		CLASS$(GameUI,'ui.GameUI',_super);
		var __proto__=GameUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.LevelUpHide",bean.LevelUpHide);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("bean.FlashScale",bean.FlashScale);
			View.regComponent("bean.ImageMove",bean.ImageMove);
			View.regComponent("ui.ResourcesInfoUI",ui.ResourcesInfoUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(GameUI.uiView);
		}
		GameUI.uiView={"type":"View","props":{"width":640,"optionIndex":"out","height":960},"child":[{"type":"Panel","props":{"y":0,"x":0,"width":640,"var":"upBgPanel","height":225,"cacheAs":"bitmap"},"child":[{"type":"Image","props":{"y":13,"x":350,"var":"scoreIcon","skin":"attack/jinbi.png"}},{"type":"Button","props":{"y":94,"x":598,"var":"button_pause","stateNum":1,"skin":"attack/button_pause.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":12,"x":489,"visible":false,"var":"jn_time_bg","skin":"attack/time.png"}},{"type":"Label","props":{"y":29,"x":480,"width":88,"visible":false,"var":"jn_time_title0","text":"倒计时","strokeColor":"#000e94","stroke":4,"height":20,"fontSize":20,"font":"SimHei","color":"#ffe400","align":"right"}},{"type":"Label","props":{"y":23,"x":568,"width":44,"visible":false,"var":"jn_time_title1","height":30,"fontSize":30,"font":"SimHei","color":"#00ff00","align":"center"}},{"type":"Label","props":{"y":23,"x":409,"width":346,"var":"scoreLabel","scaleY":0.65,"scaleX":0.65,"height":55,"fontSize":55,"font":"myFontGameScore","color":"#ffffff","align":"left"}},{"type":"ProgressBar","props":{"y":6,"x":0.9999999999999822,"var":"hpBar","skin":"attack/progressBar.png","sizeGrid":"23,40,44,74,1"}},{"type":"Image","props":{"y":58,"x":146,"visible":false,"var":"chest","skin":"attack/boxzd.png"}},{"type":"Label","props":{"y":59,"x":191,"width":64,"visible":false,"var":"chestNumberLable","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":63,"x":33,"width":38,"var":"physicalLable","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":57,"x":147.99999999999997,"width":100,"var":"chapterLable","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"ProgressBar","props":{"y":80,"x":217,"visible":false,"var":"boss_hp","skin":"attack/progressBar_boss.png","sizeGrid":"17,1,1,2,1"}},{"type":"Image","props":{"y":56,"x":149,"visible":false,"var":"jnXW","skin":"attack/icondiku.png"}},{"type":"Label","props":{"y":59,"x":191,"width":64,"visible":false,"var":"jnNumber","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Image","props":{"y":263,"x":9,"visible":false,"var":"batterImg","skin":"attack/lianji.png","anchorY":0.5}},{"type":"Label","props":{"y":262,"x":100,"width":400,"visible":false,"var":"batterLabel","height":52,"fontSize":52,"font":"myFontBattleNumber","anchorY":0.5}},{"type":"Panel","props":{"y":705,"x":0,"width":170,"var":"skillPanel","height":255},"child":[{"type":"Button","props":{"y":67,"x":87,"var":"button_kzhzhy","stateNum":1,"skin":"attack/wsparticle_daoju2.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":-9,"skin":"attack/jinengshumu.png"}},{"type":"Label","props":{"y":2,"x":-7,"width":25,"var":"skillNumber_kzhzhy","height":15,"fontSize":15,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Button","props":{"y":170,"x":86,"var":"button_ltjg","stateNum":1,"skin":"attack/wsparticle_daoju1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":-9,"skin":"attack/jinengshumu.png"}},{"type":"Label","props":{"y":2,"x":-7,"width":25,"var":"skillNumber_ltjg","height":15,"fontSize":15,"font":"SimHei","color":"#ffffff","align":"center"}}]}]},{"type":"Image","props":{"y":229.99999999999997,"x":0,"visible":false,"var":"bossWarning","skin":"attack/bosswarning.png"}},{"type":"Animation","props":{"visible":false,"var":"boxShow","source":"Ani_box.ani"}},{"type":"Image","props":{"y":40,"x":50,"visible":false,"var":"boxImg","skin":"attack/chest.png","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":367,"x":0,"visible":false,"var":"readyBg","skin":"attack/ready.png"}},{"type":"Image","props":{"y":354,"x":227,"visible":false,"var":"readygo","skin":"attack/readygo.png"}},{"type":"Image","props":{"y":303.00000000000006,"x":42.000000000000036,"visible":false,"var":"chapterBg","skin":"attack/di.png"},"child":[{"type":"Image","props":{"y":7,"x":183,"skin":"attack/lantiaoz2.png"}},{"type":"Image","props":{"y":7,"x":335,"skin":"attack/lantiaoz1.png"}},{"type":"Label","props":{"y":10,"x":234,"width":100,"var":"chapterId","height":43,"fontSize":43,"font":"myFontRankScore","align":"center"}}]},{"type":"Image","props":{"y":315,"x":320,"visible":false,"var":"readyJN_0","skin":"attack/tan3 (2).png","pivotX":580,"anchorY":0.5}},{"type":"Image","props":{"y":315,"x":320,"visible":false,"var":"readyJN_1","skin":"attack/tan3 (2).png","pivotX":580,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"Sprite","props":{"y":481,"x":320,"width":640,"visible":false,"var":"maskSprite","scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.8},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"height":960,"fillColor":"#000000"}}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"jnPanel","height":960},"child":[{"type":"Image","props":{"y":332,"x":320,"visible":false,"var":"ani_jn_0_0","skin":"attack/tan1.png","pivotX":595,"anchorY":0.5}},{"type":"Image","props":{"y":332,"x":320,"visible":false,"var":"ani_jn_0_1","skin":"attack/tan1.png","pivotX":595,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Image","props":{"y":311,"x":320,"visible":false,"var":"ani_jn_1_0","skin":"attack/tan2.png","pivotX":580,"anchorY":0.5}},{"type":"Image","props":{"y":311,"x":320,"visible":false,"var":"ani_jn_1_1","skin":"attack/tan2.png","pivotX":580,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Button","props":{"y":545,"x":460,"visible":false,"var":"btn_goJn","stateNum":1,"skin":"attack/btn_goJn.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":545,"x":187,"visible":false,"var":"btn_noJn","stateNum":1,"skin":"attack/btn_noJn.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"jnPanelPass","height":960},"child":[{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_pass_0","skin":"attack/tan3 (1).png","pivotX":580,"anchorY":0.5}},{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_pass_1","skin":"attack/tan3 (1).png","pivotX":580,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Button","props":{"y":545,"x":190,"visible":false,"var":"btn_jnOver","stateNum":1,"skin":"attack/btn_jnOver.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":545,"x":451,"visible":false,"var":"btn_jnBack","stateNum":1,"skin":"attack/btn_goJn.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"jnPanelFail","height":960},"child":[{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_fail0_0","skin":"attack/tan3 (3).png","pivotX":580,"anchorY":0.5}},{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_fail0_1","skin":"attack/tan3 (3).png","pivotX":580,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Button","props":{"y":545,"x":190,"visible":false,"var":"btn_jnOver_fail0","stateNum":1,"skin":"attack/btn_jnOver.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":545,"x":451,"visible":false,"var":"btn_jnBack_fail","stateNum":1,"skin":"attack/btn_goJn.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_fail1_0","skin":"attack/tan3 (4).png","pivotX":580,"anchorY":0.5}},{"type":"Image","props":{"y":289,"x":320,"visible":false,"var":"ani_jn_fail1_1","skin":"attack/tan3 (4).png","pivotX":580,"anchorY":0.5,"runtime":"bean.LevelUpHide"}},{"type":"Button","props":{"y":545,"x":320,"visible":false,"var":"btn_jnOver_fail1","stateNum":1,"skin":"attack/btn_jnOver.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":1,"x":0,"width":640,"visible":false,"var":"overBg","height":960},"child":[{"type":"Panel","props":{"y":210,"x":116.00000000000003,"width":520,"var":"overFalsh","height":665},"child":[{"type":"Image","props":{"skin":"attack/diGame.png"}},{"type":"Image","props":{"y":154,"skin":"attack/dilight.png"}},{"type":"Image","props":{"y":224,"x":12,"var":"scoreIconOver","skin":"attack/jinbi.png"}},{"type":"Image","props":{"y":350,"x":14,"var":"expBg","skin":"attack/di2.png"}},{"type":"Image","props":{"y":350,"x":14,"var":"chestBg","skin":"attack/di1.png"}},{"type":"Image","props":{"y":566,"x":14,"var":"levelLine","skin":"attack/di3.png"}},{"type":"Label","props":{"y":123,"x":115,"width":100,"text":"分数加成","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":123,"x":221,"width":72,"var":"scoreAddition","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Label","props":{"y":221,"x":64,"width":280,"var":"scoreAdd","height":55,"fontSize":55,"font":"myFontGameScore","align":"center"}},{"type":"Label","props":{"y":574,"x":179,"width":50,"var":"chapterIdMax","height":25,"fontSize":25,"font":"SimHei","color":"#ff4593","align":"center"}},{"type":"Label","props":{"y":574,"x":226,"width":50,"var":"levelGuan","text":"关","height":24,"fontSize":24,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Label","props":{"y":542,"x":101,"width":200,"var":"levelTitle","text":"历史最高通关","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Label","props":{"y":574,"x":129,"width":50,"var":"level_di","text":"第","height":24,"fontSize":24,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Image","props":{"y":188,"x":349,"var":"best_dbqb","skin":"attack/iconlishi.png"}},{"type":"Image","props":{"y":429,"x":82,"var":"chestImg","skin":"attack/chest.png"}},{"type":"Label","props":{"y":443,"x":207,"width":53,"var":"chestX","text":"X","strokeColor":"#006f7f","stroke":6,"height":44,"fontSize":44,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":424,"x":248,"width":269,"var":"chestNumber","text":"3","strokeColor":"#006f7f","stroke":6,"height":80,"fontSize":80,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Image","props":{"y":561,"x":48,"var":"bestBg","skin":"attack/di2_dbqb.png"}},{"type":"Label","props":{"y":575,"x":-15,"width":200,"var":"chestTitle","text":"历史最高分","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":572,"x":206,"width":285,"var":"bestScore","height":22,"fontSize":22,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}}]},{"type":"Image","props":{"y":176,"x":317,"var":"overTitle","toRotate":false,"skin":"attack/bt1.png","delayTime":0,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"Image","props":{"y":163,"x":0,"var":"overTitle_dbqb","skin":"attack/bt_dbqb.png"}},{"type":"List","props":{"y":610.0000000000001,"x":320,"width":542,"var":"goodsList","repeatY":1,"repeatX":5,"height":120,"hScrollBarSkin":"attack/hscroll.png","anchorX":0.5},"child":[{"type":"Box","props":{"y":0,"x":-2,"width":110,"scaleY":0.9,"scaleX":0.9,"name":"render","height":120},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":78,"x":13,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Button","props":{"y":885,"x":325,"var":"btn_over","stateNum":1,"skin":"attack/btn_over.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":878,"x":442,"visible":false,"var":"btn_share","stateNum":1,"skin":"attack/btn_share.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":878,"x":192,"var":"btn_over_dbqb","stateNum":1,"skin":"attack/btn_over_dbqb.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"dbqb_rank","height":960},"child":[{"type":"Image","props":{"y":243,"x":25,"var":"myBg","to_y":243,"to_x":25,"src_y":243,"src_x":-500,"skin":"attack/di1_dbqb.png","runtime":"bean.ImageMove"},"child":[{"type":"Label","props":{"y":9,"x":166,"width":252,"var":"myName","height":16,"fontSize":16,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":70,"x":183,"width":261,"var":"myScore","height":40,"fontSize":40,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":42,"x":45,"width":72,"var":"myHead","height":76}}]},{"type":"Image","props":{"y":548,"x":131,"var":"otherBg","to_y":548,"to_x":131,"src_y":548,"src_x":656,"skin":"attack/di2dbqb.png","runtime":"bean.ImageMove"},"child":[{"type":"Image","props":{"y":33,"x":373,"width":72,"var":"otherHead","height":76}},{"type":"Label","props":{"y":113,"x":170,"width":252,"var":"otherName","height":16,"fontSize":16,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":49,"x":49,"width":261,"var":"otherScore","height":40,"fontSize":40,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Image","props":{"y":412,"x":92,"var":"rankTitle","to_y":412,"to_x":92,"src_y":512,"src_x":92,"skin":"attack/jtm2.png","runtime":"bean.ImageMove"}},{"type":"Button","props":{"y":878,"x":442,"visible":false,"var":"btn_share_rank","stateNum":1,"skin":"attack/btn_share.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":878,"x":192,"var":"btn_over_dbqb_rank","stateNum":1,"skin":"attack/btn_over_dbqb.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"ResourcesInfo","props":{"y":0,"x":0,"var":"resourcesInfo","runtime":"ui.ResourcesInfoUI"}},{"type":"Panel","props":{"y":0,"x":0,"width":640,"visible":false,"var":"pauseBg","height":960},"child":[{"type":"Image","props":{"y":551.9999999999999,"x":109,"width":430,"skin":"rank/diTip.png","sizeGrid":"30,40,30,40","height":144}},{"type":"Image","props":{"y":574,"x":141.99999999999997,"skin":"role/mbgm.png"}},{"type":"Image","props":{"y":639.9999999999999,"x":142.99999999999986,"skin":"role/mfxm.png"}},{"type":"Label","props":{"y":580.9999999999999,"x":192.99999999999986,"width":58,"text":"音乐","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":647.9999999999999,"x":192.99999999999986,"width":58,"text":"音效","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":585,"x":377.9999999999998,"width":58,"text":"开","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":647.9999999999999,"x":377.9999999999998,"width":58,"text":"开","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":585,"x":488.99999999999994,"width":58,"text":"关","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":647.9999999999999,"x":488.99999999999994,"width":58,"text":"关","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"RadioGroup","props":{"y":575,"x":335.00000000000006,"var":"radio_music","space":73,"skin":"role/radioGroup.png","labels":",","direction":"horizontal"}},{"type":"RadioGroup","props":{"y":637.9999999999998,"x":335.00000000000006,"var":"radio_sound","space":73,"skin":"role/radioGroup.png","labels":",","direction":"horizontal"}},{"type":"Image","props":{"y":193,"x":318,"skin":"attack/bt.png","anchorX":0.5}},{"type":"Button","props":{"y":438,"x":401,"var":"btn_exit","stateNum":1,"skin":"attack/btn_exit.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":438,"x":241,"var":"btn_continue","stateNum":1,"skin":"attack/btn_continue.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return GameUI;
	})(View);
var GiftRewardUI=(function(_super){
		function GiftRewardUI(){
			
		    this.gift_title_0=null;
		    this.gift_title_1=null;
		    this.btn_giftClose=null;
		    this.giftGoodsList=null;
		    this.gift_text0_0=null;
		    this.gift_text0_1=null;

			GiftRewardUI.__super.call(this);
		}

		CLASS$(GiftRewardUI,'ui.GiftRewardUI',_super);
		var __proto__=GiftRewardUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(GiftRewardUI.uiView);
		}
		GiftRewardUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.8},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":184,"x":71,"skin":"share/di_gift.png"}},{"type":"Image","props":{"y":206,"x":210,"var":"gift_title_0","skin":"share/bt1_gift.png"}},{"type":"Image","props":{"y":206,"x":134,"var":"gift_title_1","skin":"share/bt2_gift.png"}},{"type":"Button","props":{"y":197,"x":543,"var":"btn_giftClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"List","props":{"y":346,"x":101,"width":465,"var":"giftGoodsList","vScrollBarSkin":"menu/vscroll.png","spaceY":0,"spaceX":6,"repeatY":2,"repeatX":4,"height":265},"child":[{"type":"Box","props":{"y":0,"x":-2,"width":110,"scaleY":0.9,"scaleX":0.9,"name":"render","height":134},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":78,"x":13,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":110,"x":3,"width":100,"visible":true,"name":"name","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}}]}]},{"type":"Label","props":{"y":302,"x":132,"width":419,"var":"gift_text0_0","text":"恭喜你获得：","strokeColor":"#803f00","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffe400","align":"center"}},{"type":"Label","props":{"y":302,"x":132,"width":419,"var":"gift_text0_1","text":"你今天已经领取过：","strokeColor":"#803f00","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffe400","align":"center"}},{"type":"Label","props":{"y":625,"x":111,"wordWrap":true,"width":426,"text":"记得明天继续到","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Label","props":{"y":650,"x":111,"wordWrap":true,"width":426,"text":"玩吧礼包中心领取新的礼包哦！","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"center"}}]};
		return GiftRewardUI;
	})(View);
var GoodsInfoUI=(function(_super){
		function GoodsInfoUI(){
			
		    this.btn_dialogClose=null;
		    this.unitLabel=null;
		    this.goodsName=null;
		    this.goodsNumber=null;
		    this.goodsText=null;
		    this.quality=null;
		    this.icon=null;
		    this.number=null;
		    this.fragment=null;
		    this.qualityNumber=null;
		    this.starList=null;

			GoodsInfoUI.__super.call(this);
		}

		CLASS$(GoodsInfoUI,'ui.GoodsInfoUI',_super);
		var __proto__=GoodsInfoUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(GoodsInfoUI.uiView);
		}
		GoodsInfoUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":264,"x":72,"width":513,"skin":"backpack/zd.png","sizeGrid":"50,105,60,55","height":313}},{"type":"Button","props":{"y":272,"x":553,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":355,"x":108,"width":430,"skin":"rank/diTip.png","sizeGrid":"30,40,30,40","height":178}},{"type":"Label","props":{"y":370.99999999999994,"x":375.9999999999999,"width":28,"var":"unitLabel","text":"件","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Label","props":{"y":370.99999999999994,"x":240.9999999999999,"width":61,"text":"拥有数量","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":328,"x":241.00000000000003,"width":290,"var":"goodsName","height":20,"fontSize":20,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Image","props":{"y":366,"x":315.00000000000006,"skin":"backpack/slk.png"}},{"type":"Label","props":{"y":371,"x":316.99999999999994,"width":46,"var":"goodsNumber","height":16,"fontSize":16,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":430.99999999999994,"x":135.99999999999997,"wordWrap":true,"width":375,"var":"goodsText","height":95,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Image","props":{"y":309,"x":134,"visible":true,"var":"quality","skin":"quality/1.png"}},{"type":"Image","props":{"y":321,"x":149,"width":71,"var":"icon","height":74}},{"type":"Image","props":{"y":374,"x":147.99999999999994,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":377,"x":150,"width":29,"var":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":304,"x":202,"var":"fragment","skin":"quality/suipian.png"}},{"type":"Image","props":{"y":303,"x":186,"width":75,"var":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":397,"x":188,"width":80,"visible":true,"var":"starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]};
		return GoodsInfoUI;
	})(View);
var LevelDifficultyUI=(function(_super){
		function LevelDifficultyUI(){
			
		    this.btn_dialogClose=null;
		    this.chapterType=null;
		    this.levelTitle=null;
		    this.chapterId=null;
		    this.flopList=null;
		    this.btn_sally=null;

			LevelDifficultyUI.__super.call(this);
		}

		CLASS$(LevelDifficultyUI,'ui.LevelDifficultyUI',_super);
		var __proto__=LevelDifficultyUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(LevelDifficultyUI.uiView);
		}
		LevelDifficultyUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.6},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":211,"x":18,"skin":"world/di.png"}},{"type":"Button","props":{"y":230,"x":575,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":251,"x":52,"var":"chapterType","skin":"world/btnorm.png"}},{"type":"Image","props":{"y":275,"x":73,"skin":"world/diguan.png"}},{"type":"Label","props":{"y":270,"x":69,"width":338,"var":"levelTitle","height":22,"fontSize":22,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":334,"x":219,"width":50,"text":"第","height":30,"fontSize":30,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":334,"x":380,"width":50,"text":"关","height":30,"fontSize":30,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":379,"x":206,"width":229,"text":"胜利可能掉落","name":"teshudiaoluo","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Label","props":{"y":327,"x":274,"width":105,"var":"chapterId","height":39,"fontSize":39,"font":"myFontChapter","align":"center"}},{"type":"List","props":{"y":408,"x":58,"width":551,"var":"flopList","repeatY":1,"repeatX":5,"height":140,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"y":0,"x":-1,"width":110,"scaleY":0.9,"scaleX":0.9,"name":"render","height":134},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":110,"x":3,"width":100,"visible":true,"name":"name","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}}]}]},{"type":"Button","props":{"y":586,"x":315,"var":"btn_sally","stateNum":1,"skin":"world/btn_sally.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]};
		return LevelDifficultyUI;
	})(View);
var MarketUI=(function(_super){
		function MarketUI(){
			
		    this.bgImg=null;
		    this.check_score=null;
		    this.check_diamond=null;
		    this.scoreList=null;
		    this.diamondList=null;
		    this.updateTime=null;
		    this.btn_back=null;
		    this.btn_backpack=null;
		    this.btn_refresh=null;
		    this.priceBtnLable=null;
		    this.priceBtnIcon=null;
		    this.buyDialog=null;
		    this.dataLoading=null;

			MarketUI.__super.call(this);
		}

		CLASS$(MarketUI,'ui.MarketUI',_super);
		var __proto__=MarketUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.ShopBuyUI",ui.ShopBuyUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(MarketUI.uiView);
		}
		MarketUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":93,"x":19,"skin":"role/di_m.png"}},{"type":"CheckBox","props":{"y":148,"x":117,"var":"check_score","stateNum":2,"skin":"role/check_score.png","anchorY":0.5,"anchorX":0.5}},{"type":"CheckBox","props":{"y":148,"x":267,"var":"check_diamond","stateNum":2,"skin":"role/check_diamond.png","anchorY":0.5,"anchorX":0.5}},{"type":"List","props":{"y":202,"x":72,"width":500,"var":"scoreList","vScrollBarSkin":"menu/vscroll.png","spaceY":16,"spaceX":7,"repeatY":3,"repeatX":3,"height":569},"child":[{"type":"Box","props":{"y":0,"x":0,"width":155,"name":"render","height":170},"child":[{"type":"Image","props":{"visible":true,"skin":"role/diwp.png"}},{"type":"Label","props":{"y":10,"x":28,"width":120,"name":"name","italic":true,"height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":152,"x":5,"visible":true,"skin":"role/jllan2a.png"}},{"type":"Image","props":{"y":160,"x":21,"visible":true,"skin":"tool/3001.png","scaleY":0.5,"scaleX":0.5,"name":"priceIcon","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":155,"x":36,"width":74,"visible":true,"name":"price","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Label","props":{"y":135,"x":36,"width":74,"visible":true,"strokeColor":"#fffc00","stroke":1,"name":"price_new","height":16,"fontSize":16,"font":"SimHei","color":"#ff0000","align":"center"}},{"type":"Image","props":{"y":162,"x":53,"visible":true,"skin":"role/zehx.png","name":"discountLine"}},{"type":"Image","props":{"y":35,"x":25,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":47,"x":40,"width":71,"visible":true,"name":"icon","height":74}},{"type":"Image","props":{"y":30,"x":93,"visible":true,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":29,"x":77,"width":75,"visible":true,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":123,"x":79,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":25,"x":127.99999999999997,"visible":true,"skin":"role/ze1.png","name":"discountBg"}},{"type":"Label","props":{"y":52.00000000000003,"x":133.9999999999998,"wordWrap":true,"width":25,"name":"discountText","height":42,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":0,"x":0,"visible":true,"skin":"role/disq.png","name":"sellOver"}}]}]},{"type":"List","props":{"y":202,"x":72.00000000000004,"width":500,"var":"diamondList","vScrollBarSkin":"menu/vscroll.png","spaceY":16,"spaceX":7,"repeatY":3,"repeatX":3,"height":569},"child":[{"type":"Box","props":{"y":0,"x":0,"width":155,"name":"render","height":170},"child":[{"type":"Image","props":{"visible":true,"skin":"role/diwp.png"}},{"type":"Label","props":{"y":10,"x":28,"width":120,"name":"name","italic":true,"height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":152,"x":5,"visible":true,"skin":"role/jllan2a.png"}},{"type":"Image","props":{"y":160,"x":21,"visible":true,"skin":"tool/3001.png","scaleY":0.5,"scaleX":0.5,"name":"priceIcon","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":155,"x":36,"width":74,"visible":true,"name":"price","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Label","props":{"y":135,"x":36,"width":74,"visible":true,"strokeColor":"#fffc00","stroke":1,"name":"price_new","height":16,"fontSize":16,"font":"SimHei","color":"#ff0000","align":"center"}},{"type":"Image","props":{"y":162,"x":53,"visible":true,"skin":"role/zehx.png","name":"discountLine"}},{"type":"Image","props":{"y":35,"x":25,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":47,"x":40,"width":71,"visible":true,"name":"icon","height":74}},{"type":"Image","props":{"y":30,"x":93,"visible":true,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":29,"x":77,"width":75,"visible":true,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":123,"x":79,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":25,"x":127.99999999999997,"visible":true,"skin":"role/ze1.png","name":"discountBg"}},{"type":"Label","props":{"y":52.00000000000003,"x":133.9999999999998,"wordWrap":true,"width":25,"name":"discountText","height":42,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":0,"x":0,"visible":true,"skin":"role/disq.png","name":"sellOver"}}]}]},{"type":"Label","props":{"y":776,"x":173,"width":90,"text":"每天早上","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":776,"x":316,"width":157,"text":"折扣会自动刷新","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":777,"x":266,"wordWrap":true,"width":50,"var":"updateTime","text":"5:00","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","align":"center"}},{"type":"Button","props":{"y":893,"x":551,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":893,"x":82,"var":"btn_backpack","stateNum":1,"skin":"warehouse/btn_backpack.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":884,"x":320,"var":"btn_refresh","stateNum":1,"skin":"warehouse/btn_buy.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":22,"x":127,"width":107,"var":"priceBtnLable","strokeColor":"#4c0c2f","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":22,"x":14,"width":80,"text":"刷新","strokeColor":"#ffd600","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#9e4500","bold":true,"align":"center"}},{"type":"Image","props":{"y":36,"x":105,"var":"priceBtnIcon","skin":"menu/icon_diamond.png","anchorY":0.5,"anchorX":0.5}}]},{"type":"ShopBuy","props":{"y":0,"x":0,"visible":false,"var":"buyDialog","runtime":"ui.ShopBuyUI"}},{"type":"DataLoading","props":{"y":10,"x":10,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return MarketUI;
	})(View);
var MenuUI=(function(_super){
		function MenuUI(){
			
		    this.bgImg=null;
		    this.menuPanel=null;
		    this.fighterIcon=null;
		    this.menu_top=null;
		    this.btn_shouchong=null;
		    this.btn_meirichongzhi=null;
		    this.new_dailyPay=null;
		    this.btn_zaixian=null;
		    this.onlineTimeLabel=null;
		    this.new_zaixian=null;
		    this.btn_activity=null;
		    this.new_activity=null;
		    this.btn_jiNiang=null;
		    this.jiNiangTime=null;
		    this.btn_dbqb=null;
		    this.menu_bottom=null;
		    this.infoBg_main=null;
		    this.infoBg_armo=null;
		    this.infoBg_sub=null;
		    this.infoBg_wingman=null;
		    this.war_weaponMain=null;
		    this.war_armo=null;
		    this.war_weaponSub=null;
		    this.war_wingman=null;
		    this.fighterName=null;
		    this.starList=null;
		    this.attack_weapon_main=null;
		    this.hp_armo=null;
		    this.attack_weapon_sub=null;
		    this.attack_wingman=null;
		    this.btn_rank=null;
		    this.btn_task=null;
		    this.new_task=null;
		    this.btn_begin=null;
		    this.btn_fighter=null;
		    this.new_btn_fighter=null;
		    this.btn_badge=null;
		    this.btn_qiandao=null;
		    this.new_qiandao=null;
		    this.btn_chongzhi=null;
		    this.new_pay=null;
		    this.btn_email=null;
		    this.new_email=null;
		    this.btn_heishi=null;
		    this.new_market=null;
		    this.btn_shangdian=null;
		    this.btn_friend=null;
		    this.btn_desk=null;
		    this.btn_diamond=null;
		    this.warehousePanel=null;
		    this.fighterIconList=null;
		    this.fighter_name_bg=null;
		    this.fighter_warehouse=null;
		    this.starList_warehouse=null;
		    this.war_type=null;
		    this.wh_infoBg_wingman=null;
		    this.wh_infoBg_sub=null;
		    this.wh_infoBg_armo=null;
		    this.wh_infoBg_main=null;
		    this.wh_weaponMain=null;
		    this.wh_armo=null;
		    this.wh_weaponSub=null;
		    this.wh_wingman=null;
		    this.wh_attack_wingman=null;
		    this.wh_attack_weapon_sub=null;
		    this.wh_hp_armo=null;
		    this.wh_attack_weapon_main=null;
		    this.combatLabel=null;
		    this.btn_war=null;
		    this.arrow_left=null;
		    this.arrow_right=null;
		    this.indexList=null;
		    this.badge=null;
		    this.roleInfo=null;
		    this.warehouseBottom=null;
		    this.btn_weapon_main=null;
		    this.selectedImg_main=null;
		    this.weapon_main_icon=null;
		    this.weapon_main_starList=null;
		    this.weapon_main_qualityNumber=null;
		    this.weapon_main_strengthen=null;
		    this.new_btn_weapon_main=null;
		    this.btn_armo=null;
		    this.selectedImg_armo=null;
		    this.armo_icon=null;
		    this.armo_starList=null;
		    this.armo_qualityNumber=null;
		    this.armo_strengthen=null;
		    this.new_btn_armo=null;
		    this.btn_weapon_sub=null;
		    this.selectedImg_sub=null;
		    this.weapon_sub_icon=null;
		    this.weapon_sub_starList=null;
		    this.weapon_sub_qualityNumber=null;
		    this.weapon_sub_strengthen=null;
		    this.new_btn_weapon_sub=null;
		    this.btn_wingman=null;
		    this.selectedImg_wingman=null;
		    this.wingman_icon=null;
		    this.wingman_starList=null;
		    this.wingman_qualityNumber=null;
		    this.wingman_strengthen=null;
		    this.new_btn_wingman=null;
		    this.buy_fighter=null;
		    this.buy_fighter_icon=null;
		    this.buy_fighter_strengthen=null;
		    this.buy_fighter_name=null;
		    this.buy_fighter_text=null;
		    this.btn_backpack=null;
		    this.btn_back=null;
		    this.btn_buy=null;
		    this.priceBtnLable=null;
		    this.priceBtnIcon=null;
		    this.check_strengthen=null;
		    this.new_strengthen=null;
		    this.check_advance=null;
		    this.new_advance=null;
		    this.check_star=null;
		    this.new_star=null;
		    this.arrow_strengthen=null;
		    this.arrow_advance=null;
		    this.arrow_star=null;
		    this.upgradeStar=null;
		    this.starLevel=null;
		    this.star_attackLable=null;
		    this.starAttack=null;
		    this.starScore=null;
		    this.starScore_upgrade=null;
		    this.starAttack_upgrade=null;
		    this.star_attackLable_upgrade=null;
		    this.starLevel_upgrade=null;
		    this.star_qualityIcon=null;
		    this.star_icon=null;
		    this.star_starList=null;
		    this.star_qualityNumber=null;
		    this.starResources=null;
		    this.starSpendScore=null;
		    this.btn_star=null;
		    this.new_btn_star=null;
		    this.starBar=null;
		    this.starEquipmentName=null;
		    this.star0=null;
		    this.star1=null;
		    this.star2=null;
		    this.star3=null;
		    this.star4=null;
		    this.upgradeStrengthen=null;
		    this.strengthenLevel=null;
		    this.strengthen_attackLable=null;
		    this.strengthenAttack=null;
		    this.strengthenScore=null;
		    this.strengthenScore_upgrade=null;
		    this.strengthenAttack_upgrade=null;
		    this.strengthen_attackLable_upgrade=null;
		    this.strengthenLevel_upgrade=null;
		    this.strengthen_qualityIcon=null;
		    this.strengthen_icon=null;
		    this.strengthen_starList=null;
		    this.strengthen_qualityNumber=null;
		    this.strengthenSpendScore=null;
		    this.strengthenEquipmentName=null;
		    this.btn_strengthen_1=null;
		    this.new_btn_strengthen_1=null;
		    this.btn_strengthen_10=null;
		    this.new_btn_strengthen_10=null;
		    this.upgradeQuality=null;
		    this.qualityLevel=null;
		    this.quality_attackLable=null;
		    this.qualityAttack=null;
		    this.qualityScore=null;
		    this.qualityScore_upgrade=null;
		    this.qualityAttack_upgrade=null;
		    this.quality_attackLable_upgrade=null;
		    this.qualityLevel_upgrade=null;
		    this.quality_qualityIcon=null;
		    this.quality_icon=null;
		    this.quality_starList=null;
		    this.quality_qualityNumber=null;
		    this.quality_qualityIcon_next=null;
		    this.quality_icon_next=null;
		    this.quality_starList_next=null;
		    this.quality_qualityNumber_next=null;
		    this.qualityResources=null;
		    this.qualitySpendScore=null;
		    this.quality_equipmentName=null;
		    this.quality_equipmentName_next=null;
		    this.btn_advance=null;
		    this.new_btn_advance=null;
		    this.goodsInfo=null;
		    this.ani_levelUp0=null;
		    this.ani_levelUp1=null;
		    this.ani_levelUp2=null;
		    this.rank=null;
		    this.resourcesInfo=null;
		    this.reward=null;
		    this.giftReward=null;
		    this.flashFighter=null;
		    this.topNotice=null;
		    this.noticeText=null;
		    this.dialogLayer=null;
		    this.dataLoading=null;

			MenuUI.__super.call(this);
		}

		CLASS$(MenuUI,'ui.MenuUI',_super);
		var __proto__=MenuUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.RoundBg",bean.RoundBg);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("bean.FlashArrow",bean.FlashArrow);
			View.regComponent("ui.RoleInfoUI",ui.RoleInfoUI);
			View.regComponent("ui.GoodsInfoUI",ui.GoodsInfoUI);
			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.GiftRewardUI",ui.GiftRewardUI);
			View.regComponent("ui.FlashFigterUI",ui.FlashFigterUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);
			View.regComponent("ui.ResourcesInfoUI",ui.ResourcesInfoUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(MenuUI.uiView);
		}
		MenuUI.uiView={"type":"View","props":{"width":640,"source":"shield/shield0.png,shield/shield1.png,shield/shield2.png,shield/shield3.png,shield/shield4.png,shield/shield5.png,shield/shield6.png,shield/shield7.png","height":960},"child":[{"type":"Image","props":{"var":"bgImg","skin":"menu/bg.jpg","centerY":0,"centerX":0},"child":[{"type":"Script","props":{"isRound":true,"runtime":"bean.RoundBg"}}]},{"type":"Panel","props":{"y":154,"x":0,"width":640,"var":"menuPanel","height":806},"child":[{"type":"Image","props":{"y":261,"x":0,"skin":"menu/di.png"}},{"type":"Image","props":{"y":100,"x":0,"var":"fighterIcon"}}]},{"type":"Panel","props":{"y":161,"x":0,"width":640,"var":"menu_top","height":400},"child":[{"type":"Button","props":{"y":48,"x":468,"var":"btn_shouchong","stateNum":1,"skin":"menu/btn_shouchong.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":52,"x":467,"visible":false,"var":"btn_meirichongzhi","stateNum":1,"skin":"menu/btn_meirichongzhi.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"var":"new_dailyPay","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":48,"x":346,"var":"btn_zaixian","stateNum":1,"skin":"menu/btn_zaixian.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":21,"x":15,"width":58,"var":"onlineTimeLabel","strokeColor":"#000000","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"visible":false,"var":"new_zaixian","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":46,"x":586,"visible":false,"var":"btn_activity","stateNum":1,"skin":"menu/btn_activity.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_activity","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":154,"x":78,"visible":false,"var":"btn_jiNiang","stateNum":1,"skin":"menu/btn_jiNiang.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":82,"x":2,"width":91,"var":"jiNiangTime","valign":"middle","strokeColor":"#ffffff","stroke":2,"height":20,"fontSize":20,"font":"SimHei","color":"#000000","align":"center"}}]},{"type":"Button","props":{"y":55,"x":77,"visible":false,"var":"btn_dbqb","stateNum":1,"skin":"menu/btn_dbqb.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":629,"x":0,"width":640,"var":"menu_bottom","height":331},"child":[{"type":"Image","props":{"y":103,"visible":false,"skin":"menu/shujudi.png"}},{"type":"Image","props":{"y":116,"x":328,"visible":false,"var":"infoBg_main","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":134,"x":328,"visible":false,"var":"infoBg_armo","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":153,"x":328,"visible":false,"var":"infoBg_sub","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":171,"x":328,"visible":false,"var":"infoBg_wingman","skin":"menu/shuzhizuidakuan.png"}},{"type":"List","props":{"y":118,"x":328,"width":325,"visible":false,"var":"war_weaponMain","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":136,"x":328,"width":325,"visible":false,"var":"war_armo","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":155,"x":328,"width":325,"visible":false,"var":"war_weaponSub","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":173,"x":328,"width":325,"visible":false,"var":"war_wingman","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"Label","props":{"y":128,"x":103,"width":166,"visible":false,"var":"fighterName","height":20,"fontSize":20,"font":"SimHei","color":"#00ff42","bold":true,"anchorX":0.5,"align":"center"}},{"type":"List","props":{"y":157,"x":103,"width":120,"visible":false,"var":"starList","spaceX":2,"repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":116,"x":207,"width":60,"visible":false,"text":"战机攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":134,"x":207,"width":60,"visible":false,"text":"装甲护值","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":152,"x":207,"width":72,"visible":false,"text":"副武器攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":170,"x":207,"width":60,"visible":false,"text":"僚机攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":116,"x":272,"width":50,"visible":false,"var":"attack_weapon_main","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":134,"x":272,"width":50,"visible":false,"var":"hp_armo","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":152,"x":279,"width":43,"visible":false,"var":"attack_weapon_sub","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":170,"x":272,"width":50,"visible":false,"var":"attack_wingman","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Button","props":{"y":249,"x":56,"var":"btn_rank","stateNum":1,"skin":"menu/btn_rank.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":251,"x":164,"var":"btn_task","stateNum":1,"skin":"menu/btn_task.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_task","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":266,"x":322,"var":"btn_begin","stateNum":1,"skin":"menu/btn_begin.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":249,"x":480,"var":"btn_fighter","stateNum":1,"skin":"menu/btn_fighter.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_fighter","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":251,"x":587,"var":"btn_badge","stateNum":1,"skin":"menu/btn_badge.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":181,"x":153,"var":"btn_qiandao","stateNum":1,"skin":"menu/btn_qiandao.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_qiandao","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":182,"x":237,"var":"btn_chongzhi","stateNum":1,"skin":"menu/btn_chongzhi.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":0,"visible":false,"var":"new_pay","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":182,"x":320,"var":"btn_email","stateNum":1,"skin":"menu/btn_email.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_email","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":183,"x":403,"var":"btn_heishi","stateNum":1,"skin":"menu/btn_heishi.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_market","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":183,"x":487,"var":"btn_shangdian","stateNum":1,"skin":"menu/btn_shangdian.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":116,"x":235,"visible":false,"var":"btn_friend","stateNum":1,"skin":"menu/btn_friend.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":116,"x":320,"visible":false,"var":"btn_desk","stateNum":1,"skin":"menu/btn_desk.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":116,"x":403,"visible":false,"var":"btn_diamond","stateNum":1,"skin":"menu/btn_diamond.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"Panel","props":{"y":134,"x":0,"width":640,"visible":false,"var":"warehousePanel","height":826},"child":[{"type":"Sprite","props":{"width":3840,"var":"fighterIconList","height":441},"child":[{"type":"Image","props":{"y":162,"x":0,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":640,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":1280,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":1920,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":2560,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":3200,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":3840,"skin":"menu/di.png"}},{"type":"Image","props":{"y":162,"x":4480,"skin":"menu/di.png"}}]},{"type":"Image","props":{"y":17,"x":187,"var":"fighter_name_bg","skin":"warehouse/mingdiye_f.png"}},{"type":"Label","props":{"y":43,"x":228,"width":155,"var":"fighter_warehouse","height":22,"fontSize":22,"font":"SimHei","bold":true,"align":"center"}},{"type":"List","props":{"y":74,"x":304,"width":100,"var":"starList_warehouse","spaceX":5,"repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":463,"x":398,"var":"war_type","skin":"warehouse/ztzding.png"}},{"type":"Image","props":{"y":507,"x":155,"var":"wh_infoBg_wingman","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":489,"x":155,"var":"wh_infoBg_sub","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":470,"x":155,"var":"wh_infoBg_armo","skin":"menu/shuzhizuidakuan.png"}},{"type":"Image","props":{"y":452,"x":155,"var":"wh_infoBg_main","skin":"menu/shuzhizuidakuan.png"}},{"type":"List","props":{"y":454,"x":155,"width":325,"var":"wh_weaponMain","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":472,"x":155,"width":325,"var":"wh_armo","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":491,"x":155,"width":325,"var":"wh_weaponSub","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"List","props":{"y":509,"x":155,"width":325,"var":"wh_wingman","spaceX":2,"repeatY":1,"height":11},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"menu/shujudiz1.png","name":"icon"}}]}]},{"type":"Label","props":{"y":506,"x":99,"width":50,"var":"wh_attack_wingman","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":488,"x":106,"width":43,"var":"wh_attack_weapon_sub","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":470,"x":99,"width":50,"var":"wh_hp_armo","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":452,"x":99,"width":50,"var":"wh_attack_weapon_main","height":14,"fontSize":14,"color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":506,"x":34,"width":60,"text":"僚机攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":488,"x":34,"width":72,"text":"副武器攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":470,"x":34,"width":60,"text":"装甲护值","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Label","props":{"y":452,"x":34,"width":60,"text":"战机攻击","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff"}},{"type":"Image","props":{"y":412.9999999999999,"x":30.999999999999986,"skin":"menu/zjzl.png"}},{"type":"Label","props":{"y":418,"x":114,"width":110,"var":"combatLabel","height":24,"fontSize":34,"font":"myFontYellow","align":"center"}},{"type":"Button","props":{"y":479,"x":546,"var":"btn_war","stateNum":1,"skin":"warehouse/btn_war.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":289,"x":64,"var":"arrow_left","skin":"warehouse/fxbiao.png","moveDirection":"left","runtime":"bean.FlashArrow"}},{"type":"Image","props":{"y":289,"x":591,"var":"arrow_right","skin":"warehouse/fxbiao.png","scaleX":-1,"moveDirection":"right","runtime":"bean.FlashArrow"}},{"type":"List","props":{"y":415,"x":320,"width":122,"var":"indexList","spaceY":0,"spaceX":10,"repeatY":1,"height":16,"anchorX":0.5},"child":[{"type":"Box","props":{"y":0,"x":0,"width":15,"name":"render","height":16},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"warehouse/fxpoint1.png","name":"icon_not_select"}},{"type":"Image","props":{"skin":"warehouse/fxpoint.png","name":"icon_select"}}]}]}]},{"type":"Sprite","props":{"y":0,"x":0,"width":640,"visible":false,"var":"badge","height":960}},{"type":"RoleInfo","props":{"y":0,"x":0,"var":"roleInfo","runtime":"ui.RoleInfoUI"}},{"type":"Panel","props":{"y":661,"x":0,"width":640,"visible":false,"var":"warehouseBottom","height":300},"child":[{"type":"Image","props":{"skin":"warehouse/diye.png"}},{"type":"Button","props":{"y":74,"x":103,"var":"btn_weapon_main","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":false,"var":"selectedImg_main","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"weapon_main_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"weapon_main_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"weapon_main_qualityNumber","height":21,"anchorX":0.5}},{"type":"Box","props":{"y":-6,"x":51,"width":75,"height":21,"anchorX":0.5}},{"type":"Sprite","props":{"y":-6,"x":13,"width":75,"height":21}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"weapon_main_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":0,"visible":false,"var":"new_btn_weapon_main","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":74,"x":243,"var":"btn_armo","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":false,"var":"selectedImg_armo","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"armo_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"armo_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"armo_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"armo_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_armo","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":74,"x":384,"var":"btn_weapon_sub","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":false,"var":"selectedImg_sub","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"weapon_sub_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"weapon_sub_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"weapon_sub_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"weapon_sub_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_weapon_sub","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":74,"x":525,"var":"btn_wingman","stateNum":1,"skin":"quality/1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-5,"x":-6,"visible":false,"var":"selectedImg_wingman","skin":"backpack/xuan.png"}},{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"wingman_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"wingman_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"wingman_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":69,"x":10,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"wingman_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_wingman","skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":74,"x":103,"var":"buy_fighter","skin":"quality/0.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":-4,"x":-1,"width":106,"var":"buy_fighter_icon","height":106}},{"type":"Label","props":{"y":73,"x":12,"width":30,"var":"buy_fighter_strengthen","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]},{"type":"Label","props":{"y":23,"x":172,"width":410,"var":"buy_fighter_name","overflow":"scroll","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":52,"x":171,"wordWrap":true,"width":410,"var":"buy_fighter_text","overflow":"scroll","height":80,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Button","props":{"y":234,"x":73,"var":"btn_backpack","stateNum":1,"skin":"warehouse/btn_backpack.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":234,"x":569,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":227,"x":318,"var":"btn_buy","stateNum":1,"skin":"warehouse/btn_buy.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":22,"x":130,"width":102,"var":"priceBtnLable","strokeColor":"#4c0c2f","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":22,"x":17,"width":80,"text":"购买","strokeColor":"#ffd600","stroke":4,"height":28,"fontSize":28,"font":"SimHei","color":"#9e4500","bold":true,"align":"center"}},{"type":"Image","props":{"y":36,"x":108,"var":"priceBtnIcon","skin":"menu/icon_diamond.png","anchorY":0.5,"anchorX":0.5}}]},{"type":"CheckBox","props":{"y":233,"x":202,"var":"check_strengthen","stateNum":2,"skin":"warehouse/check_strengthen.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_strengthen","skin":"menu/zd.png"}}]},{"type":"CheckBox","props":{"y":233,"x":321,"var":"check_advance","stateNum":2,"skin":"warehouse/check_advance.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_advance","skin":"menu/zd.png"}}]},{"type":"CheckBox","props":{"y":233,"x":439,"var":"check_star","stateNum":2,"skin":"warehouse/check_star.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"y":-169.0000000000001,"x":-328.0000000000001,"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_star","skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":155,"x":205,"visible":false,"var":"arrow_strengthen","skin":"warehouse/biao.png","anchorX":0.5}},{"type":"Image","props":{"y":155,"x":320,"visible":false,"var":"arrow_advance","skin":"warehouse/biao.png","anchorX":0.5}},{"type":"Image","props":{"y":155,"x":435,"visible":false,"var":"arrow_star","skin":"warehouse/biao.png","anchorX":0.5}}]},{"type":"Panel","props":{"y":67,"x":0,"width":640,"visible":false,"var":"upgradeStar","height":600},"child":[{"type":"Image","props":{"y":22,"x":50,"skin":"warehouse/diye1.png"}},{"type":"Image","props":{"y":28,"x":51,"skin":"warehouse/dimin.png"}},{"type":"Image","props":{"y":66,"x":133,"skin":"warehouse/diguan.png"}},{"type":"Image","props":{"y":336,"x":78,"skin":"warehouse/dx1.png"}},{"type":"Image","props":{"y":339,"x":74,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":367,"x":73,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":393,"x":71,"skin":"warehouse/tishen.png"}},{"type":"Label","props":{"y":352,"x":83,"width":100,"text":"星级:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":353,"x":194,"width":100,"var":"starLevel","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":379,"x":83,"width":100,"var":"star_attackLable","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":380,"x":194,"width":100,"var":"starAttack","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":406,"x":83,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":407,"x":194,"width":100,"var":"starScore","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":407,"x":493,"width":100,"var":"starScore_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":406,"x":382,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":380,"x":493,"width":100,"var":"starAttack_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":379,"x":382,"width":100,"var":"star_attackLable_upgrade","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":353,"x":493,"width":100,"var":"starLevel_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":352,"x":382,"width":100,"text":"星级:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Image","props":{"y":54.999999999999986,"x":270.99999999999994,"skin":"warehouse/min3.png"}},{"type":"Image","props":{"y":158,"x":268,"var":"star_qualityIcon","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"var":"star_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"star_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"star_qualityNumber","height":21,"anchorX":0.5}}]},{"type":"Label","props":{"y":478,"x":102,"width":90,"text":"升星材料:","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"List","props":{"y":448,"x":200,"width":195,"var":"starResources","vScrollBarSkin":"menu/vscroll.png","repeatY":1,"repeatX":3,"height":81},"child":[{"type":"Box","props":{"width":80,"scaleY":0.6,"scaleX":0.6,"name":"render"},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":106,"x":0,"width":101,"name":"starNumber","height":20,"fontSize":20,"font":"SimHei","align":"center"}}]}]},{"type":"Label","props":{"y":542,"x":93,"width":90,"text":"消耗积分","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Image","props":{"y":539,"x":205,"skin":"backpack/jbk.png"}},{"type":"Image","props":{"y":537,"x":202,"width":27,"skin":"attack/jinbi.png","height":27}},{"type":"Label","props":{"y":543,"x":232.99999999999994,"width":100,"var":"starSpendScore","height":20,"fontSize":20,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Button","props":{"y":509,"x":485,"var":"btn_star","stateNum":1,"skin":"warehouse/btn_star.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_star","skin":"menu/zd.png"}}]},{"type":"ProgressBar","props":{"y":316,"x":233,"var":"starBar","value":0.2,"skin":"warehouse/progressBar.png","sizeGrid":"0,0,0,0,1"}},{"type":"Image","props":{"y":303,"x":72,"skin":"warehouse/mingdiye.png"}},{"type":"Label","props":{"y":308,"x":83,"width":145,"var":"starEquipmentName","height":22,"fontSize":22,"font":"SimHei","align":"center"}},{"type":"Image","props":{"y":303,"x":254,"var":"star0","skin":"warehouse/xin1.png"}},{"type":"Image","props":{"y":303,"x":299,"var":"star1","skin":"warehouse/xin1.png"}},{"type":"Image","props":{"y":303,"x":344,"var":"star2","skin":"warehouse/xin1.png"}},{"type":"Image","props":{"y":303,"x":388,"var":"star3","skin":"warehouse/xin1.png"}},{"type":"Image","props":{"y":303,"x":433,"var":"star4","skin":"warehouse/xin1.png"}}]},{"type":"Panel","props":{"y":67,"x":0,"width":640,"visible":false,"var":"upgradeStrengthen","height":600},"child":[{"type":"Image","props":{"y":22,"x":50,"skin":"warehouse/diye1.png"}},{"type":"Image","props":{"y":28,"x":51,"skin":"warehouse/dimin.png"}},{"type":"Image","props":{"y":66,"x":133,"skin":"warehouse/diguan.png"}},{"type":"Image","props":{"y":336,"x":77,"skin":"warehouse/dx1.png"}},{"type":"Image","props":{"y":339,"x":74,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":367,"x":74,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":393,"x":71,"skin":"warehouse/tishen.png"}},{"type":"Label","props":{"y":352,"x":83,"width":100,"text":"等级:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":353,"x":194,"width":100,"var":"strengthenLevel","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":379,"x":83,"width":100,"var":"strengthen_attackLable","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":380,"x":194,"width":100,"var":"strengthenAttack","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":406,"x":83,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":407,"x":194,"width":100,"var":"strengthenScore","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":407,"x":493,"width":100,"var":"strengthenScore_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":406,"x":382,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":380,"x":493,"width":100,"var":"strengthenAttack_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":379,"x":382,"width":100,"var":"strengthen_attackLable_upgrade","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":353,"x":493,"width":100,"var":"strengthenLevel_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":352,"x":382,"width":100,"text":"等级:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Image","props":{"y":54,"x":272,"skin":"warehouse/min1.png"}},{"type":"Image","props":{"y":158,"x":268,"var":"strengthen_qualityIcon","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"var":"strengthen_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"strengthen_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"strengthen_qualityNumber","height":21,"anchorX":0.5}}]},{"type":"Label","props":{"y":468,"x":174,"width":90,"text":"消耗积分","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Image","props":{"y":465,"x":283,"skin":"backpack/jbk.png"}},{"type":"Image","props":{"y":463,"x":280,"width":27,"skin":"attack/jinbi.png","height":27}},{"type":"Label","props":{"y":469,"x":311,"width":100,"var":"strengthenSpendScore","height":20,"fontSize":20,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":303,"x":71,"skin":"warehouse/mingdiye.png"}},{"type":"Label","props":{"y":308,"x":82,"width":145,"var":"strengthenEquipmentName","height":22,"fontSize":22,"font":"SimHei","align":"center"}},{"type":"Button","props":{"y":537,"x":187,"var":"btn_strengthen_1","stateNum":1,"skin":"warehouse/btn_strengthen_1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_strengthen_1","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":537,"x":455,"var":"btn_strengthen_10","stateNum":1,"skin":"warehouse/btn_strengthen_10.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_strengthen_10","skin":"menu/zd.png"}}]}]},{"type":"Panel","props":{"y":67,"x":0,"width":640,"visible":false,"var":"upgradeQuality","height":600},"child":[{"type":"Image","props":{"y":22,"x":50,"skin":"warehouse/diye1.png"}},{"type":"Image","props":{"y":28,"x":51,"skin":"warehouse/dimin.png"}},{"type":"Image","props":{"y":329,"x":77,"skin":"warehouse/dx1.png"}},{"type":"Image","props":{"y":334,"x":74,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":362,"x":73,"skin":"warehouse/tishen.png"}},{"type":"Image","props":{"y":388,"x":72,"skin":"warehouse/tishen.png"}},{"type":"Label","props":{"y":347,"x":83,"width":100,"text":"品质:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":348,"x":194,"width":100,"var":"qualityLevel","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":374,"x":83,"width":100,"var":"quality_attackLable","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":375,"x":194,"width":100,"var":"qualityAttack","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":401,"x":83,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":402,"x":194,"width":100,"var":"qualityScore","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":402,"x":493,"width":100,"var":"qualityScore_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":401,"x":382,"width":100,"text":"分数加成:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":375,"x":493,"width":100,"var":"qualityAttack_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":374,"x":382,"width":100,"var":"quality_attackLable_upgrade","text":"攻击:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":348,"x":493,"width":100,"var":"qualityLevel_upgrade","height":19,"fontSize":19,"font":"SimHei","color":"#00ff42","align":"left"}},{"type":"Label","props":{"y":347,"x":382,"width":100,"text":"品质:","height":19,"fontSize":19,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Image","props":{"y":54,"x":271,"skin":"warehouse/min2.png"}},{"type":"Image","props":{"y":204,"x":120,"var":"quality_qualityIcon","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"var":"quality_icon","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"quality_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"quality_qualityNumber","height":21,"anchorX":0.5}}]},{"type":"Image","props":{"y":204,"x":410,"var":"quality_qualityIcon_next","skin":"quality/1.png"},"child":[{"type":"Image","props":{"y":12,"x":16,"width":71,"var":"quality_icon_next","height":74}},{"type":"List","props":{"y":89,"x":56,"width":80,"var":"quality_starList_next","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"quality_qualityNumber_next","height":21,"anchorX":0.5}}]},{"type":"Label","props":{"y":478,"x":102,"width":90,"text":"进阶材料:","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"List","props":{"y":447,"x":200,"width":195,"var":"qualityResources","vScrollBarSkin":"menu/vscroll.png","repeatY":1,"repeatX":3,"height":81},"child":[{"type":"Box","props":{"y":0,"x":0,"width":80,"scaleY":0.6,"scaleX":0.6,"name":"render"},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":106,"x":0,"width":101,"name":"starNumber","height":20,"fontSize":20,"font":"SimHei","align":"center"}}]}]},{"type":"Label","props":{"y":543,"x":94,"width":90,"text":"消耗积分","height":17,"fontSize":17,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Image","props":{"y":539,"x":203,"skin":"backpack/jbk.png"}},{"type":"Image","props":{"y":533,"x":196,"width":36,"skin":"tool/3001.png","height":36}},{"type":"Label","props":{"y":543,"x":230.99999999999994,"width":100,"var":"qualitySpendScore","height":20,"fontSize":20,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":136,"x":90,"skin":"warehouse/mingdiye.png"}},{"type":"Label","props":{"y":141,"x":101,"width":145,"var":"quality_equipmentName","height":22,"fontSize":22,"font":"SimHei","align":"center"}},{"type":"Image","props":{"y":136,"x":381,"skin":"warehouse/mingdiye.png"}},{"type":"Label","props":{"y":141,"x":392,"width":145,"var":"quality_equipmentName_next","height":22,"fontSize":22,"font":"SimHei","align":"center"}},{"type":"Button","props":{"y":509,"x":485,"var":"btn_advance","stateNum":1,"skin":"warehouse/btn_advance.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"visible":false,"var":"new_btn_advance","skin":"menu/zd.png"}}]}]},{"type":"GoodsInfo","props":{"y":0,"x":0,"visible":false,"var":"goodsInfo","runtime":"ui.GoodsInfoUI"}},{"type":"Animation","props":{"y":770,"x":320,"visible":false,"var":"ani_levelUp0","source":"Ani_levelUp0.ani"}},{"type":"Animation","props":{"y":158,"x":42,"visible":false,"var":"ani_levelUp1","source":"Ani_levelUp1.ani"}},{"type":"Animation","props":{"y":158,"x":331,"visible":false,"var":"ani_levelUp2","source":"Ani_levelUp1.ani"}},{"type":"Sprite","props":{"y":0,"x":0,"visible":false,"var":"rank"}},{"type":"ResourcesInfo","props":{"y":0,"x":0,"visible":false,"var":"resourcesInfo","runtime":"ui.ResourcesInfoUI"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"GiftReward","props":{"y":0,"x":0,"visible":false,"var":"giftReward","runtime":"ui.GiftRewardUI"}},{"type":"FlashFigter","props":{"y":0,"x":0,"visible":false,"var":"flashFighter","runtime":"ui.FlashFigterUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Sprite","props":{"y":0,"x":0,"width":640,"visible":false,"var":"dialogLayer","height":960}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return MenuUI;
	})(View);
var OnlineRewardUI=(function(_super){
		function OnlineRewardUI(){
			
		    this.btn_back=null;
		    this.rewardList=null;
		    this.reward=null;
		    this.dataLoading=null;

			OnlineRewardUI.__super.call(this);
		}

		CLASS$(OnlineRewardUI,'ui.OnlineRewardUI',_super);
		var __proto__=OnlineRewardUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(OnlineRewardUI.uiView);
		}
		OnlineRewardUI.uiView={"type":"View","props":{"width":640,"visible":false,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.8},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":88,"x":15,"width":620,"skin":"dialogRes/bg_checkIn.png","sizeGrid":"150,250,150,250","height":735}},{"type":"Image","props":{"y":99,"x":208,"skin":"world/bt.png"}},{"type":"Button","props":{"y":886,"x":320,"var":"btn_back","stateNum":1,"skin":"dialogRes/btn_close_ol.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"List","props":{"y":161,"x":45,"width":590,"var":"rewardList","vScrollBarSkin":"menu/vscroll.png","spaceY":6,"repeatY":6,"repeatX":1,"height":650},"child":[{"type":"Box","props":{"y":0,"x":0,"width":590,"name":"render","height":100},"child":[{"type":"Image","props":{"visible":true,"skin":"rank/lantiao1.png"}},{"type":"Image","props":{"y":7,"x":15,"visible":true,"skin":"world/iconqdjl.png"}},{"type":"List","props":{"y":29,"x":114,"width":355,"visible":true,"repeatY":1,"repeatX":5,"name":"goodsList","height":68,"hScrollBarSkin":"menu/hscroll.png"},"child":[{"type":"Box","props":{"width":80,"scaleY":0.6,"scaleX":0.6,"name":"render"},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"width":50,"visible":true,"skin":"quality/jidi.png","sizeGrid":"9,10,9,10","height":20}},{"type":"Label","props":{"y":77,"x":14,"width":45,"name":"number","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Label","props":{"y":12,"x":341,"width":70,"text":"累计在线","height":14,"fontSize":14,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Label","props":{"y":12,"x":431,"width":50,"text":"分钟","height":14,"fontSize":14,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Label","props":{"y":8,"x":392,"width":40,"text":"99","name":"time","height":20,"fontSize":20,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Button","props":{"y":54,"x":513,"visible":true,"stateNum":1,"skin":"rank/btn_task_receive.png","name":"btn_receive","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"x":441,"visible":true,"skin":"rank/lanbutton4.png","name":"onlineFinish"}}]}]},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return OnlineRewardUI;
	})(View);
var PayTipUI=(function(_super){
		function PayTipUI(){
			
		    this.btn_submit=null;
		    this.btn_dialogClose=null;
		    this.tipText=null;

			PayTipUI.__super.call(this);
		}

		CLASS$(PayTipUI,'ui.PayTipUI',_super);
		var __proto__=PayTipUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(PayTipUI.uiView);
		}
		PayTipUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":296,"x":115,"skin":"4399/di.png"}},{"type":"Button","props":{"y":567,"x":326,"var":"btn_submit","stateNum":1,"skin":"4399/btn_bg.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"y":-364,"x":-149,"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":7,"x":8,"width":120,"valign":"middle","text":"确定","strokeColor":"#813100","stroke":4,"height":34,"fontSize":24,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Button","props":{"y":307,"x":501,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":338,"x":128,"wordWrap":true,"width":390,"var":"tipText","valign":"middle","text":"充值完毕后，请点击“确定”来刷新钻石数量","height":148,"fontSize":24,"font":"SimHei","color":"#78fff8","align":"center"}}]};
		return PayTipUI;
	})(View);
var RankUI=(function(_super){
		function RankUI(){
			
		    this.bgImg=null;
		    this.scorePanel=null;
		    this.scoreList=null;
		    this.medal_scoreMy=null;
		    this.head_scoreMy=null;
		    this.none_scoreMy=null;
		    this.rank_scoreMy=null;
		    this.name_scoreMy=null;
		    this.score_scoreMy=null;
		    this.combatPowerPanel=null;
		    this.combatPowerList=null;
		    this.medal_combatPowerMy=null;
		    this.head_combatPowerMy=null;
		    this.none_combatPowerMy=null;
		    this.rank_combatPowerMy=null;
		    this.name_combatPowerMy=null;
		    this.score_combatPowerMy=null;
		    this.chapterPanel=null;
		    this.chapterList=null;
		    this.medal_chapterMy=null;
		    this.head_chapterMy=null;
		    this.none_chapterMy=null;
		    this.rank_chapterMy=null;
		    this.name_chapterMy=null;
		    this.score_chapterMy=null;
		    this.check_chapter=null;
		    this.check_combatPower=null;
		    this.check_score=null;
		    this.btn_reward=null;
		    this.btn_back=null;
		    this.rewardDialog=null;
		    this.dataLoading=null;

			RankUI.__super.call(this);
		}

		CLASS$(RankUI,'ui.RankUI',_super);
		var __proto__=RankUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.RankRewardDialogUI",ui.RankRewardDialogUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RankUI.uiView);
		}
		RankUI.uiView={"type":"View","props":{"width":640,"visible":false,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":90,"x":4,"width":629,"skin":"dialogRes/bg_checkIn.png","sizeGrid":"150,250,150,250","height":735}},{"type":"Panel","props":{"y":169,"x":19,"width":621,"var":"scorePanel","height":791},"child":[{"type":"Image","props":{"y":672,"skin":"rank/di2.png"}},{"type":"Label","props":{"y":686,"x":28,"wordWrap":true,"width":360,"text":"积分榜每周重置\\n积分榜按照玩家累计获得的积分总数进行排名\\n排行榜刷新时间间隔为1小时","leading":6,"height":74,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"List","props":{"x":9,"width":587,"var":"scoreList","vScrollBarSkin":"menu/vscroll.png","repeatY":5,"repeatX":1,"height":520},"child":[{"type":"Box","props":{"name":"render","height":106},"child":[{"type":"Image","props":{"visible":true,"skin":"rank/lan.png"}},{"type":"Image","props":{"y":14,"x":30,"visible":true,"skin":"rank/jb1.png","name":"medal"}},{"type":"Image","props":{"y":7,"x":132,"width":91,"visible":true,"skin":"createRole/head0_0.png","name":"head","height":91}},{"type":"Label","props":{"y":31,"x":11,"width":118,"name":"rank","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":46,"x":228,"width":160,"name":"name","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":37,"x":370,"width":210,"name":"score","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]}]},{"type":"Image","props":{"y":526,"x":9,"skin":"rank/lantiaoplayer.png"}},{"type":"Image","props":{"y":530,"x":39,"var":"medal_scoreMy","skin":"rank/jb1.png"}},{"type":"Image","props":{"y":533,"x":141,"width":91,"var":"head_scoreMy","skin":"createRole/head0_0.png","height":91}},{"type":"Image","props":{"y":555,"x":27,"var":"none_scoreMy","skin":"rank/lantiaobt.png"}},{"type":"Label","props":{"y":553,"x":20,"width":118,"var":"rank_scoreMy","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":572,"x":237,"width":160,"var":"name_scoreMy","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":563,"x":397,"width":190,"var":"score_scoreMy","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]},{"type":"Panel","props":{"y":169,"x":19,"width":621,"var":"combatPowerPanel","height":791},"child":[{"type":"Image","props":{"y":672,"x":0,"skin":"rank/di2.png"}},{"type":"Label","props":{"y":686,"x":28,"wordWrap":true,"width":360,"text":"每日23:30结算，根据当前名次发放对应奖励\\n排行榜刷新时间间隔为1小时","leading":6,"height":74,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"List","props":{"x":9,"width":587,"var":"combatPowerList","vScrollBarSkin":"menu/vscroll.png","repeatY":5,"repeatX":1,"height":520},"child":[{"type":"Box","props":{"name":"render","height":106},"child":[{"type":"Image","props":{"visible":true,"skin":"rank/lan.png"}},{"type":"Image","props":{"y":14,"x":30,"visible":true,"skin":"rank/jb1.png","name":"medal"}},{"type":"Image","props":{"y":7,"x":132,"width":91,"visible":true,"skin":"createRole/head0_0.png","name":"head","height":91}},{"type":"Label","props":{"y":31,"x":11,"width":118,"name":"rank","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":46,"x":228,"width":160,"name":"name","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":37,"x":370,"width":210,"name":"score","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]}]},{"type":"Image","props":{"y":526,"x":9,"skin":"rank/lantiaoplayer.png"}},{"type":"Image","props":{"y":530,"x":39,"var":"medal_combatPowerMy","skin":"rank/jb1.png"}},{"type":"Image","props":{"y":533,"x":141,"width":91,"var":"head_combatPowerMy","skin":"createRole/head0_0.png","height":91}},{"type":"Image","props":{"y":555,"x":27,"var":"none_combatPowerMy","skin":"rank/lantiaobt.png"}},{"type":"Label","props":{"y":553,"x":20,"width":118,"var":"rank_combatPowerMy","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":572,"x":237,"width":160,"var":"name_combatPowerMy","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":563,"x":397,"width":190,"var":"score_combatPowerMy","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]},{"type":"Panel","props":{"y":169,"x":18.999999999999986,"width":621,"visible":false,"var":"chapterPanel","height":791},"child":[{"type":"Image","props":{"y":672,"x":2.842170943040401e-14,"skin":"rank/di2.png"}},{"type":"Label","props":{"y":686,"x":27.999999999999936,"wordWrap":true,"width":360,"text":"每日23:30结算，根据当前名次发放对应奖励\\n排行榜刷新时间间隔为1小时","leading":6,"height":74,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"List","props":{"x":9,"width":587,"var":"chapterList","vScrollBarSkin":"menu/vscroll.png","repeatY":5,"repeatX":1,"height":520},"child":[{"type":"Box","props":{"name":"render","height":106},"child":[{"type":"Image","props":{"visible":true,"skin":"rank/lan.png"}},{"type":"Image","props":{"y":14,"x":30,"visible":true,"skin":"rank/jb1.png","name":"medal"}},{"type":"Image","props":{"y":7,"x":132,"width":91,"visible":true,"skin":"createRole/head0_0.png","name":"head","height":91}},{"type":"Label","props":{"y":31,"x":11,"width":118,"name":"rank","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":46,"x":228,"width":160,"name":"name","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":37,"x":370,"width":210,"name":"score","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]}]},{"type":"Image","props":{"y":526,"x":9,"skin":"rank/lantiaoplayer.png"}},{"type":"Image","props":{"y":530,"x":39,"var":"medal_chapterMy","skin":"rank/jb1.png"}},{"type":"Image","props":{"y":533,"x":141,"width":91,"var":"head_chapterMy","skin":"createRole/head0_0.png","height":91}},{"type":"Image","props":{"y":555,"x":27,"var":"none_chapterMy","skin":"rank/lantiaobt.png"}},{"type":"Label","props":{"y":553,"x":20,"width":118,"var":"rank_chapterMy","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":572,"x":237,"width":160,"var":"name_chapterMy","height":22,"fontSize":22,"font":"SimHei","color":"#004280","align":"left"}},{"type":"Label","props":{"y":563,"x":397,"width":190,"var":"score_chapterMy","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}}]},{"type":"CheckBox","props":{"y":134,"x":349,"var":"check_chapter","stateNum":2,"skin":"dialogRes/check_chapter.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":134,"x":223,"var":"check_combatPower","stateNum":2,"skin":"dialogRes/check_combatPower.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":134,"x":96,"var":"check_score","stateNum":2,"skin":"dialogRes/check_score.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":125,"x":430,"skin":"rank/btk.png"}},{"type":"Image","props":{"y":125,"x":578,"skin":"rank/btk.png","scaleX":-1}},{"type":"Button","props":{"y":134,"x":506,"width":80,"var":"btn_reward","stateNum":1,"labelSize":18,"labelFont":"SimHei","labelColors":"#f2ffac","labelAlign":"center","label":"奖励说明","height":20,"anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":890,"x":546,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"RankRewardDialog","props":{"y":0,"x":0,"visible":false,"var":"rewardDialog","runtime":"ui.RankRewardDialogUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return RankUI;
	})(View);
var RankDBQBUI=(function(_super){
		function RankDBQBUI(){
			
		    this.rankList=null;
		    this.btn_chest=null;
		    this.btn_back=null;
		    this.btn_start=null;

			RankDBQBUI.__super.call(this);
		}

		CLASS$(RankDBQBUI,'ui.RankDBQBUI',_super);
		var __proto__=RankDBQBUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RankDBQBUI.uiView);
		}
		RankDBQBUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":93,"x":19,"skin":"dbqb/di.png"}},{"type":"Image","props":{"y":107,"x":193,"skin":"share/bt4.png"}},{"type":"List","props":{"y":163,"x":47,"width":561,"var":"rankList","vScrollBarSkin":"menu/vscroll.png","spaceY":0,"spaceX":0,"repeatY":7,"repeatX":1,"height":640},"child":[{"type":"Box","props":{"y":0,"x":0,"width":530,"name":"render","height":90},"child":[{"type":"Image","props":{"y":9,"x":4,"width":525,"skin":"share/tiao1.png","sizeGrid":"0,150,0,50","name":"bg","height":75}},{"type":"Image","props":{"y":14,"x":13,"skin":"share/tiaotxk.png"}},{"type":"Image","props":{"y":17,"x":18,"width":57,"name":"head","height":59}},{"type":"Image","props":{"skin":"resourcesInfo/lan1_dj.png","scaleY":0.75,"scaleX":0.75}},{"type":"Label","props":{"y":10,"x":2,"width":40,"scaleY":0.75,"scaleX":0.75,"name":"level","height":20,"fontSize":36,"font":"myFontBlue","align":"center"}},{"type":"Image","props":{"y":42,"x":144,"skin":"rank/jb1.png","name":"medal","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":24,"x":83,"width":118,"name":"rank","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":52,"x":215,"width":91,"text":"最高得分:","height":17,"fontSize":17,"font":"SimHei","color":"#aa500a","bold":true,"align":"right"}},{"type":"Label","props":{"y":46,"x":306,"width":316,"scaleY":0.7,"scaleX":0.7,"name":"score","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}},{"type":"Label","props":{"y":23,"x":225,"width":287,"strokeColor":"#000000","stroke":4,"name":"name","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Image","props":{"y":63,"x":110,"skin":"share/tiao2me.png","name":"isMe"}}]}]},{"type":"Label","props":{"y":827,"x":157,"width":329,"visible":false,"text":"夺宝分数排行每周重置*","strokeColor":"#0066ff","stroke":2,"height":18,"fontSize":18,"font":"SimHei","color":"#00f5ff","align":"center"}},{"type":"Button","props":{"y":895,"x":83,"var":"btn_chest","stateNum":1,"skin":"dbqb/btn_chest.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":896,"x":563,"var":"btn_back","stateNum":1,"skin":"dbqb/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":897,"x":323,"var":"btn_start","stateNum":1,"skin":"dbqb/btn_start.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]};
		return RankDBQBUI;
	})(View);
var RankFriendUI=(function(_super){
		function RankFriendUI(){
			
		    this.btn_dialogClose=null;
		    this.rankList=null;
		    this.dataLoading=null;

			RankFriendUI.__super.call(this);
		}

		CLASS$(RankFriendUI,'ui.RankFriendUI',_super);
		var __proto__=RankFriendUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RankFriendUI.uiView);
		}
		RankFriendUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":185,"x":91,"skin":"share/di.png"}},{"type":"Button","props":{"y":191,"x":539,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":199,"x":193,"skin":"share/bt4.png"}},{"type":"List","props":{"y":257,"x":107,"width":446,"var":"rankList","vScrollBarSkin":"menu/vscroll.png","spaceY":1,"repeatY":6,"repeatX":1,"height":539},"child":[{"type":"Box","props":{"y":0,"x":0,"width":421,"name":"render","height":88},"child":[{"type":"Image","props":{"y":9,"x":4,"skin":"share/tiao1.png","name":"bg"}},{"type":"Image","props":{"y":14,"x":13,"skin":"share/tiaotxk.png"}},{"type":"Image","props":{"y":17,"x":18,"width":57,"name":"head","height":59}},{"type":"Image","props":{"skin":"resourcesInfo/lan1_dj.png","scaleY":0.75,"scaleX":0.75}},{"type":"Label","props":{"y":10,"x":2,"width":40,"scaleY":0.75,"scaleX":0.75,"name":"level","height":20,"fontSize":36,"font":"myFontBlue","align":"center"}},{"type":"Image","props":{"y":45,"x":143,"skin":"rank/jb1.png","name":"medal","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":24,"x":83,"width":118,"name":"rank","height":47,"font":"myFontRankNumber","align":"center"}},{"type":"Label","props":{"y":52,"x":198,"width":91,"text":"最高关卡:","height":17,"fontSize":17,"font":"SimHei","color":"#aa500a","bold":true,"align":"right"}},{"type":"Label","props":{"y":46,"x":283,"width":191,"scaleY":0.7,"scaleX":0.7,"name":"score","height":42,"fontSize":42,"font":"myFontRankScore","align":"center"}},{"type":"Label","props":{"y":23,"x":194,"width":220,"strokeColor":"#000000","stroke":4,"name":"name","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":63,"x":110,"skin":"share/tiao2me.png","name":"isMe"}}]}]},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return RankFriendUI;
	})(View);
var RankRewardDialogUI=(function(_super){
		function RankRewardDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.chapterList=null;
		    this.combatPowerList=null;
		    this.scoreList=null;

			RankRewardDialogUI.__super.call(this);
		}

		CLASS$(RankRewardDialogUI,'ui.RankRewardDialogUI',_super);
		var __proto__=RankRewardDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RankRewardDialogUI.uiView);
		}
		RankRewardDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":172,"x":15,"width":607,"skin":"backpack/di1_3.png","sizeGrid":"150,120,150,120","height":570}},{"type":"Image","props":{"y":177,"x":183,"skin":"backpack/jlshm.png"}},{"type":"Button","props":{"y":179,"x":590,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":709,"width":425,"text":"*每天23:30结算后，根据当前名次发放对应奖励","height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","centerX":0.5,"bold":true,"align":"center"}},{"type":"List","props":{"y":223,"x":33,"width":573,"visible":false,"var":"chapterList","vScrollBarSkin":"menu/vscroll.png","spaceY":5,"repeatY":8,"repeatX":1,"height":477},"child":[{"type":"Box","props":{"name":"render","height":55},"child":[{"type":"Image","props":{"x":-2.1316282072803006e-14,"visible":true,"skin":"rank/ditiao.png"}},{"type":"Label","props":{"y":19,"x":25.99999999999998,"width":43,"text":"名次","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":182.99999999999997,"width":43,"text":"奖励","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":71.99999999999997,"width":100,"visible":true,"name":"rankIndex","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":232.99999999999997,"width":330,"visible":true,"name":"rankGoods","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}}]}]},{"type":"List","props":{"y":223,"x":33,"width":573,"visible":false,"var":"combatPowerList","vScrollBarSkin":"menu/vscroll.png","spaceY":5,"repeatY":8,"repeatX":1,"height":477},"child":[{"type":"Box","props":{"name":"render","height":55},"child":[{"type":"Image","props":{"x":-2.1316282072803006e-14,"visible":true,"skin":"rank/ditiao.png"}},{"type":"Label","props":{"y":19,"x":25.99999999999998,"width":43,"text":"名次","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":182.99999999999997,"width":43,"text":"奖励","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":71.99999999999997,"width":100,"visible":true,"name":"rankIndex","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":232.99999999999997,"width":330,"visible":true,"name":"rankGoods","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}}]}]},{"type":"List","props":{"y":223,"x":33,"width":573,"visible":false,"var":"scoreList","vScrollBarSkin":"menu/vscroll.png","spaceY":5,"repeatY":8,"repeatX":1,"height":477},"child":[{"type":"Box","props":{"name":"render","height":55},"child":[{"type":"Image","props":{"x":-2.1316282072803006e-14,"visible":true,"skin":"rank/ditiao.png"}},{"type":"Label","props":{"y":19,"x":25.99999999999998,"width":43,"text":"名次","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":182.99999999999997,"width":43,"text":"奖励","height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":71.99999999999997,"width":100,"visible":true,"name":"rankIndex","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":19,"x":232.99999999999997,"width":330,"visible":true,"name":"rankGoods","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}}]}]}]};
		return RankRewardDialogUI;
	})(View);
var ResourcesInfoUI=(function(_super){
		function ResourcesInfoUI(){
			
		    this.diamondLabel=null;
		    this.btn_add_diamond=null;
		    this.scoreLabel=null;
		    this.physicalLimit=null;
		    this.physicalLabel=null;
		    this.btn_add_physical=null;

			ResourcesInfoUI.__super.call(this);
		}

		CLASS$(ResourcesInfoUI,'ui.ResourcesInfoUI',_super);
		var __proto__=ResourcesInfoUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ResourcesInfoUI.uiView);
		}
		ResourcesInfoUI.uiView={"type":"View","props":{"width":640,"source":"shield/shield0.png,shield/shield1.png,shield/shield2.png,shield/shield3.png,shield/shield4.png,shield/shield5.png,shield/shield6.png,shield/shield7.png","height":67,"cacheAs":"bitmap"},"child":[{"type":"Image","props":{"y":0,"x":0,"skin":"resourcesInfo/dinglan.png"}},{"type":"Image","props":{"y":10,"x":203,"skin":"resourcesInfo/diamond.png","name":"diamondBg"}},{"type":"Label","props":{"y":29,"x":254,"width":90,"var":"diamondLabel","height":16,"fontSize":16,"color":"#ffffff","bold":true,"align":"center"}},{"type":"Button","props":{"y":37,"x":371,"var":"btn_add_diamond","stateNum":1,"skin":"resourcesInfo/btn_add.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":9,"x":413,"skin":"resourcesInfo/score.png","name":"scoreBg"}},{"type":"Label","props":{"y":29,"x":463,"width":118,"var":"scoreLabel","height":16,"fontSize":16,"color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":8,"x":23,"skin":"resourcesInfo/physical.png","name":"physicalBg"}},{"type":"Label","props":{"y":29,"x":102,"width":33,"var":"physicalLimit","height":16,"fontSize":15,"color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":29,"x":70,"width":30,"var":"physicalLabel","height":16,"fontSize":15,"color":"#78fff8","bold":true,"align":"right"}},{"type":"Button","props":{"y":34,"x":162,"var":"btn_add_physical","stateNum":1,"skin":"resourcesInfo/btn_add.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]};
		return ResourcesInfoUI;
	})(View);
var RewardUI=(function(_super){
		function RewardUI(){
			
		    this.bgImg=null;
		    this.title=null;
		    this.goodsList=null;
		    this.bgLabel=null;

			RewardUI.__super.call(this);
		}

		CLASS$(RewardUI,'ui.RewardUI',_super);
		var __proto__=RewardUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.FlashAlpha",bean.FlashAlpha);
			View.regComponent("bean.FlashScale",bean.FlashScale);
			View.regComponent("bean.FlashAlphaLabel",bean.FlashAlphaLabel);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RewardUI.uiView);
		}
		RewardUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.8},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":182,"x":0,"var":"bgImg","toRotate":false,"skin":"rank/di.png","showTime":250,"delayTime":0,"runtime":"bean.FlashAlpha"}},{"type":"Image","props":{"y":194,"x":329,"var":"title","toRotate":false,"skin":"rank/gxhd.png","delayTime":0,"anchorY":0.5,"anchorX":0.5,"runtime":"bean.FlashScale"}},{"type":"List","props":{"y":280,"x":172,"width":333,"var":"goodsList","vScrollBarSkin":"menu/vscroll.png","repeatY":2,"repeatX":3,"height":265},"child":[{"type":"Box","props":{"y":0,"x":-2,"width":110,"scaleY":0.9,"scaleX":0.9,"name":"render","height":134},"child":[{"type":"Image","props":{"y":6,"x":0,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":75,"x":11,"visible":true,"skin":"quality/jidi.png"}},{"type":"Label","props":{"y":78,"x":13,"width":29,"name":"number","height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":0,"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":110,"x":3,"width":100,"visible":true,"name":"name","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}}]}]},{"type":"Label","props":{"y":595,"x":161,"width":312,"var":"bgLabel","text":"点 击 空 白 处 继 续","showTime":250,"height":24,"fontSize":24,"font":"SimHei","color":"#ffffff","bold":true,"align":"center","runtime":"bean.FlashAlphaLabel"}}]};
		return RewardUI;
	})(View);
var RoleDialogUI=(function(_super){
		function RoleDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.check_set=null;
		    this.check_giftCode=null;
		    this.panelSet=null;
		    this.btn_head=null;
		    this.levelLabel=null;
		    this.nameLabel=null;
		    this.expLimitLabel=null;
		    this.expLabel=null;
		    this.expBar=null;
		    this.radio_music=null;
		    this.radio_sound=null;
		    this.panelGiftCode=null;
		    this.btn_giftCode=null;
		    this.giftCodeText=null;
		    this.btn_annc=null;
		    this.btn_close=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.topNotice=null;
		    this.noticeText=null;

			RoleDialogUI.__super.call(this);
		}

		CLASS$(RoleDialogUI,'ui.RoleDialogUI',_super);
		var __proto__=RoleDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RoleDialogUI.uiView);
		}
		RoleDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":236,"x":86,"skin":"dialogRes/zd.png"}},{"type":"Button","props":{"y":254,"x":520,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":211,"x":171,"var":"check_set","stateNum":2,"skin":"dialogRes/check_set.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":211,"x":319,"var":"check_giftCode","stateNum":2,"skin":"dialogRes/check_giftCode.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Panel","props":{"y":279,"x":108,"width":458,"var":"panelSet","height":371},"child":[{"type":"Image","props":{"x":75,"skin":"role/dijs.png"}},{"type":"Image","props":{"y":3,"x":78,"width":115,"var":"btn_head","height":124}},{"type":"Label","props":{"y":53,"x":317,"width":40,"var":"levelLabel","height":19,"fontSize":19,"font":"myFontBlue","align":"center"}},{"type":"Label","props":{"y":65,"x":202,"width":112,"var":"nameLabel","height":18,"fontSize":16,"color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":153,"x":226,"width":210,"var":"expLimitLabel","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":153,"x":16,"width":210,"var":"expLabel","height":20,"fontSize":20,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":171,"x":10,"width":44,"text":"EXP","italic":true,"height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","bold":true,"align":"right"}},{"type":"ProgressBar","props":{"y":173,"x":59,"var":"expBar","skin":"role/progressBar.png","sizeGrid":"3,11,3,10,1"}},{"type":"Image","props":{"y":206,"width":430,"skin":"rank/diTip.png","sizeGrid":"30,40,30,40","height":144}},{"type":"Image","props":{"y":228,"x":33,"skin":"role/mbgm.png"}},{"type":"Image","props":{"y":294,"x":34,"skin":"role/mfxm.png"}},{"type":"Label","props":{"y":235,"x":84,"width":58,"text":"音乐","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":302,"x":84,"width":58,"text":"音效","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":239,"x":269,"width":58,"text":"开","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":302,"x":269,"width":58,"text":"开","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":239,"x":380,"width":58,"text":"关","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":302,"x":380,"width":58,"text":"关","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"RadioGroup","props":{"y":229,"x":226,"var":"radio_music","space":73,"skin":"role/radioGroup.png","labels":",","direction":"horizontal"}},{"type":"RadioGroup","props":{"y":292,"x":226,"var":"radio_sound","space":73,"skin":"role/radioGroup.png","labels":",","direction":"horizontal"}}]},{"type":"Panel","props":{"y":307,"x":108.00000000000001,"width":443,"visible":false,"var":"panelGiftCode","height":319},"child":[{"type":"Image","props":{"y":-0.9999999999999432,"skin":"role/di.png"}},{"type":"Image","props":{"y":78.00000000000006,"x":100,"skin":"role/bt1.png"}},{"type":"Button","props":{"y":225.00000000000006,"x":208,"var":"btn_giftCode","stateNum":1,"skin":"role/btn_giftCode.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":142.00000000000006,"x":56,"skin":"role/kuan.png"}},{"type":"TextInput","props":{"y":145.00000000000006,"x":67,"width":294,"var":"giftCodeText","promptColor":"#f2ffac","prompt":"请输入礼品码","height":27,"fontSize":27,"font":"SimHei","color":"#ffffff","align":"left"}}]},{"type":"Button","props":{"y":714,"x":157,"visible":false,"var":"btn_annc","stateNum":1,"skin":"role/btn_annc.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":714,"x":505,"visible":false,"var":"btn_close","stateNum":1,"skin":"role/btn_close.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":10,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return RoleDialogUI;
	})(View);
var RoleInfoUI=(function(_super){
		function RoleInfoUI(){
			
		    this.diamondLabel=null;
		    this.btn_add_diamond=null;
		    this.combatLabel=null;
		    this.scoreLabel=null;
		    this.physicalLimit=null;
		    this.physicalLabel=null;
		    this.btn_add_physical=null;
		    this.btn_head=null;
		    this.levelLabel=null;
		    this.nameLabel=null;
		    this.scoreAdditionLabel=null;

			RoleInfoUI.__super.call(this);
		}

		CLASS$(RoleInfoUI,'ui.RoleInfoUI',_super);
		var __proto__=RoleInfoUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(RoleInfoUI.uiView);
		}
		RoleInfoUI.uiView={"type":"View","props":{"width":640,"source":"shield/shield0.png,shield/shield1.png,shield/shield2.png,shield/shield3.png,shield/shield4.png,shield/shield5.png,shield/shield6.png,shield/shield7.png","height":154,"cacheAs":"bitmap"},"child":[{"type":"Image","props":{"y":0,"x":0,"top":0,"skin":"menu/lan1.png","name":"upTitle","centerX":0}},{"type":"Image","props":{"y":72,"x":403,"skin":"resourcesInfo/diamond.png","name":"diamondBg"}},{"type":"Label","props":{"y":92,"x":454,"width":95,"var":"diamondLabel","height":16,"fontSize":16,"color":"#ffffff","bold":true,"align":"center"}},{"type":"Button","props":{"y":99,"x":571,"var":"btn_add_diamond","stateNum":1,"skin":"resourcesInfo/btn_add.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":77,"x":127,"width":253,"skin":"menu/combatPowerBg.png","sizeGrid":"0,20,0,60","height":39}},{"type":"Label","props":{"y":90,"x":176,"width":220,"var":"combatLabel","height":24,"fontSize":34,"font":"myFontYellow","align":"center"}},{"type":"Image","props":{"y":8,"x":285,"skin":"resourcesInfo/score.png","name":"scoreBg"}},{"type":"Label","props":{"y":29,"x":337,"width":116,"var":"scoreLabel","height":16,"fontSize":16,"color":"#ffffff","bold":true,"align":"center"}},{"type":"Image","props":{"y":10,"x":479,"skin":"resourcesInfo/physical.png","name":"physicalBg"}},{"type":"Label","props":{"y":30,"x":558,"width":33,"var":"physicalLimit","height":16,"fontSize":15,"color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":30,"x":526,"width":30,"var":"physicalLabel","height":16,"fontSize":15,"color":"#78fff8","bold":true,"align":"right"}},{"type":"Button","props":{"y":35,"x":617,"var":"btn_add_physical","stateNum":1,"skin":"resourcesInfo/btn_add.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":75,"x":64,"width":115,"var":"btn_head","stateNum":1,"height":124,"anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":2,"x":90,"skin":"resourcesInfo/lan1_dj.png"}},{"type":"Label","props":{"y":16,"x":94,"width":40,"var":"levelLabel","height":19,"fontSize":19,"font":"myFontBlue","align":"center"}},{"type":"Label","props":{"y":38,"x":127,"width":161,"var":"nameLabel","height":18,"fontSize":16,"color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":145,"x":30,"width":66,"var":"scoreAdditionLabel","height":25,"fontSize":25,"font":"myFontRed","align":"center"}},{"type":"Image","props":{"y":150,"x":9,"skin":"menu/szjc1.png","name":"jingyanjia"}},{"type":"Image","props":{"y":131,"x":96,"skin":"menu/szjc3.png","name":"jingyanbaifenbi"}}]};
		return RoleInfoUI;
	})(View);
var SellDialogUI=(function(_super){
		function SellDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.quality_icon=null;
		    this.goods_icon=null;
		    this.goods_starList=null;
		    this.goods_qualityNumber=null;
		    this.goods_fragment=null;
		    this.btn_sell=null;
		    this.totalLable=null;
		    this.numberLable=null;
		    this.numberSlider=null;
		    this.priceTotal=null;
		    this.btn_reduce=null;
		    this.btn_add=null;

			SellDialogUI.__super.call(this);
		}

		CLASS$(SellDialogUI,'ui.SellDialogUI',_super);
		var __proto__=SellDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(SellDialogUI.uiView);
		}
		SellDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":296,"x":114,"width":416,"skin":"backpack/zd_3.png","height":230}},{"type":"Button","props":{"y":309,"x":486,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":365,"x":203,"var":"quality_icon","skin":"quality/0.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Image","props":{"y":13,"x":16,"width":71,"var":"goods_icon","height":74}},{"type":"List","props":{"y":89,"x":54,"width":80,"var":"goods_starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Image","props":{"y":-6,"x":51,"width":75,"var":"goods_qualityNumber","height":21,"anchorX":0.5}},{"type":"Image","props":{"y":1,"x":68,"var":"goods_fragment","skin":"quality/suipian.png"}}]},{"type":"Label","props":{"y":446,"x":297,"width":60,"text":"出售数量","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Button","props":{"y":567,"x":321,"var":"btn_sell","stateNum":1,"skin":"backpack/btn_sell.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":485,"x":282,"skin":"backpack/sl.png"}},{"type":"Label","props":{"y":488,"x":324,"width":44,"var":"totalLable","height":16,"fontSize":16,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":488,"x":280,"width":44,"var":"numberLable","height":16,"fontSize":16,"font":"SimHei","color":"#ff4593","align":"right"}},{"type":"HSlider","props":{"y":466,"x":209,"var":"numberSlider","skin":"backpack/hSlider.png","showLabel":false,"allowClickBack":true}},{"type":"Image","props":{"y":360,"x":362,"width":37,"skin":"tool/3001.png","height":37}},{"type":"Label","props":{"y":370,"x":401,"width":70,"text":"出售总价","height":14,"fontSize":14,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Image","props":{"y":391,"x":353,"skin":"backpack/jbk.png"}},{"type":"Label","props":{"y":394,"x":364,"width":120,"var":"priceTotal","height":22,"fontSize":22,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Button","props":{"y":475,"x":190,"var":"btn_reduce","stateNum":1,"skin":"backpack/btn_reduce.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":475,"x":450,"var":"btn_add","stateNum":1,"skin":"backpack/btn_add.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]};
		return SellDialogUI;
	})(View);
var ShareUI=(function(_super){
		function ShareUI(){
			
		    this.btn_dialogClose=null;
		    this.btn_share=null;
		    this.scheduleLabel=null;
		    this.friendsList=null;
		    this.dataLoading=null;

			ShareUI.__super.call(this);
		}

		CLASS$(ShareUI,'ui.ShareUI',_super);
		var __proto__=ShareUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ShareUI.uiView);
		}
		ShareUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":185,"x":91,"skin":"share/di.png"}},{"type":"Image","props":{"y":199,"x":193,"skin":"share/bt1.png"}},{"type":"Button","props":{"y":190,"x":540,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":184,"x":121,"skin":"share/di2_s.png"}},{"type":"Button","props":{"y":331,"x":474,"var":"btn_share","stateNum":1,"skin":"share/btn_share.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":784,"x":172,"skin":"share/bt3.png"}},{"type":"Label","props":{"y":262,"x":395,"width":74,"text":"进度:","strokeColor":"#000000","stroke":4,"height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","bold":true,"align":"right"}},{"type":"Label","props":{"y":263,"x":478,"width":52,"var":"scheduleLabel","text":"0/1","strokeColor":"#000000","stroke":4,"height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","bold":true,"align":"left"}},{"type":"Label","props":{"y":395,"x":131,"wordWrap":true,"width":400,"text":"他人点击你发送的【链接】能成为好友","leading":6,"height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Label","props":{"y":765,"x":140,"wordWrap":true,"width":400,"text":"他人点击你发送的【链接】能成为好友","leading":6,"height":16,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Image","props":{"y":412,"x":132,"skin":"share/bt2.png"}},{"type":"List","props":{"y":464,"x":133,"width":416,"var":"friendsList","vScrollBarSkin":"menu/vscroll.png","spaceY":5,"spaceX":17,"repeatY":2,"repeatX":3,"height":292},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"y":3,"skin":"share/pai.png"}},{"type":"Image","props":{"y":35,"x":30,"width":58,"name":"head","height":58}},{"type":"Label","props":{"y":9,"x":17,"width":80,"strokeColor":"#821f06","stroke":2,"name":"index","height":20,"fontSize":20,"font":"SimHei","color":"#e36c0f","bold":true,"align":"center"}},{"type":"Image","props":{"y":4,"x":60,"skin":"share/paia1.png","name":"canReceive"}},{"type":"Image","props":{"x":79,"skin":"share/paib1.png","name":"hookReceive"}},{"type":"Image","props":{"y":106,"x":29,"skin":"share/paib2.png","name":"receive"}},{"type":"Image","props":{"y":106,"x":23,"width":22,"skin":"menu/icon_diamond.png","name":"diamondIcon","height":20}},{"type":"Label","props":{"y":106,"x":44,"width":52,"name":"diamondLabel","height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}}]}]},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return ShareUI;
	})(View);
var ShopUI=(function(_super){
		function ShopUI(){
			
		    this.bgImg=null;
		    this.check_fragment=null;
		    this.check_resources=null;
		    this.resourcesList=null;
		    this.fragmentList=null;
		    this.btn_back=null;
		    this.btn_backpack=null;
		    this.buyDialog=null;
		    this.dataLoading=null;

			ShopUI.__super.call(this);
		}

		CLASS$(ShopUI,'ui.ShopUI',_super);
		var __proto__=ShopUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.ShopBuyUI",ui.ShopBuyUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ShopUI.uiView);
		}
		ShopUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":93,"x":19,"skin":"role/di_m.png"}},{"type":"Image","props":{"y":107.99999999999999,"x":185.99999999999994,"skin":"role/btmz.png"}},{"type":"CheckBox","props":{"y":177,"x":245,"var":"check_fragment","stateNum":2,"skin":"backpack/check_fragment.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"CheckBox","props":{"y":177,"x":112,"var":"check_resources","stateNum":2,"skin":"backpack/check_resources.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"List","props":{"y":210,"x":76,"width":500,"var":"resourcesList","vScrollBarSkin":"menu/vscroll.png","spaceY":15,"spaceX":7,"repeatY":4,"repeatX":3,"height":569},"child":[{"type":"Box","props":{"y":0,"x":0,"width":155,"name":"render","height":170},"child":[{"type":"Image","props":{"visible":true,"skin":"role/diwp.png"}},{"type":"Label","props":{"y":10,"x":28,"width":120,"text":"强化尾随导弹：X1","name":"name","italic":true,"height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":145,"x":8,"visible":true,"skin":"role/jllan2a.png"}},{"type":"Image","props":{"y":153,"x":24,"visible":true,"skin":"tool/3002.png","scaleY":0.5,"scaleX":0.5,"name":"priceIcon","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":148,"x":39,"width":74,"name":"price","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":38,"x":25,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":50,"x":40,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":33,"x":93,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":32,"x":77,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":126,"x":79,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"List","props":{"y":210,"x":76,"width":500,"var":"fragmentList","vScrollBarSkin":"menu/vscroll.png","spaceY":15,"spaceX":7,"repeatY":4,"repeatX":3,"height":569},"child":[{"type":"Box","props":{"y":0,"x":0,"width":155,"name":"render","height":170},"child":[{"type":"Image","props":{"visible":true,"skin":"role/diwp.png"}},{"type":"Label","props":{"y":10,"x":28,"width":120,"name":"name","italic":true,"height":14,"fontSize":14,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":145,"x":8,"visible":true,"skin":"role/jllan2a.png"}},{"type":"Image","props":{"y":153,"x":24,"visible":true,"skin":"tool/3002.png","scaleY":0.5,"scaleX":0.5,"name":"priceIcon","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":148,"x":39,"width":74,"name":"price","height":16,"fontSize":16,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":38,"x":25,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":50,"x":40,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":33,"x":93,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":32,"x":77,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":126,"x":79,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Label","props":{"y":789,"x":183,"width":270,"text":"向下滑动可浏览所有物品","height":19,"fontSize":19,"font":"SimHei","color":"#78fff8","align":"center"}},{"type":"Button","props":{"y":894,"x":551,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":894,"x":82,"var":"btn_backpack","stateNum":1,"skin":"warehouse/btn_backpack.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"ShopBuy","props":{"y":0,"x":0,"visible":false,"var":"buyDialog","runtime":"ui.ShopBuyUI"}},{"type":"DataLoading","props":{"y":10,"x":10,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}}]};
		return ShopUI;
	})(View);
var ShopBuyUI=(function(_super){
		function ShopBuyUI(){
			
		    this.btn_dialogClose=null;
		    this.goodsName=null;
		    this.goodsNumber=null;
		    this.goodsText=null;
		    this.quality=null;
		    this.icon=null;
		    this.fragment=null;
		    this.qualityNumber=null;
		    this.starList=null;
		    this.buyNumber=null;
		    this.priceIcon=null;
		    this.buyPrice=null;
		    this.btn_buy=null;
		    this.topNotice=null;
		    this.noticeText=null;

			ShopBuyUI.__super.call(this);
		}

		CLASS$(ShopBuyUI,'ui.ShopBuyUI',_super);
		var __proto__=ShopBuyUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(ShopBuyUI.uiView);
		}
		ShopBuyUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.7},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":238,"x":72,"width":513,"skin":"backpack/zd.png","sizeGrid":"50,105,60,55","height":357}},{"type":"Button","props":{"y":246,"x":553,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":329,"x":108,"width":430,"skin":"rank/diTip.png","sizeGrid":"30,40,30,40","height":178}},{"type":"Label","props":{"y":344.99999999999994,"x":375.9999999999999,"width":28,"text":"件","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"Label","props":{"y":344.99999999999994,"x":240.9999999999999,"width":61,"text":"拥有数量","height":14,"fontSize":14,"font":"SimHei","color":"#f2ffac","align":"right"}},{"type":"Label","props":{"y":302,"x":241.00000000000003,"width":290,"var":"goodsName","height":20,"fontSize":20,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Image","props":{"y":340,"x":315.00000000000006,"skin":"backpack/slk.png"}},{"type":"Label","props":{"y":345,"x":316.99999999999994,"width":46,"var":"goodsNumber","height":16,"fontSize":16,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Label","props":{"y":404.99999999999994,"x":135.99999999999997,"wordWrap":true,"width":375,"var":"goodsText","height":95,"fontSize":16,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Image","props":{"y":283,"x":134,"visible":true,"var":"quality","skin":"quality/1.png"}},{"type":"Image","props":{"y":295,"x":149,"width":71,"var":"icon","height":74}},{"type":"Image","props":{"y":278,"x":202,"var":"fragment","skin":"quality/suipian.png"}},{"type":"Image","props":{"y":277,"x":186,"width":75,"var":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":371,"x":188,"width":80,"visible":true,"var":"starList","repeatY":1,"repeatX":5,"height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]},{"type":"Label","props":{"y":517,"x":253,"width":150,"text":"购买    件","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","align":"center"}},{"type":"Label","props":{"y":517,"x":318,"wordWrap":true,"width":40,"var":"buyNumber","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","align":"center"}},{"type":"Image","props":{"y":541,"x":259,"skin":"backpack/jbk.png"}},{"type":"Image","props":{"y":552,"x":272,"var":"priceIcon","skin":"tool/3001.png","scaleY":0.5,"scaleX":0.5,"anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":545,"x":287,"width":100,"var":"buyPrice","height":20,"fontSize":20,"font":"SimHei","color":"#f2ffac","align":"center"}},{"type":"Image","props":{"y":610,"x":259,"skin":"role/gmbutton2.png"}},{"type":"Button","props":{"y":649,"x":329,"var":"btn_buy","stateNum":1,"skin":"role/btn_buy.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return ShopBuyUI;
	})(View);
var StationUI=(function(_super){
		function StationUI(){
			
		    this.bgImg=null;
		    this.btn_login=null;
		    this.stationDialog=null;
		    this.stationLabel=null;
		    this.stateName=null;
		    this.btn_changeStation=null;

			StationUI.__super.call(this);
		}

		CLASS$(StationUI,'ui.StationUI',_super);
		var __proto__=StationUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.dialogStationUI",ui.dialogStationUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(StationUI.uiView);
		}
		StationUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"station/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Button","props":{"y":730.5,"x":197,"var":"btn_login","stateNum":1,"skin":"station/btn_login.png","centerY":340,"centerX":0},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"dialogStation","props":{"y":53,"x":24,"width":600,"visible":false,"var":"stationDialog","popupCenter":true,"height":600,"runtime":"ui.dialogStationUI"}},{"type":"Image","props":{"y":654,"x":161,"skin":"station/xuanfu1.png","centerY":220,"centerX":0},"child":[{"type":"Image","props":{"y":22,"x":106,"skin":"station/fu.png","anchorY":0.5,"anchorX":0.5}},{"type":"Label","props":{"y":6,"x":22,"width":120,"var":"stationLabel","height":33,"fontSize":24,"color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":6,"x":142,"width":50,"var":"stateName","height":33,"fontSize":26,"bold":true,"align":"center"}},{"type":"Button","props":{"y":22,"x":264,"var":"btn_changeStation","stateNum":1,"skin":"station/btn_changeStation.png","anchorY":0.5,"anchorX":0.5}}]}]};
		return StationUI;
	})(View);
var TaskUI=(function(_super){
		function TaskUI(){
			
		    this.bgImg=null;
		    this.btn_back=null;
		    this.updateTime=null;
		    this.dailyList=null;
		    this.achievementList=null;
		    this.check_achievement=null;
		    this.new_achievement=null;
		    this.check_daily=null;
		    this.new_daily=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.flashLevelUp=null;

			TaskUI.__super.call(this);
		}

		CLASS$(TaskUI,'ui.TaskUI',_super);
		var __proto__=TaskUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.FlashLevelUpUI",ui.FlashLevelUpUI);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(TaskUI.uiView);
		}
		TaskUI.uiView={"type":"View","props":{"width":640,"visible":false,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Image","props":{"y":87,"x":16,"width":620,"skin":"dialogRes/bg_checkIn.png","sizeGrid":"150,250,150,250","height":735}},{"type":"Button","props":{"y":887,"x":547,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":842,"x":18,"skin":"rank/di2.png"}},{"type":"Label","props":{"y":874,"x":29,"wordWrap":true,"width":82,"text":"每天早上","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"right"}},{"type":"Label","props":{"y":874,"x":160,"wordWrap":true,"width":130,"text":"刷新日常任务","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#78fff8","align":"left"}},{"type":"Label","props":{"y":874,"x":113,"wordWrap":true,"width":45,"var":"updateTime","text":"5:00","leading":6,"height":18,"fontSize":18,"font":"SimHei","color":"#ff4593","align":"center"}},{"type":"List","props":{"y":177,"x":44,"width":589,"var":"dailyList","vScrollBarSkin":"menu/vscroll.png","spaceY":3,"repeatY":6,"repeatX":1,"height":622},"child":[{"type":"Box","props":{"y":0,"x":0,"width":575,"visible":true,"name":"render","height":101},"child":[{"type":"Image","props":{"y":1,"visible":true,"skin":"rank/lantiao1.png"}},{"type":"Box","props":{"x":11,"visible":true,"scaleY":0.9,"scaleX":0.9,"name":"icon"},"child":[{"type":"Image","props":{"y":5,"x":1,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]},{"type":"Label","props":{"y":10,"x":136,"width":166,"visible":true,"name":"task_name","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","bold":true,"align":"left"}},{"type":"Label","props":{"y":39,"x":127,"width":325,"visible":true,"name":"task_text","height":14,"fontSize":14,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Image","props":{"y":66,"x":122,"visible":true,"skin":"rank/jl.png"}},{"type":"Sprite","props":{"y":63,"x":177,"width":261,"name":"goods","height":26}},{"type":"Button","props":{"y":35,"x":514,"visible":true,"stateNum":1,"skin":"rank/btn_task_go.png","name":"btn_task_go","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Label","props":{"y":50,"x":-44,"width":128,"visible":true,"text":"99/99","name":"taskSchedule_go","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","align":"right"}},{"type":"Script","props":{"y":0,"x":5.684341886080802e-14,"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":54,"x":513,"visible":true,"stateNum":1,"skin":"rank/btn_task_receive.png","name":"btn_task_receive","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":18,"x":479,"visible":true,"skin":"rank/lanbutton3.png","name":"lanbutton3"}},{"type":"Label","props":{"y":38,"x":453,"width":128,"visible":true,"text":"99/99","name":"taskSchedule","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":1,"x":441,"visible":false,"skin":"rank/lanbutton4.png","name":"taskFinish"}}]}]},{"type":"List","props":{"y":177,"x":44,"width":589,"var":"achievementList","vScrollBarSkin":"menu/vscroll.png","spaceY":3,"repeatY":6,"repeatX":1,"height":622},"child":[{"type":"Box","props":{"y":0,"x":0,"width":575,"visible":true,"name":"render","height":101},"child":[{"type":"Image","props":{"y":1,"visible":true,"skin":"rank/lantiao1.png"}},{"type":"Box","props":{"x":11,"visible":true,"scaleY":0.9,"scaleX":0.9,"name":"icon"},"child":[{"type":"Image","props":{"y":5,"x":1,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":18,"x":15,"width":71,"name":"icon","height":74}},{"type":"Image","props":{"y":1,"x":68,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"x":52,"width":75,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":94,"x":54,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]},{"type":"Label","props":{"y":10,"x":136,"width":166,"visible":true,"name":"task_name","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","bold":true,"align":"left"}},{"type":"Label","props":{"y":39,"x":127,"width":325,"visible":true,"name":"task_text","height":14,"fontSize":14,"font":"SimHei","color":"#0c274c","align":"left"}},{"type":"Image","props":{"y":66,"x":122,"visible":true,"skin":"rank/jl.png"}},{"type":"Sprite","props":{"y":63,"x":177,"width":261,"name":"goods","height":26}},{"type":"Button","props":{"y":35,"x":514,"visible":true,"stateNum":1,"skin":"rank/btn_task_go.png","name":"btn_task_go","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Label","props":{"y":50,"x":-44,"width":128,"visible":true,"text":"99/99","name":"taskSchedule_go","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","align":"right"}},{"type":"Script","props":{"y":0,"x":5.684341886080802e-14,"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":54,"x":513,"visible":true,"stateNum":1,"skin":"rank/btn_task_receive.png","name":"btn_task_receive","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Image","props":{"y":18,"x":479,"visible":true,"skin":"rank/lanbutton3.png","name":"lanbutton3"}},{"type":"Label","props":{"y":38,"x":453,"width":128,"visible":true,"text":"99/99","name":"taskSchedule","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Image","props":{"y":1,"x":441,"visible":false,"skin":"rank/lanbutton4.png","name":"taskFinish"}}]}]},{"type":"CheckBox","props":{"y":137,"x":272,"var":"check_achievement","stateNum":2,"skin":"dialogRes/check_achievement.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":5,"x":20,"visible":false,"var":"new_achievement","skin":"menu/zd.png","anchorY":0.5,"anchorX":0.5}}]},{"type":"CheckBox","props":{"y":137,"x":123,"var":"check_daily","stateNum":2,"skin":"dialogRes/check_daily.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":5,"x":20,"visible":false,"var":"new_daily","skin":"menu/zd.png","anchorY":0.5,"anchorX":0.5}}]},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"FlashLevelUp","props":{"y":0,"x":0,"visible":false,"var":"flashLevelUp","runtime":"ui.FlashLevelUpUI"}}]};
		return TaskUI;
	})(View);
var TipsDialogUI=(function(_super){
		function TipsDialogUI(){
			
		    this.btn_dialogClose=null;
		    this.tipText=null;
		    this.btn_submit=null;
		    this.submitText=null;
		    this.btn_cancel=null;

			TipsDialogUI.__super.call(this);
		}

		CLASS$(TipsDialogUI,'ui.TipsDialogUI',_super);
		var __proto__=TipsDialogUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(TipsDialogUI.uiView);
		}
		TipsDialogUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.5},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":296,"x":115,"skin":"dialogRes/bg.png"}},{"type":"Button","props":{"y":306,"x":500,"var":"btn_dialogClose","stateNum":1,"skin":"dialogRes/button_close.png","name":"close","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Label","props":{"y":334,"x":132,"wordWrap":true,"width":380,"var":"tipText","valign":"middle","height":148,"fontSize":28,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"Button","props":{"y":567,"x":440,"var":"btn_submit","stateNum":1,"skin":"dialogRes/btn_bg.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":12,"x":10,"width":116,"var":"submitText","text":"确定","strokeColor":"#002481","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Button","props":{"y":567,"x":203,"var":"btn_cancel","stateNum":1,"skin":"dialogRes/btn_bg_ye.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Label","props":{"y":12,"x":10,"width":116,"text":"取消","strokeColor":"#813100","stroke":4,"height":26,"fontSize":26,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return TipsDialogUI;
	})(View);
var TopNoticeUI=(function(_super){
		function TopNoticeUI(){
			
		    this.noticeText=null;
		    this.noticeLable=null;

			TopNoticeUI.__super.call(this);
		}

		CLASS$(TopNoticeUI,'ui.TopNoticeUI',_super);
		var __proto__=TopNoticeUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ShowNotice",bean.ShowNotice);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(TopNoticeUI.uiView);
		}
		TopNoticeUI.uiView={"type":"View","props":{"width":640,"isShowNotice":true,"height":960,"runtime":"bean.ShowNotice"},"child":[{"type":"Image","props":{"y":377,"x":0,"skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"HTMLDivElement","props":{"y":10,"x":20,"width":600,"var":"noticeText","height":60}}]},{"type":"Label","props":{"y":387,"x":20,"wordWrap":true,"width":600,"var":"noticeLable","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]};
		return TopNoticeUI;
	})(View);
var VersionVerifyUI=(function(_super){
		function VersionVerifyUI(){
			
		    this.loadBar=null;

			VersionVerifyUI.__super.call(this);
		}

		CLASS$(VersionVerifyUI,'ui.VersionVerifyUI',_super);
		var __proto__=VersionVerifyUI.prototype;
		__proto__.createChildren=function(){
		    
			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(VersionVerifyUI.uiView);
		}
		VersionVerifyUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":121,"x":36,"skin":"activity/logoload.png"}},{"type":"Label","props":{"y":357,"x":3,"width":640,"text":"游戏正在加载","height":50,"fontSize":50,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"Label","props":{"y":419,"x":0,"width":640,"text":"请稍候","height":30,"fontSize":30,"font":"SimHei","color":"#ffffff","bold":true,"align":"center"}},{"type":"ProgressBar","props":{"y":329,"x":95,"var":"loadBar","skin":"activity/progressBar.png"}}]};
		return VersionVerifyUI;
	})(View);
var VipUI=(function(_super){
		function VipUI(){
			
		    this.bgImg=null;
		    this.vipTitle=null;
		    this.vipTitle0=null;
		    this.vipPayment=null;
		    this.vipName=null;
		    this.btn_vip0=null;
		    this.receive0=null;
		    this.receiveOver0=null;
		    this.receiveToday0=null;
		    this.timeTitle0=null;
		    this.timeTitle0_1=null;
		    this.timeValue0=null;
		    this.new_vip0=null;
		    this.btn_vip1=null;
		    this.receive1=null;
		    this.receiveOver1=null;
		    this.receiveToday1=null;
		    this.timeTitle1=null;
		    this.timeTitle1_1=null;
		    this.timeValue1=null;
		    this.new_vip1=null;
		    this.btn_vip2=null;
		    this.receive2=null;
		    this.receiveOver2=null;
		    this.receiveToday2=null;
		    this.timeTitle2=null;
		    this.timeTitle2_1=null;
		    this.timeValue2=null;
		    this.new_vip2=null;
		    this.vipBar=null;
		    this.barPosition0=null;
		    this.barPosition1=null;
		    this.paySchedule=null;
		    this.paymentList=null;
		    this.btn_back=null;
		    this.resourcesInfo=null;
		    this.dataLoading=null;
		    this.flashFighter=null;
		    this.payTip=null;
		    this.topNotice=null;
		    this.noticeText=null;

			VipUI.__super.call(this);
		}

		CLASS$(VipUI,'ui.VipUI',_super);
		var __proto__=VipUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("HTMLDivElement",laya.html.dom.HTMLDivElement);
			View.regComponent("ui.ResourcesInfoUI",ui.ResourcesInfoUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.FlashFigterUI",ui.FlashFigterUI);
			View.regComponent("ui.PayTipUI",ui.PayTipUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(VipUI.uiView);
		}
		VipUI.uiView={"type":"View","props":{"width":640,"height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"Sprite","props":{"y":480,"x":320,"width":640,"scaleY":2,"pivotY":480,"pivotX":320,"height":960,"alpha":0.25},"child":[{"type":"Rect","props":{"y":0,"x":0,"width":640,"lineWidth":1,"height":960,"fillColor":"#000000"}}]},{"type":"Image","props":{"y":52,"x":3,"skin":"role/diye.png"}},{"type":"Image","props":{"y":143,"x":125,"skin":"role/btf.png"}},{"type":"Image","props":{"y":143,"x":510,"skin":"role/btf.png","scaleX":-1}},{"type":"Label","props":{"y":141,"x":161,"width":100,"var":"vipTitle","text":"再充值","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":141,"x":275,"width":121,"var":"vipTitle0","text":"钻石可激活","height":19,"fontSize":19,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":134,"x":220,"wordWrap":true,"width":56,"var":"vipPayment","strokeColor":"#ffffff","stroke":3,"height":28,"fontSize":28,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Label","props":{"y":140,"x":371,"width":107,"var":"vipName","strokeColor":"#ffffff","stroke":3,"height":20,"fontSize":20,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Button","props":{"y":286,"x":121,"var":"btn_vip0","stateNum":1,"skin":"role/btn_vip0.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":97,"visible":false,"var":"receive0","skin":"role/zkklq.png"}},{"type":"Image","props":{"visible":false,"var":"receiveOver0","skin":"role/zklw.png"}},{"type":"Image","props":{"visible":false,"var":"receiveToday0","skin":"role/zklq.png"}},{"type":"Label","props":{"y":85,"x":74.49999999999994,"width":100,"visible":false,"var":"timeTitle0","text":"剩余","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":85,"x":147,"width":38,"visible":false,"var":"timeTitle0_1","text":"天","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":79.99999999999994,"x":108.49999999999994,"width":40,"visible":false,"var":"timeValue0","height":24,"fontSize":24,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Image","props":{"visible":false,"var":"new_vip0","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":286,"x":318,"var":"btn_vip1","stateNum":1,"skin":"role/btn_vip1.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":96.99999999999994,"visible":false,"var":"receive1","skin":"role/zkklq.png"}},{"type":"Image","props":{"visible":false,"var":"receiveOver1","skin":"role/zklw.png"}},{"type":"Image","props":{"visible":false,"var":"receiveToday1","skin":"role/zklq.png"}},{"type":"Label","props":{"y":84.99999999999994,"x":74.49999999999989,"width":100,"visible":false,"var":"timeTitle1","text":"剩余","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":85,"x":147,"width":38,"visible":false,"var":"timeTitle1_1","text":"天","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":80,"x":108.50000000000006,"width":40,"visible":false,"var":"timeValue1","height":24,"fontSize":24,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Image","props":{"visible":false,"var":"new_vip1","skin":"menu/zd.png"}}]},{"type":"Button","props":{"y":286,"x":513,"var":"btn_vip2","stateNum":1,"skin":"role/btn_vip2.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}},{"type":"Image","props":{"y":0,"x":103.49999999999989,"visible":false,"var":"receive2","skin":"role/zkklq.png"}},{"type":"Image","props":{"y":0,"x":6,"visible":false,"var":"receiveOver2","skin":"role/zklw.png"}},{"type":"Image","props":{"y":0,"x":6,"visible":false,"var":"receiveToday2","skin":"role/zklq.png"}},{"type":"Label","props":{"y":85.00000000000006,"x":74.49999999999994,"width":100,"visible":false,"var":"timeTitle2","text":"剩余","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":85,"x":147,"width":38,"visible":false,"var":"timeTitle2_1","text":"天","height":18,"fontSize":18,"font":"SimHei","color":"#ffffff","align":"left"}},{"type":"Label","props":{"y":84,"x":74,"width":92,"visible":false,"var":"timeValue2","text":"可无限领取","height":20,"fontSize":20,"font":"SimHei","color":"#ff4593","bold":true,"align":"center"}},{"type":"Image","props":{"visible":false,"var":"new_vip2","skin":"menu/zd.png"}}]},{"type":"Image","props":{"y":830,"x":288,"skin":"warehouse/biao.png"}},{"type":"Image","props":{"y":567,"x":288,"skin":"warehouse/biao.png","scaleY":-1}},{"type":"ProgressBar","props":{"y":376,"x":55,"var":"vipBar","skin":"role/progressBar_vip.png","sizeGrid":"70,14,11,17,1"}},{"type":"Image","props":{"y":421,"x":549,"skin":"role/jdb.png"}},{"type":"Image","props":{"y":421,"x":272,"var":"barPosition0","skin":"role/jdb.png"}},{"type":"Image","props":{"y":421,"x":111,"var":"barPosition1","skin":"role/jdb.png"}},{"type":"Label","props":{"y":478.00000000000006,"x":118.99999999999999,"width":400,"var":"paySchedule","italic":true,"height":20,"fontSize":20,"font":"SimHei","color":"#ffffff","align":"center"}},{"type":"List","props":{"y":572,"x":47,"width":570,"var":"paymentList","vScrollBarSkin":"menu/vscroll.png","spaceY":8,"repeatY":3,"repeatX":1,"height":265},"child":[{"type":"Box","props":{"y":0,"x":0,"width":557,"name":"render","height":127},"child":[{"type":"Image","props":{"visible":true,"skin":"role/lan.png"}},{"type":"Label","props":{"y":22,"x":155,"width":148,"name":"name","height":18,"fontSize":18,"font":"SimHei","color":"#0c274c","bold":true,"align":"left"}},{"type":"Label","props":{"y":7,"x":479,"width":65,"visible":true,"name":"price","height":26,"fontSize":26,"font":"SimHei","color":"#f2ffac","align":"left"}},{"type":"HTMLDivElement","props":{"y":54,"x":161,"width":280,"name":"amount","height":20}},{"type":"HTMLDivElement","props":{"y":78,"x":162,"width":270,"name":"gift","height":20}},{"type":"Image","props":{"y":18,"x":24,"visible":true,"skin":"quality/1.png","name":"quality"}},{"type":"Image","props":{"y":30,"x":39,"width":71,"visible":true,"name":"icon","height":74}},{"type":"Image","props":{"y":13,"x":92,"visible":true,"skin":"quality/suipian.png","name":"fragment"}},{"type":"Image","props":{"y":12,"x":76,"width":75,"visible":true,"name":"qualityNumber","height":21,"anchorX":0.5}},{"type":"List","props":{"y":106,"x":78,"width":80,"visible":true,"repeatY":1,"repeatX":5,"name":"starList","height":15,"anchorX":0.5},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"skin":"quality/mingxin.png"}}]}]}]}]},{"type":"Button","props":{"y":895,"x":569,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"ResourcesInfo","props":{"y":0,"x":0,"var":"resourcesInfo","runtime":"ui.ResourcesInfoUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"FlashFigter","props":{"y":0,"x":0,"visible":false,"var":"flashFighter","runtime":"ui.FlashFigterUI"}},{"type":"PayTip","props":{"y":0,"x":0,"visible":false,"var":"payTip","runtime":"ui.PayTipUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return VipUI;
	})(View);
var WorldUI=(function(_super){
		function WorldUI(){
			
		    this.bgImg=null;
		    this.chapterList=null;
		    this.chapters=null;
		    this.world_bottom=null;
		    this.moppingUpText=null;
		    this.btn_back=null;
		    this.btn_backpack=null;
		    this.btn_moppingUp=null;
		    this.btn_battle=null;
		    this.chapterDialog=null;
		    this.reward=null;
		    this.dataLoading=null;
		    this.flashLevelUp=null;
		    this.flashFighter=null;
		    this.topNotice=null;
		    this.noticeText=null;

			WorldUI.__super.call(this);
		}

		CLASS$(WorldUI,'ui.WorldUI',_super);
		var __proto__=WorldUI.prototype;
		__proto__.createChildren=function(){
		    			View.regComponent("bean.ScaleButton",bean.ScaleButton);
			View.regComponent("ui.LevelDifficultyUI",ui.LevelDifficultyUI);
			View.regComponent("ui.RewardUI",ui.RewardUI);
			View.regComponent("ui.DataLoadingUI",ui.DataLoadingUI);
			View.regComponent("ui.FlashLevelUpUI",ui.FlashLevelUpUI);
			View.regComponent("ui.FlashFigterUI",ui.FlashFigterUI);
			View.regComponent("bean.ShowNotice",bean.ShowNotice);

			laya.ui.Component.prototype.createChildren.call(this);
			this.createView(WorldUI.uiView);
		}
		WorldUI.uiView={"type":"View","props":{"width":640,"source":"shield/shield0.png,shield/shield1.png,shield/shield2.png,shield/shield3.png,shield/shield4.png,shield/shield5.png,shield/shield6.png,shield/shield7.png","height":960},"child":[{"type":"Image","props":{"y":480,"x":320,"var":"bgImg","skin":"menu/bg.jpg","anchorY":0.5,"anchorX":0.5}},{"type":"List","props":{"y":120,"x":0,"width":640,"visible":false,"var":"chapterList","vScrollBarSkin":"menu/vscroll.png","height":841},"child":[{"type":"Box","props":{"name":"render"},"child":[{"type":"Image","props":{"y":119.00000000000006,"x":47,"visible":true,"skin":"world/gkhuanjy.png","name":"aperture","anchorY":0.5,"anchorX":0.5}},{"type":"Button","props":{"y":120.00000000000006,"x":49.00000000000017,"visible":true,"stateNum":1,"skin":"world/btn_pass.png","name":"btn_chapter","anchorY":1,"anchorX":0.5}},{"type":"Image","props":{"y":-0.9999999999999432,"x":17.00000000000017,"skin":"world/gkboss.png","name":"boss"}},{"type":"Label","props":{"y":14.000000000000057,"x":-0.9999999999998295,"width":100,"name":"chapterNumber","height":39,"font":"myFontChapter","align":"center"}}]}]},{"type":"Panel","props":{"y":20,"x":0,"width":640,"var":"chapters","vScrollBarSkin":"menu/vscroll.png","height":1040}},{"type":"Panel","props":{"y":806,"x":7,"width":633,"var":"world_bottom","height":154},"child":[{"type":"Image","props":{"x":70,"skin":"world/zd.png"},"child":[{"type":"Label","props":{"y":8,"x":8,"width":317,"var":"moppingUpText","text":"次日开始，“每日可进行一次扫荡操作”","height":17,"fontSize":17,"font":"SimHei","color":"#ffffff","align":"center"}}]},{"type":"Button","props":{"y":89,"x":562,"var":"btn_back","stateNum":1,"skin":"warehouse/btn_back.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":88,"x":66,"var":"btn_backpack","stateNum":1,"skin":"warehouse/btn_backpack.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":88,"x":234,"var":"btn_moppingUp","stateNum":1,"skin":"world/btn_moppingUp.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]},{"type":"Button","props":{"y":88,"x":394,"var":"btn_battle","stateNum":1,"skin":"world/btn_battle.png","anchorY":0.5,"anchorX":0.5},"child":[{"type":"Script","props":{"isScale":true,"runtime":"bean.ScaleButton"}}]}]},{"type":"LevelDifficulty","props":{"y":0,"x":0,"visible":false,"var":"chapterDialog","runtime":"ui.LevelDifficultyUI"}},{"type":"Reward","props":{"y":0,"x":0,"visible":false,"var":"reward","runtime":"ui.RewardUI"}},{"type":"DataLoading","props":{"y":0,"x":0,"visible":false,"var":"dataLoading","runtime":"ui.DataLoadingUI"}},{"type":"FlashLevelUp","props":{"y":0,"x":0,"visible":false,"var":"flashLevelUp","runtime":"ui.FlashLevelUpUI"}},{"type":"FlashFigter","props":{"y":0,"x":0,"visible":false,"var":"flashFighter","runtime":"ui.FlashFigterUI"}},{"type":"Image","props":{"y":377,"x":0,"visible":false,"var":"topNotice","skin":"world/ditc.png","isShowNotice":true,"runtime":"bean.ShowNotice"},"child":[{"type":"Label","props":{"y":10,"x":20,"wordWrap":true,"width":600,"var":"noticeText","valign":"middle","height":60,"fontSize":30,"font":"SimHei","color":"#ffffff","align":"center"}}]}]};
		return WorldUI;
	})(View);