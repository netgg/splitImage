# coding: utf-8
import os
import sys
import json
import time
from PIL import Image

def checkPath(path):
	if not os.path.exists( path ):
		print "not find 1 %s"%path
		return False
	return True


def splitImage(path, fileName, outPath ):
	# 检查JSON文件 
	jsonPath = os.path.join(path, "%s.json"%fileName  )
	if not os.path.exists( jsonPath ):
		jsonPath = os.path.join( path, "%s.atlas"%(fileName ))
		if not os.path.exists( jsonPath ):
			print "not find 0 {}".format(jsonPath)
			return
	
	# 检查PNG文件 
	pngPath = os.path.join( path, "%s.png"%fileName )
	if checkPath(pngPath) == False:
		return
	
	# 检查输出目录
	outPath = os.path.join( path, outPath )
	if not os.path.isdir( outPath ):
		os.mkdir( outPath )
		
	# 取JSON文件
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	f.close()
	jsonData = json.loads( fileStr ) 
	
	#检查image集合
	meta = jsonData.get( "meta" )
	imageStr = meta.get( "image" )
	
	#拆分文件名
	images = imageStr.split(",") 
	
	#拆分文件名
	images = imageStr.split(",") 
	
	imgList = []
	
	#打开多个文件准备切割
	for img in images: 
		pngPath = os.path.join( path, img ) 
		pngPath = pngPath.replace("~","-")
		if not os.path.exists( pngPath ):
			print "not find 2 %s"%pngPath 
			break;
		imgList.append(Image.open( pngPath, 'r' ))
	
	# 开始切图
	lastIdx = 0
	frames = jsonData.get( "frames" )
	for fn in frames.keys():
		data = frames.get( fn )
		frame = data.get( "frame" )
		idx = frame.get( "idx" ) 
		x = frame.get("x")
		y = frame.get("y")
		w = frame.get("w")
		h = frame.get("h")
		box = ( x, y, x+w, y+h )
		outFile = os.path.join( outPath, fn )
		imgData = imgList[idx].crop( box )
		imgData.save( outFile, 'png' )

#读取指定目录	 
def find_file( path, outPath):
	for name in os.listdir( path ): 
		if os.path.isdir( os.path.join(path, name) ): 
			find_file(os.path.join(path, name),outPath )
		else:
			portion = os.path.splitext(name)
			if portion[1] == '.atlas' or portion[1] == '.json': 
				fileName = os.path.join(path, portion[0])
				outDir = os.path.join(outPath, portion[0]); 
				splitImage(path,fileName , outDir)
				


if __name__=='__main__':
	# 取得参数
	if len( sys.argv ) < 2:
		target = raw_input("Enter your DirName: ")
	else:
		target = sys.argv[1]
	if len( sys.argv ) < 3:
		outPath = raw_input("Enter your outPath: ")
	else:
		outPath = sys.argv[2]

	outPath = os.path.join( os.getcwd(), outPath )
	if not os.path.isdir( outPath ):
		os.mkdir( outPath )

	path =  os.path.join(os.getcwd(),target)
	if checkPath(path): 
		find_file(path,outPath)
	