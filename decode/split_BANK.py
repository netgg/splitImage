# coding: utf-8
import os
import sys
import time
import json 
from compare import compareJson

cCompId = 0
PAGEDATA = {}

'''
读取json数据
'''
def readDataJson(jsonPath):
	if not os.path.exists( jsonPath ): 
		return None
		
	f = open( jsonPath, 'r' )
	fileStr = f.read()
	jsonData = json.loads( fileStr )
	f.close() 
	return jsonData

def writeDataJson(exportName,datas):
	file = open( exportName, 'w+' )
	json_data = json.dumps( datas,indent=4 )
	#formatR = formatR.replace(" ", "");
	file.write( json_data )
	file.close()

#设置子属性模板
def getChildTemplete():
	return { 
		"x":0,
		"nodeParent":8,
		"label":"Sprite",
		"isDirectory":False,
		"isAniNode":True,
		"hasChild":False,
		"isOpen":True,
		"child":[]	
	}

#自增compID
def getCompID():
	global cCompId
	cCompId = cCompId + 1
	return cCompId

#组装子节点
def readChild(value,fileName): 
	array = [] 
	for each in value:
		d = getChildTemplete()
		#//属性级别
		child3 = each.items()
		for k, v in child3:
			# if k =="props":
			# 	if "skin" in v:
			# 		skin = v.get("skin")
			# 		if skin.find("big")>-1 :
			# 			print skin

			 
			if k =="type":
				props = each.get(u"props") 
				name = None
				if "var" in props:
					name = props.get("var")
				if name == None or name == v :
					d.update( { "searchKey":v})  
				else:
					d.update( { "searchKey":"{0},{1}".format(v,name)})  
				d.update({ k:v })

			elif k == "child":
				if len(v)>1:
					d.update({ "hasChild":True })
					d.update({ "isDirectory":True })
				d.update({ k:readChild(v,fileName) }) 
			elif k == "type": 
				d.update({ "x":15 })
			else: 
				d.update({ k:v })
		array.append(d)
	
	return array

def reloadJson(items,fileName):
	#场景文件还原申明
	configs = {
		"x":0,
		"type":"Scene",
		"selectedBox":2,
		"searchKey":"Scene",
		"selecteID":3,
		"nodeParent":-1,
		"maxID":14,
		"label":"Scene",
		"isOpen":True,
		"isDirectory":False,
		"isAniNode":False,
   		"hasChild":False,
		"compId":2,
		"props":{},
		"child":[],
		"animations":[],
	}
	for key, value in items:
		if key == "compId":
			configs.update( { key:value })
		if key == "type" and value == "Scene":
			configs.update( { key:value }) 
			configs.update( { "searchKey":value})
			configs.update( { "label":value }) 
		if key == "props":
			value.update({ "sceneColor":"#000000" })
			configs.update( { key:value })  
		if key == "child":
			if len(value)>1: 
				configs.update({ "isDirectory":True })
				configs.update({ "hasChild":True })
			configs.update({key:readChild(value,fileName)})
		
	return configs


def loadScene(path,outDir,fileName):
		data = readDataJson(path)
		if data == None:
			print u"读取失败"+path
		else:
			items = data.items() 
			configs = reloadJson(items,fileName)
			#保存为场景文件
			writeDataJson(outDir+".scene",configs) 
		

#读取指定目录	 
def find_file( path, outPath):
	for name in os.listdir( path ): 
		if os.path.isdir( os.path.join(path, name) ): 
			find_file(os.path.join(path, name),outPath )
		else:
			portion = os.path.splitext(name) 
			if portion[1] == '.json':
				#print u"开始解析：{0}".format(name)
				outDir = os.path.join(outPath,portion[0])
				tarPath = os.path.join(path,name)  
				loadScene(tarPath,outDir,name)
				#print u"完成解析："+name
			

if __name__=='__main__':
	target = os.path.join(os.getcwd(),"test")
	outDir = os.path.join(os.getcwd(),"newScene2")
	if not os.path.isdir( outDir ):
		os.mkdir( outDir )

	#预读取嵌套文件
	PAGEDATA = readDataJson(os.path.join(os.getcwd(),"pageData.json"))
	 

	find_file(target,outDir)
	#读取文件  

