const FS = require("fire-fs");
const PATH = require('fire-path');
const Electron = require('electron');
var http = require("http");

var CfgUtil = Editor.require("packages://fetch/core/CfgUtil");
var FileDown = Editor.require("packages://fetch/core/FileDown");

Editor.Panel.extend({
    style: FS.readFileSync(Editor.url('packages://fetch/panel/index.css', 'utf8')) + "",
    template: FS.readFileSync(Editor.url('packages://fetch/panel/index.html', 'utf8')) + "",

    $: {
        logTextArea: '#logTextArea',
    },

    ready() {
        let logCtrl = this.$logTextArea;
        let logListScrollToBottom = function () {
            setTimeout(function () {
                logCtrl.scrollTop = logCtrl.scrollHeight;
            }, 10);
        };

        window.plugin = new window.Vue({
            el: this.shadowRoot,
            created() {
                console.log("created");
                this.initPlugin();
            },
            data: {
                gameNetAddr:"",
                gameAddr: "", 
                rawAssets: null,
                importArr :null,
                uuids:null,
                packedAssets:null,
                localSettingsPath:"",
                isNetFrist:true,
                localDownDir:"",
                gameBaseAddr:"",
                isAutoDown:false,
                isMd5 : false,
                gameName:"test",
                AllUrlPath : [], //存储网络上全部资源路径
                logView: [],
                resRealNameRawAssets:{}, // 下载后的资源需要一个真实名字
            },
            methods: {
                _addLog(str,flag) {
                    if(flag){
                        this.logView += "[" + flag + "]: " + str + "\n";
                    }else{
                        let time = new Date();
                        this.logView += "[" + time.toLocaleString() + "]: " + str + "\n";
                    } 
                    logListScrollToBottom();
                }, 
                //初始化插件
				initPlugin(){
                    CfgUtil.initCfg(function (data) {
                        if (data) {
                           this.localSettingsPath = data.localSettingsPath;
                           this.isAutoDown = data.isAutoDown;
                           this.localDownDir  = data.localDownDir;
                           this.gameBaseAddr = data.gameBaseAddr;
                           this.gameNetAddr  = data.settingsPath;
                           this.isMd5 = data.isMd5;
                           this.gameName = data.gameName;
                        } else {
                            this._saveConfig();
                        } 
                    }.bind(this));
                },

                onChangeGameBaseAddr() { 
                    CfgUtil.setGameBaseAddr(this.gameBaseAddr);
                }, 

                onChangeNetGameSetting(){
                    CfgUtil.setNetGameSetting(this.gameNetAddr);
                },

                _isNum(str) {
                    let reg = /^[0-9]+.?[0-9]*$/;
                    if (reg.test(str)) {
                        return true;
                    } else {
                        return false;
                    }
                },

                //保存配置信息
                _saveConfig() { 
                    CfgUtil.saveConfig({
                        localSettingsPath:this.localSettingsPath, 
                    });
                    this._addLog("保存配置")
                },
                  
                onOpenLocalSetting(){
                    if (!FS.existsSync(this.localSettingsPath)) {
                        this._addLog("目录不存在：" + this.localSettingsPath);
                        return;
                    }
                    Electron.shell.showItemInFolder(this.localSettingsPath);
                    Electron.shell.beep();
                },

                onOpenDownDirPath(){
                    if (!FS.existsSync(this.localDownDir)) {
                        this._addLog("目录不存在：" + this.localDownDir);
                        return;
                    }
                    Electron.shell.showItemInFolder(this.localDownDir);
                    Electron.shell.beep();
                },
 
                onBuildFinished() {
                     
                },

                //打开本地setting文件位置
                onSelectLocalSettingsPath(){
                    let path = Editor.Project.path;
                    if (this.localSettingsPath && this.localSettingsPath.length > 0) {
                        if (FS.existsSync(this.localSettingsPath)) {
                            path = this.localSettingsPath;
                        }
                    }
                    let res = Editor.Dialog.openFile({
                        title: "选择本地Settings目录",
                        defaultPath: path,
                        properties: ['openFile'],
                    });

                    if (res !== -1) {
                        this.localSettingsPath = res[0];
                        this._addLog(this.localSettingsPath)
                        this._saveConfig(); 
                    } 
                },

                 //打开本地setting文件位置
                 onSelectDownDirPath(){
                    let path = Editor.Project.path;
                    if (this.localDownDir && this.localDownDir.length > 0) {
                        if (FS.existsSync(this.localDownDir)) {
                            path = this.localDownDir;
                        }
                    }
                    let res = Editor.Dialog.openFile({
                        title: "选择本地下载存放目录",
                        defaultPath: path,
                        properties: ['openDirectory'],
                    });

                    if (res !== -1) {
                        this.localDownDir = res[0];
                        this._addLog(this.localDownDir)
                        CfgUtil.setDownDir(this.localDownDir)
                    } 
                },

                onChangeGameName(){
                    CfgUtil.commSet("isMd5",this.gameName );
                },

                clickNetFrist(){ 
                    this.isNetFrist = !this.isNetFrist; 
                    CfgUtil.setIsNetFrist(this.isNetFrist);
                },

                //解析完成后自动下载资源
                clickAutoDown(){
                    this.isAutoDown = !this.isAutoDown;
                    console.log(this.isAutoDown)
                    CfgUtil.setIsAutoDown(this.isAutoDown);
                },
 
                clickIsMd5(){
                    this.isMd5 = !this.isMd5; 
                    CfgUtil.commSet("isMd5",this.isMd5 );
                },

 
                _statrAnalysis(){

                    //this.UuidUtils("f62aVfnk9LMIX8PGB6svWo");   

                    this._addLog("开始解析"+this.localSettingsPath)
                    require(this.localSettingsPath);
                    this.uuids =  window._CCSettings.uuids; 
                    this.packedAssets = window._CCSettings.packedAssets;
                     
                    if(this.isMd5){
                        this.importArr =  window._CCSettings.md5AssetsMap["import"];
                        this.rawAssets =  window._CCSettings.md5AssetsMap["raw-assets"];
                          //生成import文件路径
                         this._generateUrl(this.importArr,1);
                         //生成raw-assets文件路径
                         this._generateUrl(this.rawAssets,2);
                    }else{   
                        //未加密的走这块  rawAssets 是resources目录下的
                        this._generateResAssest(); 
                        //直接下载没加密的
                        this._generateScene(this.packedAssets);  

                        // let url =  "http://s5.4399.com/4399swf/upload_swf/ftp32/liuxinyu/20200602/6/res/raw-assets/5f/5fca8235-d50b-4732-b1a5-38c9fc6f3294.mp3"
                        // FileDown.startDownloadTask(url,"E:\\testlesson\\ui\\packages", "test.mp3"); 
                    } 
                     //开始自动下载
                    if(this.isAutoDown){
                       this.startDown();
                    } 
                }, 

                //启动解析
                onBtnClickAnalysis(){ 
                    //先从网络上下载
                    let path = Editor.Project.path + "/packages/fetch/settings/";
                    this.localSettingsPath = path+"settings.js"; 
                    if(this.isNetFrist){  
                        FileDown.startDownloadTask(this.gameNetAddr,path, "settings.js",()=>{
                            this._saveConfig();
                            this._addLog("下载Setting.js完成")
                            this._statrAnalysis(); 
                        });  
                    }else{
                        this._statrAnalysis();
                    } 
                },

                _generateResAssest(){
                    var rawAssets = window._CCSettings.rawAssets;
                    var assetTypes = window._CCSettings.assetTypes
                    var realRawAssets = window._CCSettings.rawAssets = {};
                    this.resRealNameRawAssets = {};
                    //获取resources下资源的全部路径
                    for (var mount in rawAssets) {
                        var entries = rawAssets[mount];
                        var realEntries = realRawAssets[mount] = {};
                        for (var id in entries) {
                            var entry = entries[id];
                            var type = entry[1];
                            // retrieve minified raw asset
                            if (typeof type === 'number') {
                                entry[1] = assetTypes[type];
                            }
                            // retrieve uuid
                            realEntries[this.uuids[id] || id] = entry;
                        } 
                    } 
                    let assets = realRawAssets.assets;
                    for (const key in assets) { 
                        const value = assets[key];  
                        let fileName = Editor.Utils.UuidUtils.decompressUuid(key); 
                        let dirName = key.substring(0,2)   
                        let isOK = false;
                        let ext = FileDown.getFilename(value[0],2)
                        switch (value[1]) {
                            case "cc.Texture2D":
                                fileName = "res/raw-assets/"+dirName+"/"+fileName+ext
                                isOK = true;
                                break;
                            case "cc.AudioClip":
                                fileName = "res/raw-assets/"+dirName+"/"+fileName+ext 
                                isOK = true;
                                break;
                            case "cc.EffectAsset": 
                                break;
                            case "cc.SpriteFrame": 
                                break; 
                            case "cc.JsonAsset":
                                fileName = "res/import/"+dirName+"/"+fileName+ext
                                isOK = true;
                                break; 
                            case "cc.Material":
                                break
                            case "cc.SpriteAtlas":  
                                if(ext == ".pac"){
                                    fileName = "res/import/"+dirName+"/"+fileName + ".json"; 
                                }
                                break
                            default:
                                break;
                        }
                        if(isOK == false) continue; 
                        this.resRealNameRawAssets[fileName] = {
                            realPath:value[0],
                            type:value[1]
                        };
                    } 
                },
 
	            //生成场景文件  import目录
                _generateScene(packedAssets){
                    let fileName;
                    for (const key in packedAssets) {  
                        let dirName = key.substring(0,2) 
                        let jsonPaht ="res/import/" + dirName + "/"+ key + ".json";  
                        this.AllUrlPath.push(jsonPaht);  
                        const values = packedAssets[key];
                        for (let index = 0; index < values.length; index++) { 
                            const value = values[index];  
                            if(typeof(value) == "number"){ 
                                fileName = Editor.Utils.UuidUtils.decompressUuid(key +value); 
                                //Editor.log(value+ "||" +fileName,"LOG")
                            }else{
                                fileName = Editor.Utils.UuidUtils.decompressUuid(value);  
                                let dirName2 = value.substring(0,2) 
                                let jsonPaht ="res/raw-assets/" + dirName2 + "/"+ fileName + ".png"; 
                                this.AllUrlPath.push(jsonPaht); 
                               // Editor.log(fileName,"LOG")
                            } 
                        } 
                    }  
                },

                //启动文件下载
                startDown(){  
                    let downPath = this.localDownDir + "/"+this.gameName;
                    //下载的不在resources文件夹里面的
                    for (let index = 0; index < this.AllUrlPath.length; index++) {
                        const url = this.gameBaseAddr + this.AllUrlPath[index];  
                        let fileName = FileDown.getFilename(url);
                        FileDown.startDownloadTask(url,downPath, fileName); 
                    }

                    //下载在resources文件夹里面的
                    for (const key in this.resRealNameRawAssets) { 
                        const element = this.resRealNameRawAssets[key];
                        const url = this.gameBaseAddr + key;
                        let dirName = downPath +"/resources/"+ FileDown.getDirName(element.realPath) ;
                        let fileName = FileDown.getFilename(element.realPath); 
                        FileDown.mkdirs(dirName,function(url,dirName, fileName){
                            FileDown.startDownloadTask(url,dirName, fileName); 
                        }.bind(this,url,dirName, fileName)) 
                    } 
                    this._addLog("已经全部下载完成");
                },

				//生成raw-assets  import目录
				_generateUrl(rawAssets,type){
                    let _seq = 1;
                    let fileName = ""
                    let resImportPath = ""
                    //找到全部raw-assets
                    for( let k = 0; k < rawAssets.length; k++){
                        const el = rawAssets[k];  
                        if(_seq == 2){
                            resImportPath = "";
                            _seq = 0;  
                            if( CfgUtil.inArray(fileName,this.uuids)){  
                                fileName = Editor.Utils.UuidUtils.decompressUuid(fileName); 
                            } 
                            fileName = fileName+ "." +el;
                            //查找到资源路径
                            let dirName = fileName.substring(0,2) 
                            if(type == 2){
                                resImportPath ="res/raw-assets/" + dirName + "/"+ fileName + ".png";;
                            }else{
                                resImportPath ="res/import/" + dirName + "/"+ fileName + ".json";;
                            } 
                            Editor.log(resImportPath);
                            this.AllUrlPath.push(resImportPath); 
                        }else{
                            fileName = el;
                        }
                        _seq += 1;
                    }
                },  
                   
                //UUID转换
                UuidUtils(uuid){ 
                    let decompressUuid=Editor.Utils.UuidUtils.decompressUuid(uuid); 
                    this._addLog(decompressUuid);
                }
            },
        })
    },

    messages: {
        'fetch:onBuildFinished'(event) {
            window.plugin.onBuildFinished();
        }
    }
});