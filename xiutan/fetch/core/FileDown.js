var FS = require('fire-fs');
var path = require('path');
const {remote} = require('electron');

let self = module.exports = {
        /**
         * 下载开始
         */
        startDownloadTask (imgSrc, dirName,fileName,call) {
            console.log("start downloading " + imgSrc);
            var req = http.request(imgSrc, this.getHttpReqCallback(imgSrc, dirName, fileName,call));
            req.setMaxListeners(50);
            req.setHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36');
            req.on('error', function(e){
                console.log("request " + imgSrc + " error, try again");
            });
            req.end();
        },
    /**
     * 下载回调
     */   
        getHttpReqCallback(imgSrc, dirName, fileName,call) { 
            var callback = function(res) { 
                if(res.statusCode == 404){
                    //Editor.log("request: " + imgSrc + " return status: " + res.statusCode);
                    return
                } 

                res.setEncoding("binary");
                var contentLength = parseInt(res.headers['content-length']); 
                var downLength = 0; 
                var fileData = "";
                res.on('data', function (chunk) {
                    fileData +=chunk;
                    downLength += chunk.length;
                    //var progress =  Math.floor(downLength*100 / contentLength);
                    // var str = "下载："+ progress +"%";
                    // Editor.log(str); 
                    //写文件
                    // out.write(chunk,function () {
                    //     //console.log(chunk.length); 
                    // });
                    
                });
                res.on('end', function() {
                    downFlag = false;
                    console.log("end downloading " + imgSrc);
                    if (isNaN(contentLength)) {
                        console.log(imgSrc + " content length error");
                        return;
                    }
                    if (downLength < contentLength) {
                        console.log(imgSrc + " download error, try again");
                        return;
                    }

                    FS.writeFile(dirName + "/" + fileName,fileData,"binary",function(err){
                        if(err){
                            console.log("[downloadPic]文件   "+fileName+"  下载失败.");
                            console.log(err);
                        }else{
                            console.log("文件"+fileName+"下载成功");
                            call && call();
                        } 
                    }); 

                });
            }; 
            return callback;
        },

        getFilename(filepath = () => { throw new  Error }, type = 1) { 
            let result = '';
            if (type === 1) {
                result = path.basename(filepath);
            } else if (type === 2) {
                result = path.extname(filepath);
            } else {
                let basename = path.basename(filepath);
                let extname = path.extname(filepath);
                result = basename.substring(0, basename.indexOf(extname));
            }
            return result;
        },

        getDirName(f){ 
            return path.dirname(f)
        },

        // 递归创建目录 同步方法
        mkdirsSync(dirname) {
            if (FS.existsSync(dirname)) {
                return true;
            } else {
                if (mkdirsSync(path.dirname(dirname))) {
                    FS.mkdirSync(dirname);
                    return true;
                }
            }
        }, 
        // 递归创建目录 异步方法  
        mkdirs(dirname, callback) {  
            FS.exists(dirname, function (exists) {  
                if (exists) {  
                    callback();  
                } else {   
                    self.mkdirs(path.dirname(dirname), function () {  
                        FS.mkdir(dirname, callback);  
                        console.log('在' + path.dirname(dirname) + '目录创建好' + dirname  +'目录');
                    });  
                }  
            });  
        }    
 
};