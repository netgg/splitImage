var FS = require('fire-fs');
var path = require('path');
const {remote} = require('electron');

let self = module.exports = {

    inArray(search,array){
        for(var i in array){
            if(array[i]==search){
                return true;
            }
        }
        return false;
    },
 
    cfgData: {
        gameAddr: null, 
        settingsPath:null,
        isAutoDown:false,
        localSettingsPath:null,
        localDownDir:null,
        isMd5:false,
        gameName:"", 
    },

    commSet(key,value){
        if(!this.cfgData[key]){
            console.log("key  not valid!" +key);
            return
        }
        this.cfgData[key] = value;
        this.saveConfig(); 
    }, 

    setIsNetFrist(b){
        this.cfgData.isNetFrist= b;
        this.saveConfig(); 
    },

    setIsAutoDown(b){
        this.cfgData.isAutoDown= b;
        this.saveConfig();
    },

    setNetGameSetting(addr){
        this.cfgData.settingsPath= addr;
        this.saveConfig();
    },

    setGameBaseAddr(addr){
        this.cfgData.gameBaseAddr= addr;
        this.saveConfig();
    }, 

    setDownDir(path){
        this.cfgData.localDownDir= path;
        this.saveConfig();
    },
  
    saveConfig(data) {
        let configFilePath = self._getAppCfgPath(); 
        if(data && data.localSettingsPath){
            this.cfgData.localSettingsPath = data.localSettingsPath;  
        }
        FS.writeFile(configFilePath, JSON.stringify(this.cfgData), function (error) {
            if (!error) {
                console.log("保存配置成功!");
            }
        }.bind(this));
    },
    cleanConfig() {
        FS.unlink(this._getAppCfgPath());
    },
    _getAppCfgPath() {
        let userDataPath = remote.app.getPath('userData');
        let tar = Editor.libraryPath;
        tar = tar.replace(/\\/g, '-');
        tar = tar.replace(/:/g, '-');
        tar = tar.replace(/\//g, '-');
        return path.join(userDataPath, "bitmap-font-cfg-" + tar + ".json");
        // return Editor.url('packages://hot-update-tools/save/cfg.json');
    },
    initCfg(cb) {
        let configFilePath = this._getAppCfgPath();
        if (FS.existsSync(configFilePath)) {
            console.log("cfg path: " + configFilePath);
            FS.readFile(configFilePath, 'utf-8', function (err, data) {
                if (!err) {
                    let saveData = JSON.parse(data.toString());
                    self.cfgData = saveData;
                    if (cb) {
                        cb(saveData);
                    }
                }
            }.bind(self));
        } else {
            if (cb) {
                cb(null);
            }
        }
    }
};