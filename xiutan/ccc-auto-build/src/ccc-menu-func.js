/**
 * 主要用户菜单触发函数的响应
 */
const path              = require("path")
const PACKAGE_NAME      = require('../package.json').name;
const scriptTemplate    = require('../package.json').scriptTemplate;
const StringUtil        = require("./utils/string-util");
const FileUtil          = require("./utils/file-util")
let ASSETS_TYPE_MAP = {'sprite-atlas':"cc.SpriteAtlas",'sprite-frame':"cc.SpriteFrame",'texture':"cc.SpriteFrame",'prefab':"cc.Prefab",'audio-clip':'cc.AudioClip','raw-asset':'cc.RawAsset'};
 
 
 
 
 const CCCMENU_FUNC = { 

    commAssets(){
        let insertUuids = Editor.Selection.curSelection('asset');
        if(insertUuids == null || insertUuids.length == 0){
			return false;
		} 
        
        Editor.assetdb.queryInfoByUuid(insertUuids[0],(_,fileInfo)=>{


        })
    },

    /**
     * 创建弹窗页面模板
     */
    onCeratePopLayer(){
        let insertUuids = Editor.Selection.curSelection("asset");
    
        if(insertUuids == null || insertUuids.length == 0){ 
			return false;
		}   

        let selectPath =  Editor.assetdb.uuidToUrl(insertUuids[0]);
        if(selectPath.indexOf("Script") <= -1){ 
            Editor.warn('请选中Script脚本路径')
            return 
        } 
        CCCMENU_FUNC.createTs(selectPath);  
    },


    createTs(selectPath){

        let tsScriptPath = Editor.url(`packages://${PACKAGE_NAME}/${scriptTemplate}`, 'utf8') 
        let data =  FileUtil.readFileSync(tsScriptPath); 
        let jsFileName = "ScriptTemplate"; 
        jsFileName = StringUtil.firstUpperCase(jsFileName);
        data = data.replace(/ScriptTemplate/g, jsFileName);
        let jsPath  = `${selectPath}/${jsFileName}.ts`;  
        if(Editor.assetdb.exists(jsPath)){
            Editor.log("已经存在",jsPath) 
            return;
        }
        Editor.assetdb.create(jsPath, data, function ( err, results ) {
            if(err) return Editor.log(err,"创建脚本失败");
            Editor.log("创建脚本成功",results) 
        }); 
    },


    loadSymbolName(callback,defineName='',result=[]){
        let ps = {value:'请输入变量名字',meta:'',score:0};
		result.unshift(ps)
        // 下拉框选中后操作事件
		let onSearchAccept = (data,cmdLine)=>
		{
			let name = cmdLine.getValue();
			if(ps.value != data.item.value){
				name = data.item.value
			}
			if(name && name != ps.value){
				callback(name);
			}
		}

        // 修改搜索框时，通过该函数读取显示的实时显示下拉列表内容, cmdLine为输入文本框对象
		let onCompletionsFunc = (cmdLine)=>{
			return result;
		}
        this.parent.openSearchBox(defineName,[],(data,cmdLine)=>onSearchAccept(data,cmdLine),(cmdLine)=>onCompletionsFunc(cmdLine))
		this.parent.setMiniSearchBoxToTouchPos();

    },


    /**
     * 创建页面模板
     */
    onCerateLayer(){
        Editor.log("自定义脚本模板 自己扩展")  
        
    },

    /**
     * 创建脚本
     */
    onCreaorSciprt(){
        Editor.log("自定义扩展菜单 自己扩展")  
    }    

 }

 module.exports = CCCMENU_FUNC;