const { BrowserWindow } = require('electron');
const I18n = require('./i18n');

/** 包名 */
const PACKAGE_NAME = require('../package.json').name;

/** 语言 */ 
const LANG = Editor.lang;

/**
 * i18n
 * @param {string} key
 * @returns {string}
 */
const translate = (key) => I18n.translate(LANG, key);

/** 扩展名称 */
const EXTENSION_NAME = translate('name');

/**
 * 面板管理器
 */
const PanelManager = {

    /**
     * 打开生产界面
     */
    openViewPanel() {
        Editor.Panel.open(`${PACKAGE_NAME}.view`);
    }, 
};

module.exports = PanelManager;
