const { ipcMain } = require('electron');

/** 包名 */
const PACKAGE_NAME = require('../package.json').name;


/**监听整个打印事件 */

const ISDEBUG = false;
if(ISDEBUG){
    func = Editor.Ipc.sendToPanel
    Editor.Ipc.sendToPanel = (n,r,...i)=>{console.log(n,r,...i); return func(n,r,...i)}
}

/**
 * 主进程工具
 * @version 20210703
 */
const MainUtil = {

    /**
     * 监听事件（一次性）
     * @param {string} event 事件名
     * @param {Function} callback 回调
     */
    once(event, callback) {
        ipcMain.once(`${PACKAGE_NAME}:${event}`, callback);
    },

    /**
     * 监听事件
     * @param {string} event 事件名
     * @param {Function} callback 回调
     */
    on(event, callback) {
        //Editor.log("监听消息",`${PACKAGE_NAME}:${event}`) 
        ipcMain.on(`${PACKAGE_NAME}:${event}`, callback);
    },

    /**
     * 取消事件监听
     * @param {string} event 事件名
     */
    removeAllListeners(event) {
        ipcMain.removeAllListeners(`${PACKAGE_NAME}:${event}`);
    },

    /**
     * 发送事件到指定渲染进程
     * @param {EventEmitter} sender 渲染进程实例
     * @param {string} event 事件名
     * @param {...any} args 参数
     */
    send(sender, event, ...args) {
        sender.send(`${PACKAGE_NAME}:${event}`, ...args);
    },

};

module.exports = MainUtil;
