/**
 * 字符串工具
 */
 const StringUtil = {

    /**
     * 首字母大写
     */
    firstUpperCase(str) {
        if(!str) return "temp";
        return str.toLowerCase().replace(/( |^)[a-z]/g, (L) => L.toUpperCase());
    } 
 }

 module.exports = StringUtil;