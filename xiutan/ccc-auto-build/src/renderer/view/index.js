const Electron = require('electron');
const { ipcRenderer } = require('electron');
const fs = require('fs'); 
const I18n = require('../../i18n'); 
/** 语言 */
const LANG = Editor.lang;

/** 包名 */
const PACKAGE_NAME = require('../../../package.json').name;
/** 模板 */
const SCRIPTTEMPLATE = require('../../../package.json').scriptTemplate; 

/**项目路径 */
let ProjectPath = Editor.Project.path.replace(/\\/g, "\/"); 

//脚本模板
let componentTemplate = fs.readFileSync(`${ProjectPath}/packages/${PACKAGE_NAME}/${SCRIPTTEMPLATE}`,"utf-8");

/**
 * 自动绑定类型
 */
 let validType = {
    Node: ["node", "Node"],
    Sprite: ["sp", "Sprite", "sprite"],
    Label: ["label", "Label"],
    Button: ["bt", "Button", "button"],
    ScrollView: ["scrollView", "ScrollView"],
    Layout: ["layout", "Layout"],
    Widget: ["widget", "Widget"]
  };
   
/**
 * i18n
 * @param {string} key
 * @returns {string}
 */
const translate = (key) => I18n.translate(LANG, key);

/** Vue 应用 */
const App = {

  /**
   * 数据
   */
  data() {
    return {  
       baseURl:"",
       assetsUrl:"",
       logView:
`命名格式参照表
let HeadMenu = {
    "node": "cc.Node",
    "list": "cc.ScrollView",
    "lb": "cc.Label",
    "spr": "cc.Sprite",
    "rich": "cc.RichText",
    "btn": "cc.Button",
    "bar": "cc.ProgressBar",
    "edit": "cc.EditBox",
    "video": "cc.VideoPlayer",
    "bone": "dragonBones.ArmatureDisplay",
    "spine": "sp.Skeleton"
  }

  1.需要被生成绑定的命名一定是 $xxxx_组件类型 只支持基础的组件类型
    如:  $money_lb  $money_spr  $open_btn  这样

  2.带有@标记符的将会阻止生成属性 包括子节点下有$的节点
    只要名字带有@将会不生成节点和属性
    会自动生成按钮绑定事件 ·on$nameTouchEnd
  3.已内置支持二开开发自定义右键菜单
    只实现了一个基础模板创建 有缘人完善自动生成路径这块

    需要生成自动绑定界面属性脚本添加
    /*===========================自动绑定组件开始===========================*/
    /*===========================自动绑定组件结束 ===========================*/

    需要生成自动绑定按钮函数需要代码内任意地方添加
    /*===========================自动生成按钮事件开始==========================*/
    /*===========================自动生成按钮事件结束==========================*/


       `,
       saveDirPath:"",   
    };
  },

  /**
   * 计算属性
   */
  computed: { 

  },

  /**
   * 监听属性
   */
  watch: { 

      
  },

  /**
   * 实例函数
   */
  methods: {

    /**
     * 添加日志
     * @param str 输出内容
     * @param flag 标记
     */
    addLog(str,flag) {
      if(flag){
          this.logView += `[${flag}]:${str}\n`;
      }else{ 
          this.logView +=  `[${new Date().toLocaleString()}]:${str}\n`;
      }
      this.$refs.textarea.scrollTop  = this.$refs.textarea.scrollHeight;
    },  

    /**
     * i18n
     * @param {string} key 
     */
    i18n(key) {
      return translate(key);
    },

    /**
     * 选择保存文件夹
     */
    onSelectSaveDirPath(){
        let path = Editor.Project.path;
        if (this.saveDirPath && this.saveDirPath.length > 0) {
            if (Fs.existsSync(this.saveDirPath)) {
                path = this.saveDirPath;
            }
        } 
        let res = Editor.Dialog.openFile({
          title: "选择本地下载存放目录",
          defaultPath: path,
          properties: ['openDirectory'],
        });

        if (res !== -1) {
          this.saveDirPath = res[0];
          this.addLog(this.saveDirPath,"保存位置");
        }   
    },

      /**
       * 打开保存目录
       */
    onOpenSaveDirPath(){
        if (!Fs.existsSync(this.saveDirPath)) {
            this.addLog("目录不存在：" + this.saveDirPath);
            return;
        }
        Electron.shell.showItemInFolder(this.saveDirPath);
        Electron.shell.beep(); 
    }, 
 
    onGetResBtn(){
       
    }, 
   }

}

module.exports = App;
