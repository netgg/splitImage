"use strict"; 
const Electron = require('electron'); 
const I18n = require('./i18n');
const PanelManager = require('./panel-manager');
const MainUtil = require('./main-util');
const Updater = require('./updater'); 
const CCCMENU = require('./ccc-menu'); 
const CCCMENU_FUNC = require('./ccc-menu-func'); 
/** 包名 */
const PACKAGE_NAME = require('../package.json').name;

/** 语言 */
const LANG = Editor.lang;

/**
 * i18n
 * @param {string} key
 * @returns {string}
 */

const translate = (key) => I18n.translate(LANG, key); 
 
let _lastUuid ;//最后打开的预制节点,记录当前打开层的uuid
 
module.exports = {
	
   /**
	* 注册监听消息
    */
  messages: {

	/**
	 * 打开说明
	 */
	'open_set'(){
		PanelManager.openViewPanel();
	},

    /**
     * 为选中的节点添加模板脚本
     */
    'build_ts'() { 
		//添加脚本
        Editor.Scene.callSceneScript(PACKAGE_NAME, 'add-script',function(error,obj){
			if(obj){
			  return Editor.log(obj);
			} 
			//执行回来的操作 
		}); 
    },  

	'bindNode'() { 
		//添加脚本
        Editor.Scene.callSceneScript(PACKAGE_NAME, 'bind_Code_ByNode',function(error,obj){
			if(obj){
			  return Editor.log(obj);
			} 
			//执行回来的操作 
		}); 
    },  
 
	'generate_attribute'() { 
		//添加脚本
        Editor.Scene.callSceneScript(PACKAGE_NAME, 'generate_attribute'); 
    },  

 
	'getPrefabUuid'(event,a){
		if (event.reply) {
			event.reply(null, _lastUuid);
		}
	},

	'scene:enter-prefab-edit-mode' (event,uuid) {
		//Editor.log("打开prefab",uuid)
		_lastUuid = uuid;
	 },

  },
 
	/**
	 * 生命周期：加载
	 */
	load() { 
		//Editor.log("注册主进程消息") 
		MainUtil.on('ready', onReadyEvent); 
		MainUtil.on('print', onPrintEvent);
		MainUtil.on('cmdCreatePopLayer', CCCMENU_FUNC.onCeratePopLayer);
		MainUtil.on('cmdCreateLayer',   CCCMENU_FUNC.onCerateLayer);
		MainUtil.on('cmdCreaorSciprt',  CCCMENU_FUNC.onCreaorSciprt); 
		CCCMENU.init(); 
	},

   /**
   * 生命周期：卸载
   */
	unload() {
		//Editor.log("移除主进程消息")
		MainUtil.removeAllListeners(`ready`);
		MainUtil.removeAllListeners(`print`); 
		MainUtil.removeAllListeners(`cmdCreatePopLayer`);
		MainUtil.removeAllListeners(`cmdCreateLayer`);
		MainUtil.removeAllListeners(`cmdCreaorSciprt`);
	},  
}
 

/**
 * （渲染进程）打印事件回调
 * @param {Electron.IpcMainEvent} event 
 * @param {{ type: string, content: string }} options 选项
 */
function onPrintEvent(event, options) {
	const { type, content } = options;
	print(type, content);
}
   
/**
 * 渲染进程的 EventEmitter
 * @type {EventEmitter}
 */
 let renderer = null;

/**
 * （渲染进程）就绪事件回调
 * @param {Electron.IpcMainEvent} event 
 */
function onReadyEvent(event) {
	// 保存实例
	renderer = event.sender;  
}


/**
 * 打印信息到控制台
 * @param {'log' | 'info' | 'warn' | 'error' | string} type 类型 | 内容
 * @param {string} content 内容
 */
 function print(type, content = undefined) {
	if (content == undefined) {
	  content = type;
	  type = 'log';
	}
	const message = `[${EXTENSION_NAME}] ${content}`;
	switch (type) {
	  default:
	  case 'log': {
		Editor.log(message);
		break;
	  }
	  case 'info': {
		Editor.info(message);
		break;
	  }
	  case 'warn': {
		Editor.warn(message);
		break;
	  }
	  case 'error': {
		Editor.error(message);
		break;
	  }
	}
  }

 
