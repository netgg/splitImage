/**
 * 主要写creator菜单的功能扩展
 * auto zhuangzhuang
 * 借鉴的simple_code编辑器功能
 */
const { ipcMain,ipcRenderer } = require('electron');
const MainUtil = require('./main-util');
/** 包名 */
const PACKAGE_NAME = require('../package.json').name;
/** 菜单对照表  取巧了 */
const MENU_PANEL_TYPE = {"创建节点":"layerMenu","新建":"assetsLayer","create":"layerMenu","new":"assetsLayer"};

const CCCMENU = { 
        /**
         * 读取扩展逻辑文件 
         */
        init(){
            this.menuCfgs = {}
            //默认给资源菜单添加   
            let menuCfg = { 
                layerMenu:[
                    { type: 'separator' },
                    { label : "绑定脚本", enabled:true, cmd: "cmdCreaorSciprt"}, 
                ],
                assetsLayer : [
                    { type: 'separator' },
                    { label : "自动生成模板", enabled:true, cmd: "cmdCreatePopLayer"},
                    { label : "自定义模板", enabled:true, cmd: "cmdCreateLayer"},
                ],
            }  
            this.menuCfgs["cc-widget-to-code"] = menuCfg; 
            // hook 菜单
            if (!Editor.Menu[`__${PACKAGE_NAME}__`]) {
                Editor.Menu[`__${PACKAGE_NAME}__`] = true;
                Editor.Menu = this.hookMenu(Editor.Menu, this.hookMenuFunc.bind(this));
            }
        },

        hookMenu(orginMenu, hookFunc) {
            const menu = function () {
                hookFunc(...arguments);
                return new orginMenu(...arguments);
            };
                let menuProps = Object.getOwnPropertyNames(orginMenu);
            for (let prop of menuProps) {
                    const object = Object.getOwnPropertyDescriptor(orginMenu, prop);
                    if (object.writable) {
                        menu[prop] = orginMenu[prop];
                    }
            }
            menu.prototype = orginMenu.prototype;
            return menu;
        },

        hookMenuFunc(template)  
        {    
            const firstMenu = template[0];  
            //Editor.log(firstMenu)
            let menuType = MENU_PANEL_TYPE[firstMenu.label]; 
            for (const id in this.menuCfgs) 
            { 
                let menuCfg = this.menuCfgs[id]; 
                if(!menuCfg) continue;
                let list = menuCfg[menuType]; 
                if(!list) continue;
                for (let i = 0; i < list.length; i++) 
                {
                    const item = list[i];
                    if(item.type != 'separator'){
                        this.applyItem(item,[item.label],firstMenu.params);
                    }
                    template.push(item);
                }
            }  
        }, 

        applyItem(item,parnetPaths,args){
            if(item.submenu){
                for (let n = 0; n < item.submenu.length; n++) 
                    {
                        let sub_item = item.submenu[n];
                        let paths = JSON.parse( JSON.stringify(parnetPaths) )
                        paths.push(sub_item.label)
                        this.applyItem(sub_item,paths)
                    }
                }else {
                    if(item.message == null){
                        let paths = JSON.parse( JSON.stringify(parnetPaths) )
                        let toArgs = item.params || {label:item.label,paths,args:args||{}}
                        item.click = ()=>{ 
                            Editor.Ipc.sendToMain(`${PACKAGE_NAME}:${item.cmd}`, toArgs);
                        };
                    }else{
                        let paths = JSON.parse( JSON.stringify(parnetPaths) )
                        item.params = item.params || {label:item.label,paths,args:args||{}}
                    }
            }
        }, 
}
 
module.exports = CCCMENU;