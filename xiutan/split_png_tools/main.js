const Exec = require('child_process').exec;
const Os = require('os');
const fs = require('fs');
const path = require('path');
const https = require('https');
const URL = require('url').URL; 
 
module.exports = {

  load() {
    Editor.Builder.on('build-start', this.onBuildStart);
    Editor.Builder.on('build-finished', this.onBuildFinished);
  },

  unload() {
    Editor.Builder.removeListener('build-start', this.onBuildStart);
    Editor.Builder.removeListener('build-finished', this.onBuildFinished);
  },

  messages: {
    'open-panel'() {
      Editor.Panel.open('split_png_tools');
    },
  },

  async onBuildStart(options, callback) {
    // 读取配置
    callback();
  },
 
  async onBuildFinished(options, callback) {
    callback();
  },
}