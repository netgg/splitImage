﻿# coding: utf-8
import os
import xlrd
import json

# 格式化
def json_format( name, value ):
	if not isinstance( name, str ) and not isinstance( name, unicode ):
		print "name is ", name
		print "key not str"
		return False
	if len( name ) < 0:
		print "key is null"
		return False
		
	# json串
	if name.find( "_json" ) >= 0:
		name = name[:len(name)-5]
		if len( value ) > 0:
			value = json.loads( value )
		
	return { name:value }
			
# 输出到json
def export_json( path, name, exportPath ):
	fileName = os.path.join(path, name)
	xls = xlrd.open_workbook(fileName)
	configs = {}
	#遍历sheets
	for sheet in xls.sheets():
		#sheet.name     签页名
		#sheet.nrows    行数
		#sheet.ncols    列数
		if sheet.name[:7] == 'export_':
			datas = {}
			# 取得字段名
			names = {}
			for col in range( sheet.ncols ) :
				v = sheet.cell_value( 1, col )
				if isinstance( v, unicode ):
					if len(v) <= 0:
						continue
					names[col] = v

			for row in range( 2, sheet.nrows ) :
				# file table start
				data = {}
				for col in range( 1, sheet.ncols ) :
					if col not in names :
						continue
						
					name = names[col]
					value = sheet.cell_value( row, col )
					data.update( json_format( name, value ) )
				
				index = sheet.cell_value( row, 0 )
				if not isinstance( index, unicode ):
					index = int( index )
				datas.update( { index:data } )

			export = sheet.name[7:]
			exportName = os.path.join( exportPath, export )
			print "%s.json"%export
			file = open( "%s.json"%exportName, 'w+' )
			formatR = json.dumps( datas )
			#formatR = formatR.replace(" ", "");
			file.write( formatR )
			file.close()
			configs.update( {export:datas} )
	return configs
	
def find_file( path, jsonPath ):
	configs = {}
	for name in os.listdir( path ):
		if os.path.isdir( name ):
			find_file( os.path.join(path, name), jsonPath )
		else:
			portion = os.path.splitext(name)
			if portion[1] == '.xlsx':
				print name
				config = export_json( path, name, jsonPath )
				# 合并配置
				configs = dict( configs.items() + config.items())
				
	# 输出合并配置
	exportName = os.path.join( jsonPath, 'gameConfigs' )
	print exportName
	file = open( "%s.json"%exportName, 'w+' )
	formatR = json.dumps( configs )
	formatR = formatR.replace(" ", "");
	file.write( formatR )
	file.close()

if __name__=='__main__':
	# 创建输出目录，如果已有删除文件
	jsonPath = os.path.join( os.getcwd(), "json" )
	if os.path.exists( jsonPath ):
		for name in os.listdir( jsonPath ):
			os.remove(os.path.join(jsonPath, name))
	else:
		os.makedirs( jsonPath )
	jsonPath = jsonPath.decode('gbk')
	
	# 开始查找
	find_file( os.getcwd(), jsonPath )
	















