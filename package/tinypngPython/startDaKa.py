# coding: utf-8
import json
import os
import sys 
import time 
import datetime
import random 
from urllib3 import encode_multipart_formdata
import requests

#当前配置数据
conf = { 
	'files':[],
	#要处理的文件夹路径，默认当前路径
	'EntryFolder': './/',
	#是否递归查找图片，
	'isDeepLoop': True,
	#要处理的文件格式，图片文件
	'Exts': ['.jpg', '.png'],
	#最大处理的图片大小
	'Max': 5200000, # 5MB == 5242848.754299136
};


def Log(txt):
	#pass
	print(txt)

'''
 * 过滤待处理文件夹，得到待处理文件列表
 * @param {*} folder 待处理文件夹
 * @param {*} files 待处理文件列表
 '''
def fileFilter(root):
	items = os.listdir(root)
	for item in items:
		path = os.path.join(root, item)
		if os.path.isdir(path):
			#print('[-]', path)
			fileFilter(path)
		if os.path.splitext(path)[1] in conf["Exts"] :
			conf["files"].append(path)

def  getRandomIP():
	m=random.randint(0,255)
	n=random.randint(0,255)
	x=random.randint(0,255)
	y=random.randint(0,255)
	randomIP=str(m)+'.'+str(n)+'.'+str(x)+'.'+str(y)
	return randomIP;

def getAjaxOptions():
    return {
        "method": 'POST',
        "hostname": 'tinypng.com',
        "path": '/web/shrink',
         
    }
 


def sendFile(filename, file_path):
	url = "https://tinypng.com/web/shrink" # 请求的接口地址 
	newname = file_path.split('\\')[-1]
	Log(newname) 
	headers = {'Content-Type': 'image/png',"path":"/web/shrink"}
	files = {'file': open(file_path, 'rb')}
	res = requests.post(url, headers=headers,files=files)
	return res.text
		
if __name__ == "__main__":  
	fileFilter("./png")
	res = sendFile("img_bg.png", "E:\\mySite\\splitImage\\package\\tinypngPython\\png\\assets\\img_bg.png") # 调用sendFile方法
	Log(str(res))
	
	